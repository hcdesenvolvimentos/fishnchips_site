-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 05/12/2017 às 11:18
-- Versão do servidor: 5.5.58-cll
-- Versão do PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `pixd_fishnchips_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_commentmeta`
--

CREATE TABLE `fc_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_comments`
--

CREATE TABLE `fc_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_links`
--

CREATE TABLE `fc_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_options`
--

CREATE TABLE `fc_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_options`
--

INSERT INTO `fc_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://fishnchips.pixd.com.br/', 'yes'),
(2, 'home', 'http://fishnchips.pixd.com.br/', 'yes'),
(3, 'blogname', 'Fish &#039;n&#039; Chips', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'fishnchips@palupa.com.br', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '100', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '100', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:190:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:25:\"index.php?xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:11:\"cardapio/?$\";s:28:\"index.php?post_type=cardapio\";s:41:\"cardapio/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=cardapio&feed=$matches[1]\";s:36:\"cardapio/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=cardapio&feed=$matches[1]\";s:28:\"cardapio/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=cardapio&paged=$matches[1]\";s:9:\"equipe/?$\";s:26:\"index.php?post_type=equipe\";s:39:\"equipe/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=equipe&feed=$matches[1]\";s:34:\"equipe/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=equipe&feed=$matches[1]\";s:26:\"equipe/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=equipe&paged=$matches[1]\";s:8:\"clube/?$\";s:25:\"index.php?post_type=clube\";s:38:\"clube/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=clube&feed=$matches[1]\";s:33:\"clube/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=clube&feed=$matches[1]\";s:25:\"clube/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=clube&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"cardapio/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"cardapio/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"cardapio/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"cardapio/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"cardapio/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"cardapio/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"cardapio/([^/]+)/embed/?$\";s:41:\"index.php?cardapio=$matches[1]&embed=true\";s:29:\"cardapio/([^/]+)/trackback/?$\";s:35:\"index.php?cardapio=$matches[1]&tb=1\";s:49:\"cardapio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?cardapio=$matches[1]&feed=$matches[2]\";s:44:\"cardapio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?cardapio=$matches[1]&feed=$matches[2]\";s:37:\"cardapio/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?cardapio=$matches[1]&paged=$matches[2]\";s:44:\"cardapio/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?cardapio=$matches[1]&cpage=$matches[2]\";s:33:\"cardapio/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?cardapio=$matches[1]&page=$matches[2]\";s:25:\"cardapio/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"cardapio/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"cardapio/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"cardapio/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"cardapio/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"cardapio/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"equipe/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"equipe/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"equipe/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"equipe/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"equipe/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"equipe/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"equipe/([^/]+)/embed/?$\";s:39:\"index.php?equipe=$matches[1]&embed=true\";s:27:\"equipe/([^/]+)/trackback/?$\";s:33:\"index.php?equipe=$matches[1]&tb=1\";s:47:\"equipe/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?equipe=$matches[1]&feed=$matches[2]\";s:42:\"equipe/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?equipe=$matches[1]&feed=$matches[2]\";s:35:\"equipe/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?equipe=$matches[1]&paged=$matches[2]\";s:42:\"equipe/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?equipe=$matches[1]&cpage=$matches[2]\";s:31:\"equipe/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?equipe=$matches[1]&page=$matches[2]\";s:23:\"equipe/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"equipe/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"equipe/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"equipe/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"equipe/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"equipe/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"clube/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"clube/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"clube/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"clube/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"clube/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"clube/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"clube/([^/]+)/embed/?$\";s:38:\"index.php?clube=$matches[1]&embed=true\";s:26:\"clube/([^/]+)/trackback/?$\";s:32:\"index.php?clube=$matches[1]&tb=1\";s:46:\"clube/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?clube=$matches[1]&feed=$matches[2]\";s:41:\"clube/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?clube=$matches[1]&feed=$matches[2]\";s:34:\"clube/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?clube=$matches[1]&paged=$matches[2]\";s:41:\"clube/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?clube=$matches[1]&cpage=$matches[2]\";s:30:\"clube/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?clube=$matches[1]&page=$matches[2]\";s:22:\"clube/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"clube/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"clube/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"clube/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"clube/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"clube/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:59:\"categoria-cardapio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaCardapio=$matches[1]&feed=$matches[2]\";s:54:\"categoria-cardapio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaCardapio=$matches[1]&feed=$matches[2]\";s:35:\"categoria-cardapio/([^/]+)/embed/?$\";s:50:\"index.php?categoriaCardapio=$matches[1]&embed=true\";s:47:\"categoria-cardapio/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaCardapio=$matches[1]&paged=$matches[2]\";s:29:\"categoria-cardapio/([^/]+)/?$\";s:39:\"index.php?categoriaCardapio=$matches[1]\";s:56:\"categoria-clube/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?categoriaClube=$matches[1]&feed=$matches[2]\";s:51:\"categoria-clube/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?categoriaClube=$matches[1]&feed=$matches[2]\";s:32:\"categoria-clube/([^/]+)/embed/?$\";s:47:\"index.php?categoriaClube=$matches[1]&embed=true\";s:44:\"categoria-clube/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?categoriaClube=$matches[1]&paged=$matches[2]\";s:26:\"categoria-clube/([^/]+)/?$\";s:36:\"index.php?categoriaClube=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=56&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:12:{i:0;s:35:\"base-fishnchips/base-fishnchips.php\";i:1;s:39:\"categories-images/categories-images.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:32:\"disqus-comment-system/disqus.php\";i:4;s:33:\"duplicate-post/duplicate-post.php\";i:5;s:21:\"meta-box/meta-box.php\";i:6;s:37:\"post-types-order/post-types-order.php\";i:7;s:35:\"redux-framework/redux-framework.php\";i:8;s:45:\"taxonomy-terms-order/taxonomy-terms-order.php\";i:9;s:24:\"wordpress-seo/wp-seo.php\";i:10;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:11;s:28:\"wysija-newsletters/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:84:\"C:\\wamp\\www\\projetos\\fishnchips_site/wp-content/plugins/wysija-newsletters/index.php\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'fish-n-chips', 'yes'),
(41, 'stylesheet', 'fish-n-chips', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '37965', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '59', 'yes'),
(84, 'page_on_front', '56', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '36686', 'yes'),
(92, 'fc_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:69:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"wysija_newsletters\";b:1;s:18:\"wysija_subscribers\";b:1;s:13:\"wysija_config\";b:1;s:16:\"wysija_theme_tab\";b:1;s:16:\"wysija_style_tab\";b:1;s:22:\"wysija_stats_dashboard\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'WPLANG', 'pt_BR', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'cron', 'a:4:{i:1508251873;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1508251894;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1508253514;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(135, 'theme_mods_twentysixteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1463410322;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(136, 'current_theme', 'Fish n\\\' Chips', 'yes'),
(137, 'theme_mods_fish-n-chips', 'a:1:{i:0;b:0;}', 'yes'),
(138, 'theme_switched', '', 'yes'),
(139, 'recently_activated', 'a:0:{}', 'yes'),
(144, 'wpcf7', 'a:2:{s:7:\"version\";s:3:\"4.7\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1463399975;s:7:\"version\";s:5:\"4.4.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(149, 'widget_wysija', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(150, 'wysija_post_type_updated', '1463410853', 'yes'),
(151, 'wysija_post_type_created', '1463410853', 'yes'),
(152, 'installation_step', '16', 'yes'),
(153, 'wysija', 'YTo3Mzp7czo5OiJmcm9tX25hbWUiO3M6MTA6ImZpc2huY2hpcHMiO3M6MTI6InJlcGx5dG9fbmFtZSI7czoxMDoiZmlzaG5jaGlwcyI7czoxNToiZW1haWxzX25vdGlmaWVkIjtzOjI0OiJmaXNobmNoaXBzQHBhbHVwYS5jb20uYnIiO3M6MTA6ImZyb21fZW1haWwiO3M6MTQ6ImluZm9AbG9jYWxob3N0IjtzOjEzOiJyZXBseXRvX2VtYWlsIjtzOjE0OiJpbmZvQGxvY2FsaG9zdCI7czoxNToiZGVmYXVsdF9saXN0X2lkIjtpOjE7czoxNzoidG90YWxfc3Vic2NyaWJlcnMiO3M6MToiMiI7czoxNjoiaW1wb3J0d3BfbGlzdF9pZCI7aToyO3M6MTg6ImNvbmZpcm1fZW1haWxfbGluayI7aTo1O3M6MTI6InVwbG9hZGZvbGRlciI7czo2MzoiQzpcd2FtcFx3d3dccHJvamV0b3NcZmlzaG5jaGlwc19zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkc1x3eXNpamFcIjtzOjk6InVwbG9hZHVybCI7czo2ODoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9maXNobmNoaXBzX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS8iO3M6MTY6ImNvbmZpcm1fZW1haWxfaWQiO2k6MztzOjk6Imluc3RhbGxlZCI7YjoxO3M6MjA6Im1hbmFnZV9zdWJzY3JpcHRpb25zIjtpOjE7czoxNDoiaW5zdGFsbGVkX3RpbWUiO2k6MTQ2MzQxMDg2NztzOjE3OiJ3eXNpamFfZGJfdmVyc2lvbiI7czo1OiIyLjcuMSI7czoxMToiZGtpbV9kb21haW4iO3M6OToibG9jYWxob3N0IjtzOjE2OiJ3eXNpamFfd2hhdHNfbmV3IjtzOjU6IjIuNy44IjtzOjI0OiJlbWFpbHNfbm90aWZpZWRfd2hlbl9zdWIiO2I6MDtzOjI3OiJlbWFpbHNfbm90aWZpZWRfd2hlbl9ib3VuY2UiO2I6MDtzOjMzOiJlbWFpbHNfbm90aWZpZWRfd2hlbl9kYWlseXN1bW1hcnkiO2I6MDtzOjE5OiJib3VuY2VfcHJvY2Vzc19hdXRvIjtiOjA7czoyMjoibXNfYm91bmNlX3Byb2Nlc3NfYXV0byI7YjowO3M6OToic2hhcmVkYXRhIjtiOjA7czoxMToiZGtpbV9hY3RpdmUiO2I6MDtzOjk6InNtdHBfcmVzdCI7YjowO3M6MTI6Im1zX3NtdHBfcmVzdCI7YjowO3M6MTQ6ImRlYnVnX2xvZ19jcm9uIjtiOjA7czoyMDoiZGVidWdfbG9nX3Bvc3Rfbm90aWYiO2I6MDtzOjIyOiJkZWJ1Z19sb2dfcXVlcnlfZXJyb3JzIjtiOjA7czoyMzoiZGVidWdfbG9nX3F1ZXVlX3Byb2Nlc3MiO2I6MDtzOjE2OiJkZWJ1Z19sb2dfbWFudWFsIjtiOjA7czoxNToiY29tcGFueV9hZGRyZXNzIjtzOjIxOiJ3d3cuZmlzaG5jaGlwcy5jb20uYnIiO3M6MTY6InVuc3Vic2NyaWJlX3BhZ2UiO3M6MToiNSI7czoxNzoiY29uZmlybWF0aW9uX3BhZ2UiO3M6MToiNSI7czo5OiJzbXRwX2hvc3QiO3M6MDoiIjtzOjEwOiJzbXRwX2xvZ2luIjtzOjEwOiJmaXNobmNoaXBzIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjg6ImZAcHAzOTY1IjtzOjk6InNtdHBfcG9ydCI7czoyOiIyNSI7czoxMToic210cF9zZWN1cmUiO3M6MToiMCI7czoxMDoidGVzdF9tYWlscyI7czoyNDoiZmlzaG5jaGlwc0BwYWx1cGEuY29tLmJyIjtzOjEyOiJib3VuY2VfZW1haWwiO3M6MDoiIjtzOjE4OiJzdWJzY3JpcHRpb25zX3BhZ2UiO3M6MToiNSI7czoxMToiaHRtbF9zb3VyY2UiO3M6MToiMCI7czo4OiJpbmR1c3RyeSI7czo1OiJvdXRybyI7czoxNjoiYXJjaGl2ZV9saW5rbmFtZSI7czoxNjoiW3d5c2lqYV9hcmNoaXZlXSI7czoyNjoic3Vic2NyaWJlcnNfY291bnRfbGlua25hbWUiO3M6MjY6Ilt3eXNpamFfc3Vic2NyaWJlcnNfY291bnRdIjtzOjEzOiJhcmNoaXZlX2xpc3RzIjthOjE6e2k6NDtiOjA7fXM6Mzg6InJvbGVzY2FwLS0tYWRtaW5pc3RyYXRvci0tLW5ld3NsZXR0ZXJzIjtiOjA7czozMToicm9sZXNjYXAtLS1lZGl0b3ItLS1uZXdzbGV0dGVycyI7YjowO3M6MzE6InJvbGVzY2FwLS0tYXV0aG9yLS0tbmV3c2xldHRlcnMiO2I6MDtzOjM2OiJyb2xlc2NhcC0tLWNvbnRyaWJ1dG9yLS0tbmV3c2xldHRlcnMiO2I6MDtzOjM1OiJyb2xlc2NhcC0tLXN1YnNjcmliZXItLS1uZXdzbGV0dGVycyI7YjowO3M6Mzg6InJvbGVzY2FwLS0tYWRtaW5pc3RyYXRvci0tLXN1YnNjcmliZXJzIjtiOjA7czozMToicm9sZXNjYXAtLS1lZGl0b3ItLS1zdWJzY3JpYmVycyI7YjowO3M6MzE6InJvbGVzY2FwLS0tYXV0aG9yLS0tc3Vic2NyaWJlcnMiO2I6MDtzOjM2OiJyb2xlc2NhcC0tLWNvbnRyaWJ1dG9yLS0tc3Vic2NyaWJlcnMiO2I6MDtzOjM1OiJyb2xlc2NhcC0tLXN1YnNjcmliZXItLS1zdWJzY3JpYmVycyI7YjowO3M6MzM6InJvbGVzY2FwLS0tYWRtaW5pc3RyYXRvci0tLWNvbmZpZyI7YjowO3M6MjY6InJvbGVzY2FwLS0tZWRpdG9yLS0tY29uZmlnIjtiOjA7czoyNjoicm9sZXNjYXAtLS1hdXRob3ItLS1jb25maWciO2I6MDtzOjMxOiJyb2xlc2NhcC0tLWNvbnRyaWJ1dG9yLS0tY29uZmlnIjtiOjA7czozMDoicm9sZXNjYXAtLS1zdWJzY3JpYmVyLS0tY29uZmlnIjtiOjA7czozNjoicm9sZXNjYXAtLS1hZG1pbmlzdHJhdG9yLS0tdGhlbWVfdGFiIjtiOjA7czoyOToicm9sZXNjYXAtLS1lZGl0b3ItLS10aGVtZV90YWIiO2I6MDtzOjI5OiJyb2xlc2NhcC0tLWF1dGhvci0tLXRoZW1lX3RhYiI7YjowO3M6MzQ6InJvbGVzY2FwLS0tY29udHJpYnV0b3ItLS10aGVtZV90YWIiO2I6MDtzOjMzOiJyb2xlc2NhcC0tLXN1YnNjcmliZXItLS10aGVtZV90YWIiO2I6MDtzOjM2OiJyb2xlc2NhcC0tLWFkbWluaXN0cmF0b3ItLS1zdHlsZV90YWIiO2I6MDtzOjI5OiJyb2xlc2NhcC0tLWVkaXRvci0tLXN0eWxlX3RhYiI7YjowO3M6Mjk6InJvbGVzY2FwLS0tYXV0aG9yLS0tc3R5bGVfdGFiIjtiOjA7czozNDoicm9sZXNjYXAtLS1jb250cmlidXRvci0tLXN0eWxlX3RhYiI7YjowO3M6MzM6InJvbGVzY2FwLS0tc3Vic2NyaWJlci0tLXN0eWxlX3RhYiI7YjowO30=', 'yes'),
(154, 'wysija_reinstall', '0', 'no'),
(155, 'wysija_schedules', 'a:5:{s:5:\"queue\";a:3:{s:13:\"next_schedule\";i:1492059019;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"bounce\";a:3:{s:13:\"next_schedule\";i:1463497267;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}s:5:\"daily\";a:3:{s:13:\"next_schedule\";i:1492072196;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"weekly\";a:3:{s:13:\"next_schedule\";i:1493240557;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:7:\"monthly\";a:3:{s:13:\"next_schedule\";i:1510628809;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}}', 'yes'),
(159, 'cpto_options', 'a:5:{s:23:\"show_reorder_interfaces\";a:2:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:10:\"capability\";s:13:\"switch_themes\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(165, 'mail_from', '', 'yes'),
(166, 'mail_from_name', '', 'yes'),
(167, 'mailer', 'smtp', 'yes'),
(168, 'mail_set_return_path', 'false', 'yes'),
(169, 'smtp_host', 'localhost', 'yes'),
(170, 'smtp_port', '25', 'yes'),
(171, 'smtp_ssl', 'none', 'yes'),
(172, 'smtp_auth', '', 'yes'),
(173, 'smtp_user', '', 'yes'),
(174, 'smtp_pass', '', 'yes'),
(178, 'wpseo', 'a:23:{s:14:\"blocking_files\";a:0:{}s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:3:\"4.5\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:12:\"website_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";b:0;s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:20:\"enable_setting_pages\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:22:\"show_onboarding_notice\";b:0;s:18:\"first_activated_on\";i:1488909510;}', 'yes'),
(179, 'wpseo_permalinks', 'a:9:{s:15:\"cleanpermalinks\";b:0;s:24:\"cleanpermalink-extravars\";s:0:\"\";s:29:\"cleanpermalink-googlecampaign\";b:0;s:31:\"cleanpermalink-googlesitesearch\";b:0;s:15:\"cleanreplytocom\";b:0;s:10:\"cleanslugs\";b:1;s:18:\"redirectattachment\";b:0;s:17:\"stripcategorybase\";b:0;s:13:\"trailingslash\";b:0;}', 'yes'),
(180, 'wpseo_titles', 'a:114:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:5:\"noodp\";b:0;s:15:\"usemetakeywords\";b:0;s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:40:\"%%name%%, Autor em %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:66:\"Você pesquisou por %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:44:\"Página não encontrada %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:18:\"metakey-home-wpseo\";s:0:\"\";s:20:\"metakey-author-wpseo\";s:0:\"\";s:22:\"noindex-subpages-wpseo\";b:0;s:20:\"noindex-author-wpseo\";b:0;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"metakey-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:16:\"hideeditbox-post\";b:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"metakey-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:16:\"hideeditbox-page\";b:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"metakey-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:22:\"hideeditbox-attachment\";b:0;s:18:\"title-tax-category\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:20:\"metakey-tax-category\";s:0:\"\";s:24:\"hideeditbox-tax-category\";b:0;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:20:\"metakey-tax-post_tag\";s:0:\"\";s:24:\"hideeditbox-tax-post_tag\";b:0;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:23:\"metakey-tax-post_format\";s:0:\"\";s:27:\"hideeditbox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:0;s:14:\"title-destaque\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-destaque\";s:0:\"\";s:16:\"metakey-destaque\";s:0:\"\";s:16:\"noindex-destaque\";b:0;s:17:\"showdate-destaque\";b:0;s:20:\"hideeditbox-destaque\";b:0;s:14:\"title-cardapio\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-cardapio\";s:0:\"\";s:16:\"metakey-cardapio\";s:0:\"\";s:16:\"noindex-cardapio\";b:0;s:17:\"showdate-cardapio\";b:0;s:20:\"hideeditbox-cardapio\";b:0;s:12:\"title-equipe\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:15:\"metadesc-equipe\";s:0:\"\";s:14:\"metakey-equipe\";s:0:\"\";s:14:\"noindex-equipe\";b:0;s:15:\"showdate-equipe\";b:0;s:18:\"hideeditbox-equipe\";b:0;s:11:\"title-clube\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-clube\";s:0:\"\";s:13:\"metakey-clube\";s:0:\"\";s:13:\"noindex-clube\";b:0;s:14:\"showdate-clube\";b:0;s:17:\"hideeditbox-clube\";b:0;s:13:\"title-wysijap\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-wysijap\";s:0:\"\";s:15:\"metakey-wysijap\";s:0:\"\";s:15:\"noindex-wysijap\";b:0;s:16:\"showdate-wysijap\";b:0;s:19:\"hideeditbox-wysijap\";b:0;s:24:\"title-ptarchive-destaque\";s:43:\"%%pt_plural%% %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-ptarchive-destaque\";s:0:\"\";s:26:\"metakey-ptarchive-destaque\";s:0:\"\";s:26:\"bctitle-ptarchive-destaque\";s:0:\"\";s:26:\"noindex-ptarchive-destaque\";b:0;s:24:\"title-ptarchive-cardapio\";s:43:\"%%pt_plural%% %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-ptarchive-cardapio\";s:0:\"\";s:26:\"metakey-ptarchive-cardapio\";s:0:\"\";s:26:\"bctitle-ptarchive-cardapio\";s:0:\"\";s:26:\"noindex-ptarchive-cardapio\";b:0;s:22:\"title-ptarchive-equipe\";s:43:\"%%pt_plural%% %%page%% %%sep%% %%sitename%%\";s:25:\"metadesc-ptarchive-equipe\";s:0:\"\";s:24:\"metakey-ptarchive-equipe\";s:0:\"\";s:24:\"bctitle-ptarchive-equipe\";s:0:\"\";s:24:\"noindex-ptarchive-equipe\";b:0;s:21:\"title-ptarchive-clube\";s:43:\"%%pt_plural%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-ptarchive-clube\";s:0:\"\";s:23:\"metakey-ptarchive-clube\";s:0:\"\";s:23:\"bctitle-ptarchive-clube\";s:0:\"\";s:23:\"noindex-ptarchive-clube\";b:0;s:27:\"title-tax-categoriaCardapio\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:30:\"metadesc-tax-categoriaCardapio\";s:0:\"\";s:29:\"metakey-tax-categoriaCardapio\";s:0:\"\";s:33:\"hideeditbox-tax-categoriaCardapio\";b:0;s:29:\"noindex-tax-categoriaCardapio\";b:0;s:24:\"title-tax-categoriaClube\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-tax-categoriaClube\";s:0:\"\";s:26:\"metakey-tax-categoriaClube\";s:0:\"\";s:30:\"hideeditbox-tax-categoriaClube\";b:0;s:26:\"noindex-tax-categoriaClube\";b:0;}', 'yes'),
(181, 'wpseo_social', 'a:20:{s:9:\"fb_admins\";a:0:{}s:12:\"fbconnectkey\";s:32:\"ff7a15327ac9c15886792eed90763dab\";s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:7:\"summary\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(182, 'wpseo_rss', 'a:2:{s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:54:\"O post %%POSTLINK%% apareceu primeiro em %%BLOGLINK%%.\";}', 'yes'),
(183, 'wpseo_internallinks', 'a:10:{s:20:\"breadcrumbs-404crumb\";s:33:\"Erro 404: Página não encontrada\";s:23:\"breadcrumbs-blog-remove\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:13:\"Arquivos para\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:7:\"Início\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:19:\"Você pesquisou por\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:23:\"post_types-post-maintax\";i:0;}', 'yes'),
(184, 'wpseo_xml', 'a:17:{s:22:\"disable_author_sitemap\";b:1;s:22:\"disable_author_noposts\";b:1;s:16:\"enablexmlsitemap\";b:1;s:16:\"entries-per-page\";i:1000;s:14:\"excluded-posts\";s:0:\"\";s:38:\"user_role-administrator-not_in_sitemap\";b:0;s:31:\"user_role-editor-not_in_sitemap\";b:0;s:31:\"user_role-author-not_in_sitemap\";b:0;s:36:\"user_role-contributor-not_in_sitemap\";b:0;s:35:\"user_role-subscriber-not_in_sitemap\";b:0;s:30:\"post_types-post-not_in_sitemap\";b:0;s:30:\"post_types-page-not_in_sitemap\";b:0;s:36:\"post_types-attachment-not_in_sitemap\";b:1;s:33:\"post_types-wysijap-not_in_sitemap\";b:0;s:34:\"taxonomies-category-not_in_sitemap\";b:0;s:34:\"taxonomies-post_tag-not_in_sitemap\";b:0;s:37:\"taxonomies-post_format-not_in_sitemap\";b:0;}', 'yes'),
(185, 'wpseo_flush_rewrite', '1', 'yes'),
(186, 'CPT_configured', 'TRUE', 'yes'),
(196, 'wysija_last_php_cron_call', '1508209606', 'yes'),
(198, 'wysija_check_pn', '1508209609.59', 'yes'),
(199, 'wysija_last_scheduled_check', '1508209609', 'yes'),
(208, 'redux_version_upgraded_from', '3.6.0.2', 'yes'),
(211, 'configuracao', 'a:26:{s:8:\"last_tab\";s:1:\"0\";s:16:\"opt-inicial-logo\";a:5:{s:3:\"url\";s:71:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-preta.png\";s:2:\"id\";s:2:\"84\";s:6:\"height\";s:3:\"211\";s:5:\"width\";s:3:\"479\";s:9:\"thumbnail\";s:79:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-preta-150x150.png\";}s:28:\"opt-inicial-area-info-titulo\";s:21:\"O Fish ‘n’ Chips.\";s:27:\"opt-inicial-area-info-texto\";s:203:\"Embarcação ancorada na Ilha da Alimentação, em Shopping Mueller. Sua cozinha é capaz de produzir os melhores hambúrgueres e pratos de frutos do mar dos Sete Mares... Ou de Curitiba, como preferir. \";s:26:\"opt-inicial-area-info-link\";s:41:\"http://fishnchips.pixd.com.br/quem-somos/\";s:30:\"opt-inicial-subtitulo-cardapio\";s:133:\"Eis que o mapa do tesouro foi achado! Mas... mas ele nos leva a muitos caminhos. É que Long Victor esconde muitas delícias, muitas.\";s:17:\"opt-inicial-clube\";s:12:\"Clube Victor\";s:23:\"opt-inicial-clube-frase\";s:162:\"Levantai “Jolly Fish Roger”, marujos! O Clube Victor tem como objetivo beneficiar seus tripulantes: aqueles que são frequentadores assíduos da embarcação.\";s:22:\"opt-inicial-clube-link\";s:79:\"http://clubedovictor.giver.com.br/index.php/customer-panel/login?requested_url=\";s:24:\"opt-inicial-clube-imagem\";a:5:{s:3:\"url\";s:74:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-hambuger.png\";s:2:\"id\";s:2:\"71\";s:6:\"height\";s:4:\"1111\";s:5:\"width\";s:4:\"1074\";s:9:\"thumbnail\";s:82:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-hambuger-150x150.png\";}s:22:\"opt-inicial-logo-fundo\";a:5:{s:3:\"url\";s:71:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/fundo-logo.png\";s:2:\"id\";s:2:\"78\";s:6:\"height\";s:3:\"356\";s:5:\"width\";s:4:\"1074\";s:9:\"thumbnail\";s:79:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/fundo-logo-150x150.png\";}s:20:\"opt-quem-somos-frase\";s:51:\"SE VOCÊ GOSTA DE COMER BEM, SEM PERDER MUITO TEMPO\";s:18:\"opt-cardapio-frase\";s:149:\"Mas também deliciosas receitas com frutos do mar, que vêm lá das profundezas da cozinha do Fish‘n’Chips. Dizem que o sabor é inconfundível! \";s:19:\"opt-cardapio-imagem\";a:5:{s:3:\"url\";s:83:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-destaque-cardapio.png\";s:2:\"id\";s:2:\"70\";s:6:\"height\";s:3:\"693\";s:5:\"width\";s:4:\"1080\";s:9:\"thumbnail\";s:91:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-destaque-cardapio-150x150.png\";}s:21:\"opt-contato-subtitulo\";s:53:\"A embarcação está ancorada  em terras curitibanas.\";s:17:\"opt-contato-frase\";s:154:\"Desejas falar com Long Victor, marujo? Não há problema! Ele está à disposição para atender suas dúvidas, expectativas e, principalmente, sua fome. \";s:16:\"opt-contato-foto\";a:5:{s:3:\"url\";s:73:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-contato.png\";s:2:\"id\";s:2:\"68\";s:6:\"height\";s:4:\"1040\";s:5:\"width\";s:4:\"1135\";s:9:\"thumbnail\";s:81:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-contato-150x150.png\";}s:12:\"opt-facebook\";s:1:\"#\";s:11:\"opt-twitter\";s:1:\"#\";s:13:\"opt-blog-foto\";a:5:{s:3:\"url\";s:72:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/banner-blog.png\";s:2:\"id\";s:2:\"61\";s:6:\"height\";s:3:\"263\";s:5:\"width\";s:4:\"1074\";s:9:\"thumbnail\";s:80:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/banner-blog-150x150.png\";}s:26:\"opt-dadosEndereco-endereco\";s:43:\"Av. Cândido de Abreu, 127 Shopping Mueller\";s:21:\"opt-dadosEndereco-cep\";s:8:\"Curitiba\";s:12:\"opt-telefone\";s:14:\"(41) 3018-8820\";s:37:\"opt-dadosEndereco-horario-atendimento\";s:13:\" 10h às 22h.\";s:45:\"opt-dadosEndereco-horario-atendimento-domingo\";s:13:\" 11h às 22h.\";s:29:\"opt-dadosEndereco-logo-branca\";a:5:{s:3:\"url\";s:72:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-branca.png\";s:2:\"id\";s:2:\"81\";s:6:\"height\";s:3:\"133\";s:5:\"width\";s:3:\"300\";s:9:\"thumbnail\";s:72:\"http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-branca.png\";}}', 'yes'),
(212, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:1:{s:26:\"opt-dadosEndereco-endereco\";s:44:\"Av.Cândido de abreu , 127 - shopping muller\";}s:9:\"last_save\";i:1490206717;}', 'yes'),
(213, 'ReduxFrameworkPlugin', 'a:1:{s:4:\"demo\";b:0;}', 'yes'),
(215, 'r_notice_data', '{\"type\":\"redux-message\",\"title\":\"<strong>Redux Framework: New Documentation coming!<\\/strong><br\\/>\",\"message\":\"We\'ve heard your screams and we\'re completely redoing our documentation. For a preview of things to come, please <a href=\\\"http:\\/\\/reduxframework.github.io\\/docs.redux.io\\/getting-started\\/\\\">visit our new docs repo<\\/a>. P.S. You can contribute! If you have an idea for a doc, <a href=\\\"https:\\/\\/github.com\\/reduxframework\\/docs.redux.io\\/issues\\\">post your ideas here<\\/a>. \",\"color\":\"rgba(0,162,227,1)\"}', 'yes'),
(216, 'redux_blast', '1463420969', 'yes');
INSERT INTO `fc_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(217, 'redux_demo', 'a:70:{s:12:\"opt-checkbox\";s:1:\"1\";s:15:\"opt-multi-check\";a:3:{i:1;s:1:\"1\";i:2;s:1:\"0\";i:3;s:1:\"0\";}s:9:\"opt-radio\";s:1:\"2\";s:12:\"opt-sortable\";a:3:{s:8:\"Text One\";s:6:\"Item 1\";s:8:\"Text Two\";s:6:\"Item 2\";s:10:\"Text Three\";s:6:\"Item 3\";}s:18:\"opt-check-sortable\";a:3:{s:3:\"cb1\";b:0;s:3:\"cb2\";b:1;s:3:\"cb3\";b:0;}s:12:\"text-example\";s:12:\"Default Text\";s:17:\"text-example-hint\";s:12:\"Default Text\";s:12:\"opt-textarea\";s:12:\"Default Text\";s:10:\"opt-editor\";s:27:\"Powered by Redux Framework.\";s:15:\"opt-editor-tiny\";s:27:\"Powered by Redux Framework.\";s:18:\"opt-ace-editor-css\";s:29:\"#header{\n   margin: 0 auto;\n}\";s:17:\"opt-ace-editor-js\";s:39:\"jQuery(document).ready(function(){\n\n});\";s:18:\"opt-ace-editor-php\";s:28:\"<?php\n    echo \"PHP String\";\";s:15:\"opt-color-title\";s:7:\"#000000\";s:16:\"opt-color-footer\";s:7:\"#dd9933\";s:16:\"opt-color-header\";a:2:{s:4:\"from\";s:7:\"#1e73be\";s:2:\"to\";s:7:\"#00897e\";}s:14:\"opt-color-rgba\";a:2:{s:5:\"color\";s:7:\"#7e33dd\";s:5:\"alpha\";s:2:\".8\";}s:14:\"opt-link-color\";a:3:{s:7:\"regular\";s:4:\"#aaa\";s:5:\"hover\";s:4:\"#bbb\";s:6:\"active\";s:4:\"#ccc\";}s:17:\"opt-palette-color\";s:3:\"red\";s:17:\"opt-header-border\";a:6:{s:12:\"border-color\";s:7:\"#1e73be\";s:12:\"border-style\";s:5:\"solid\";s:10:\"border-top\";s:3:\"3px\";s:12:\"border-right\";s:3:\"3px\";s:13:\"border-bottom\";s:3:\"3px\";s:11:\"border-left\";s:3:\"3px\";}s:26:\"opt-header-border-expanded\";a:6:{s:12:\"border-color\";s:7:\"#1e73be\";s:12:\"border-style\";s:5:\"solid\";s:10:\"border-top\";s:3:\"3px\";s:12:\"border-right\";s:3:\"3px\";s:13:\"border-bottom\";s:3:\"3px\";s:11:\"border-left\";s:3:\"3px\";}s:14:\"opt-dimensions\";a:2:{s:5:\"width\";i:200;s:6:\"height\";i:100;}s:20:\"opt-dimensions-width\";a:2:{s:5:\"width\";i:200;s:6:\"height\";i:100;}s:11:\"opt-spacing\";a:4:{s:10:\"margin-top\";s:3:\"1px\";s:12:\"margin-right\";s:3:\"2px\";s:13:\"margin-bottom\";s:3:\"3px\";s:11:\"margin-left\";s:3:\"4px\";}s:20:\"opt-spacing-expanded\";a:4:{s:10:\"margin-top\";s:3:\"1px\";s:12:\"margin-right\";s:3:\"2px\";s:13:\"margin-bottom\";s:3:\"3px\";s:11:\"margin-left\";s:3:\"4px\";}s:9:\"opt-media\";a:1:{s:3:\"url\";s:52:\"http://s.wordpress.org/style/images/codeispoetry.png\";}s:14:\"opt-button-set\";s:1:\"2\";s:20:\"opt-button-set-multi\";a:2:{i:0;s:1:\"2\";i:1;s:1:\"3\";}s:9:\"switch-on\";b:1;s:10:\"switch-off\";b:0;s:13:\"switch-parent\";i:0;s:13:\"switch-child1\";b:0;s:13:\"switch-child2\";b:0;s:10:\"opt-select\";s:1:\"2\";s:21:\"opt-select-stylesheet\";s:11:\"default.css\";s:19:\"opt-select-optgroup\";s:1:\"2\";s:16:\"opt-multi-select\";a:2:{i:0;s:1:\"2\";i:1;s:1:\"3\";}s:16:\"opt-select-image\";s:13:\"tree_bark.png\";s:23:\"opt-image-select-layout\";s:1:\"2\";s:12:\"opt-patterns\";i:0;s:16:\"opt-image-select\";s:1:\"2\";s:11:\"opt-presets\";i:0;s:16:\"opt-select_image\";i:0;s:16:\"opt-slider-label\";i:250;s:15:\"opt-slider-text\";i:75;s:17:\"opt-slider-select\";a:2:{i:1;i:100;i:2;i:300;}s:16:\"opt-slider-float\";d:0.5;s:11:\"opt-spinner\";s:2:\"40\";s:19:\"opt-typography-body\";a:4:{s:5:\"color\";s:7:\"#dd9933\";s:9:\"font-size\";s:4:\"30px\";s:11:\"font-family\";s:26:\"Arial,Helvetica,sans-serif\";s:11:\"font-weight\";s:6:\"Normal\";}s:14:\"opt-typography\";a:6:{s:5:\"color\";s:4:\"#333\";s:10:\"font-style\";s:3:\"700\";s:11:\"font-family\";s:4:\"Abel\";s:6:\"google\";b:1;s:9:\"font-size\";s:4:\"33px\";s:11:\"line-height\";s:4:\"40px\";}s:19:\"opt-homepage-layout\";a:3:{s:7:\"enabled\";a:4:{s:10:\"highlights\";s:10:\"Highlights\";s:6:\"slider\";s:6:\"Slider\";s:10:\"staticpage\";s:11:\"Static Page\";s:8:\"services\";s:8:\"Services\";}s:8:\"disabled\";a:0:{}s:6:\"backup\";a:0:{}}s:21:\"opt-homepage-layout-2\";a:2:{s:8:\"disabled\";a:2:{s:10:\"highlights\";s:10:\"Highlights\";s:6:\"slider\";s:6:\"Slider\";}s:7:\"enabled\";a:2:{s:10:\"staticpage\";s:11:\"Static Page\";s:8:\"services\";s:8:\"Services\";}}s:14:\"opt-text-email\";s:13:\"test@test.com\";s:12:\"opt-text-url\";s:25:\"http://reduxframework.com\";s:16:\"opt-text-numeric\";s:1:\"0\";s:22:\"opt-text-comma-numeric\";s:1:\"0\";s:25:\"opt-text-no-special-chars\";s:1:\"0\";s:20:\"opt-text-str_replace\";s:20:\"This is the default.\";s:21:\"opt-text-preg_replace\";s:1:\"0\";s:24:\"opt-text-custom_validate\";s:1:\"0\";s:20:\"opt-textarea-no-html\";s:27:\"No HTML is allowed in here.\";s:17:\"opt-textarea-html\";s:24:\"HTML is allowed in here.\";s:22:\"opt-textarea-some-html\";s:36:\"<p>Some HTML is allowed in here.</p>\";s:18:\"opt-required-basic\";b:0;s:19:\"opt-required-nested\";b:0;s:29:\"opt-required-nested-buttonset\";s:11:\"button-text\";s:19:\"opt-required-select\";s:10:\"no-sidebar\";s:32:\"opt-required-select-left-sidebar\";s:0:\"\";s:33:\"opt-required-select-right-sidebar\";s:0:\"\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(218, 'redux_demo-transients', 'a:2:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1463420969;}', 'yes'),
(272, 'wysija_queries', '', 'no'),
(273, 'wysija_queries_errors', '', 'no'),
(274, 'wysija_msg', '', 'no'),
(432, 'duplicate_post_copyexcerpt', '1', 'yes'),
(433, 'duplicate_post_copyattachments', '0', 'yes'),
(434, 'duplicate_post_copychildren', '0', 'yes'),
(435, 'duplicate_post_copystatus', '0', 'yes'),
(436, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(437, 'duplicate_post_show_row', '1', 'yes'),
(438, 'duplicate_post_show_adminbar', '1', 'yes'),
(439, 'duplicate_post_show_submitbox', '1', 'yes'),
(537, 'z_taxonomy_image15', 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/BananaPie_Alta-min.jpg', 'yes'),
(538, 'wpseo_taxonomy_meta', 'a:3:{s:14:\"categoriaClube\";a:3:{i:15;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:14;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:13;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}}s:17:\"categoriaCardapio\";a:17:{i:2;a:1:{s:13:\"wpseo_linkdex\";s:2:\"20\";}i:3;a:1:{s:13:\"wpseo_linkdex\";s:2:\"18\";}i:4;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:5;a:2:{s:13:\"wpseo_linkdex\";s:2:\"11\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:20;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"30\";}i:7;a:1:{s:13:\"wpseo_linkdex\";s:2:\"15\";}i:21;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:22;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:9;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:10;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:11;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:12;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:8;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"60\";}i:24;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"30\";}i:26;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"30\";}i:25;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"30\";}i:27;a:2:{s:13:\"wpseo_linkdex\";s:2:\"15\";s:19:\"wpseo_content_score\";s:2:\"30\";}}s:8:\"category\";a:1:{i:17;a:1:{s:13:\"wpseo_linkdex\";s:2:\"15\";}}}', 'yes'),
(541, 'z_taxonomy_image14', 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/SaladaInglesaCamarão-min.jpg', 'yes'),
(544, 'z_taxonomy_image13', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/ZZZ.png', 'yes'),
(614, 'z_taxonomy_image19', '', 'yes'),
(624, 'z_taxonomy_image2', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/knife-and-fork-azul.svg', 'yes'),
(627, 'z_taxonomy_image3', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/cherry-top.svg', 'yes'),
(630, 'z_taxonomy_image4', 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/sweet-cake-piece-2.png', 'yes'),
(635, 'z_taxonomy_image5', 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/one-hamburguer.png', 'yes'),
(662, 'z_taxonomy_image20', 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/dish-spoon-and-fork-1.png', 'yes'),
(755, 'tto_options', 'a:3:{s:8:\"autosort\";s:1:\"1\";s:9:\"adminsort\";s:1:\"1\";s:10:\"capability\";s:15:\"install_plugins\";}', 'yes'),
(758, 'z_taxonomy_image7', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-destaque-cardapio.png', 'yes'),
(776, 'z_taxonomy_image21', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/as.png', 'yes'),
(778, 'z_taxonomy_image22', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/13238992_885967388197585_7706280536372095337_n.png', 'yes'),
(783, 'z_taxonomy_image8', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-destaque.png', 'yes'),
(786, 'z_taxonomy_image9', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-destaque-cardapio.png', 'yes'),
(789, 'z_taxonomy_image10', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-hambuger.png', 'yes'),
(792, 'z_taxonomy_image11', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/asasasasa.jpg', 'yes'),
(794, 'z_taxonomy_image12', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/as.png', 'yes'),
(1058, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:24:\"fishnchips@palupa.com.br\";s:7:\"version\";s:5:\"4.6.7\";s:9:\"timestamp\";i:1508209621;}', 'no'),
(1099, 'disqus_active', '1', 'yes'),
(1100, 'disqus_version', '2.85', 'yes'),
(1101, 'disqus_forum_url', 'fishnchipsdisqus', 'yes'),
(1102, 'dsq_external_js', '1', 'yes'),
(1103, 'disqus_cc_fix', '1', 'yes'),
(1104, 'disqus_api_key', 'We2NNStjzrS9lSmlMNalKZjzkOWqkZfYxlAWTKDMy9NLcRTsng1DHvuv0lF8HwRN', 'yes'),
(1105, 'disqus_user_api_key', 'EhDtu9oyTUqRUyPGP5fYPXkOSFevZ5ChuPmAsVD0lvzd5LCh0pwMSLKtzZ9ycNbK', 'yes'),
(1106, 'disqus_replace', 'all', 'yes'),
(1138, 'z_taxonomy_image17', '', 'yes'),
(1455, '_site_transient_timeout_browser_ebee2cb07f407535234173ddc900149d', '1472750490', 'yes'),
(1456, '_site_transient_browser_ebee2cb07f407535234173ddc900149d', 'a:9:{s:8:\"platform\";s:7:\"Windows\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"52.0.2743.116\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'yes'),
(1461, 'db_upgraded', '', 'yes'),
(1464, '_transient_timeout__redux_activation_redirect', '1508209637', 'no'),
(1465, '_transient__redux_activation_redirect', '1', 'no'),
(1466, 'can_compress_scripts', '1', 'no'),
(1479, 'z_taxonomy_image23', '', 'yes'),
(1565, '_site_transient_timeout_browser_51874df863d9bfae72869c204369dfc7', '1474915769', 'no'),
(1566, '_site_transient_browser_51874df863d9bfae72869c204369dfc7', 'a:9:{s:8:\"platform\";s:7:\"Windows\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"53.0.2785.116\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(1570, '_transient_timeout_plugin_slugs', '1490205113', 'no'),
(1571, '_transient_plugin_slugs', 'a:14:{i:0;s:19:\"akismet/akismet.php\";i:1;s:35:\"base-fishnchips/base-fishnchips.php\";i:2;s:39:\"categories-images/categories-images.php\";i:3;s:45:\"taxonomy-terms-order/taxonomy-terms-order.php\";i:4;s:36:\"contact-form-7/wp-contact-form-7.php\";i:5;s:32:\"disqus-comment-system/disqus.php\";i:6;s:33:\"duplicate-post/duplicate-post.php\";i:7;s:9:\"hello.php\";i:8;s:28:\"wysija-newsletters/index.php\";i:9;s:21:\"meta-box/meta-box.php\";i:10;s:37:\"post-types-order/post-types-order.php\";i:11;s:35:\"redux-framework/redux-framework.php\";i:12;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:13;s:24:\"wordpress-seo/wp-seo.php\";}', 'no'),
(1864, '_site_transient_timeout_browser_f40541a13446712011cd7fb71eb39c10', '1482499051', 'no'),
(1865, '_site_transient_browser_f40541a13446712011cd7fb71eb39c10', 'a:9:{s:8:\"platform\";s:7:\"Windows\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"56.0.2924.28\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(2459, '_site_transient_timeout_browser_88337cd9fed317d029aaa03191c0be0b', '1490643299', 'no'),
(2460, '_site_transient_browser_88337cd9fed317d029aaa03191c0be0b', 'a:9:{s:8:\"platform\";s:7:\"Windows\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"56.0.2924.87\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(2497, 'duplicate_post_copytitle', '1', 'yes'),
(2498, 'duplicate_post_copydate', '0', 'yes'),
(2499, 'duplicate_post_copyslug', '1', 'yes'),
(2500, 'duplicate_post_copycontent', '1', 'yes'),
(2501, 'duplicate_post_copythumbnail', '1', 'yes'),
(2502, 'duplicate_post_copytemplate', '1', 'yes'),
(2503, 'duplicate_post_copyformat', '1', 'yes'),
(2504, 'duplicate_post_copyauthor', '0', 'yes'),
(2505, 'duplicate_post_copypassword', '0', 'yes'),
(2506, 'duplicate_post_copycomments', '0', 'yes'),
(2507, 'duplicate_post_copymenuorder', '1', 'yes'),
(2508, 'duplicate_post_blacklist', '', 'yes'),
(2509, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(2510, 'duplicate_post_show_bulkactions', '1', 'yes'),
(2511, 'duplicate_post_version', '3.1.2', 'no'),
(2512, 'duplicate_post_show_notice', '0', 'no'),
(2530, 'wpseo_sitemap_cache_validator_global', '5G6Jb', 'no'),
(2532, '_site_transient_timeout_available_translations', '1490129984', 'no'),
(2533, '_site_transient_available_translations', 'a:89:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-06 11:26:31\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-16 18:36:09\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-21 10:19:10\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.6.4/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-29 08:38:56\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-11 22:42:10\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-03-20 01:58:07\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.6.4/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-24 13:13:07\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-20 16:53:20\";s:12:\"english_name\";s:7:\"Bengali\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-05 09:44:12\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.5.7\";s:7:\"updated\";s:19:\"2016-04-19 23:16:37\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.5.7/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-05 11:04:12\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.4.7\";s:7:\"updated\";s:19:\"2016-02-16 15:34:57\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.7/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-11 18:32:36\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:12:\"Čeština‎\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 17:21:14\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-29 14:03:59\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Forts&#230;t\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-21 21:20:26\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-21 21:21:17\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.6.4/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-15 12:56:13\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-15 12:59:43\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/4.6.4/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 20:02:13\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-11 23:19:29\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-12 02:18:44\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-11 22:36:25\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-16 11:54:12\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-20 07:14:07\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-04 22:04:52\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-17 12:34:44\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/es_VE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-17 22:11:44\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/es_CL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-29 15:07:52\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/es_MX.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/es_PE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-17 17:56:31\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/es_GT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-19 13:48:04\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/es_AR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 17:21:14\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/es_ES.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"es\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:6:\"4.3-RC\";s:7:\"updated\";s:19:\"2015-08-04 06:10:33\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.3-RC/es_CO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 17:21:14\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-27 18:10:49\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-23 20:20:40\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-15 18:30:48\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-10 18:17:57\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-10 18:42:25\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-11-02 11:49:52\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"4.6.3\";s:7:\"updated\";s:19:\"2016-08-21 15:44:17\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.3/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-08 11:09:06\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-25 19:56:49\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 17:21:15\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-07 15:12:28\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-30 13:25:31\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-04 07:13:54\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-22 05:34:53\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 17:22:14\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-19 08:05:09\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-11-01 15:23:06\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-29 11:51:34\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-24 07:18:31\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-11 21:29:34\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-02-18 14:32:25\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.5.7\";s:7:\"updated\";s:19:\"2016-05-12 13:55:28\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.5.7/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:5:\"4.6.3\";s:7:\"updated\";s:19:\"2016-11-13 20:38:52\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.3/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-14 14:18:43\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.16\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.16/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-16 13:09:49\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-14 13:24:10\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.6.4/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-02-09 12:31:50\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-28 08:58:28\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-23 13:45:11\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.6.4/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-22 09:54:16\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.16\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.16/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-03-20 09:35:06\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 17:22:14\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-30 10:29:38\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-30 19:40:04\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-08 14:52:32\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-11-04 18:38:43\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-14 07:00:01\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-12 16:41:17\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-03-15 16:10:12\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.6.4/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-10-12 07:04:13\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-11-27 15:51:36\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-16 10:50:15\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-02-10 18:03:30\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:9:\"Uyƣurqə\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-29 17:22:14\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2017-01-08 10:11:20\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.6.4/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-09 01:01:25\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-08-18 13:53:15\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"4.6.4\";s:7:\"updated\";s:19:\"2016-12-05 11:58:02\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.6.4/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"4.5.7\";s:7:\"updated\";s:19:\"2016-04-17 03:29:01\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.5.7/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no');
INSERT INTO `fc_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2534, 'wpseo_sitemap_1_cache_validator', 'nRng', 'no'),
(2535, 'wpseo_sitemap_destaque_cache_validator', '4KzD1', 'no'),
(2541, '_transient_timeout_select2-css_style_cdn_is_up', '1490206973', 'no'),
(2542, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(2543, '_transient_timeout_select2-js_script_cdn_is_up', '1490206973', 'no'),
(2544, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(2545, 'wpseo_sitemap_categoriaCardapio_cache_validator', '4KzYA', 'no'),
(2546, 'wpseo_sitemap_cardapio_cache_validator', '4KzZv', 'no'),
(2549, 'wpseo_sitemap_278_cache_validator', 'njNH', 'no'),
(2552, 'wpseo_sitemap_215_cache_validator', '5PZbk', 'no'),
(2556, 'wpseo_sitemap_214_cache_validator', 'njNz', 'no'),
(2561, 'wpseo_sitemap_217_cache_validator', '5R2dQ', 'no'),
(2562, 'wpseo_sitemap_220_cache_validator', '5Rjyk', 'no'),
(2572, 'wpseo_sitemap_categoriaClube_cache_validator', '4KA8b', 'no'),
(2573, 'wpseo_sitemap_clube_cache_validator', '4KA8Q', 'no'),
(2579, 'wpseo_sitemap_page_cache_validator', 'xDaM', 'no'),
(2585, 'wpseo_sitemap_equipe_cache_validator', '4KAaO', 'no'),
(2603, 'z_taxonomy_image24', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/fotocarrossel.png', 'yes'),
(2668, 'wpseo_sitemap_205_cache_validator', '4KzOJ', 'no'),
(2669, 'wpseo_sitemap_208_cache_validator', '4KzNw', 'no'),
(2670, 'wpseo_sitemap_211_cache_validator', 'nRj9', 'no'),
(2671, 'wpseo_sitemap_216_cache_validator', '4KzDi', 'no'),
(2676, 'category_children', 'a:0:{}', 'yes'),
(2677, 'wpseo_sitemap_category_cache_validator', '4KAzg', 'no'),
(2678, 'wpseo_sitemap_275_cache_validator', '4KAb2', 'no'),
(2679, 'wpseo_sitemap_post_cache_validator', 'nRnj', 'no'),
(2727, 'wpseo_sitemap_329_cache_validator', '4r43Z', 'no'),
(2730, '_site_transient_timeout_browser_9bc134e93ca434cc48bb120cdc5e1606', '1490879954', 'no'),
(2731, '_site_transient_browser_9bc134e93ca434cc48bb120cdc5e1606', 'a:9:{s:8:\"platform\";s:9:\"Macintosh\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"56.0.2924.87\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(2746, 'wpseo_sitemap_355_cache_validator', 'njNQ', 'no'),
(2750, 'z_taxonomy_image26', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/knife-and-fork-azul.svg', 'yes'),
(2755, 'wpseo_sitemap_52_cache_validator', '4KzYp', 'no'),
(2756, 'wpseo_sitemap_185_cache_validator', 'njNv', 'no'),
(2757, 'wpseo_sitemap_226_cache_validator', 'nRid', 'no'),
(2758, 'wpseo_sitemap_297_cache_validator', 'njNK', 'no'),
(2759, 'wpseo_sitemap_304_cache_validator', 'njNO', 'no'),
(2764, 'z_taxonomy_image25', 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/tray.png', 'yes'),
(2786, 'z_taxonomy_image27', 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/nachos.png', 'yes'),
(2791, 'categoriaCardapio_children', 'a:1:{i:7;a:6:{i:0;i:24;i:1;i:8;i:2;i:9;i:3;i:10;i:4;i:11;i:5;i:12;}}', 'yes'),
(2806, 'categoriaClube_children', 'a:0:{}', 'yes'),
(2818, '_transient_is_multi_author', '0', 'yes'),
(2820, '_transient_fish_n_chips_categories', '0', 'yes'),
(3039, 'wpseo_sitemap_218_cache_validator', '4KzEv', 'yes'),
(3040, 'wpseo_sitemap_219_cache_validator', '4KzFt', 'yes'),
(3041, 'wpseo_sitemap_222_cache_validator', '4KzGt', 'yes'),
(3042, 'wpseo_sitemap_221_cache_validator', '4KzHp', 'yes'),
(3043, 'wpseo_sitemap_223_cache_validator', '4KzIm', 'yes'),
(3044, 'wpseo_sitemap_224_cache_validator', '4KzJx', 'yes'),
(3045, 'wpseo_sitemap_225_cache_validator', '4KzKy', 'yes'),
(3046, 'wpseo_sitemap_182_cache_validator', '4KzQt', 'yes'),
(3047, 'wpseo_sitemap_179_cache_validator', '4KzRq', 'yes'),
(3048, 'wpseo_sitemap_176_cache_validator', '4KzSq', 'yes'),
(3049, 'wpseo_sitemap_173_cache_validator', '4KzTs', 'yes'),
(3050, 'wpseo_sitemap_171_cache_validator', '4KzUo', 'yes'),
(3051, 'wpseo_sitemap_95_cache_validator', '4KzVk', 'yes'),
(3052, 'wpseo_sitemap_94_cache_validator', '4KzWm', 'yes'),
(3053, 'wpseo_sitemap_93_cache_validator', '4KzXs', 'yes'),
(3054, 'wpseo_sitemap_161_cache_validator', '4KzZM', 'yes'),
(3055, 'wpseo_sitemap_166_cache_validator', '4KA1I', 'yes'),
(3056, 'wpseo_sitemap_165_cache_validator', '4KA2t', 'yes'),
(3057, 'wpseo_sitemap_163_cache_validator', 'nRki', 'yes'),
(3058, 'wpseo_sitemap_revision_cache_validator', '3irE', 'yes'),
(3059, 'wpseo_sitemap_164_cache_validator', '4KA4u', 'yes'),
(3060, 'wpseo_sitemap_162_cache_validator', '4KA5y', 'yes'),
(3061, 'wpseo_sitemap_160_cache_validator', '4KA6o', 'yes'),
(3062, 'wpseo_sitemap_159_cache_validator', '4KA7f', 'yes'),
(3063, 'wpseo_sitemap_158_cache_validator', '4KA83', 'yes'),
(3064, 'wpseo_sitemap_255_cache_validator', '4KAcb', 'yes'),
(3065, 'wpseo_sitemap_251_cache_validator', '4KAdi', 'yes'),
(3066, 'wpseo_sitemap_249_cache_validator', '4KAem', 'yes'),
(3067, 'wpseo_sitemap_244_cache_validator', '4KAfq', 'yes'),
(3068, 'wpseo_sitemap_112_cache_validator', '4KAgC', 'yes'),
(3069, 'wpseo_sitemap_110_cache_validator', '4KAhF', 'yes'),
(3070, 'wpseo_sitemap_108_cache_validator', '4KAiO', 'yes'),
(3071, 'wpseo_sitemap_106_cache_validator', 'nRlW', 'yes'),
(3072, 'wpseo_sitemap_104_cache_validator', '4KAkZ', 'yes'),
(3073, 'wpseo_sitemap_102_cache_validator', '4KAlZ', 'yes'),
(3074, 'wpseo_sitemap_98_cache_validator', '4KAn2', 'yes'),
(3075, 'wpseo_sitemap_248_cache_validator', '4KAo5', 'yes'),
(3076, 'wpseo_sitemap_247_cache_validator', '4KAp8', 'yes'),
(3077, 'wpseo_sitemap_246_cache_validator', '4KAqi', 'yes'),
(3078, 'wpseo_sitemap_250_cache_validator', '4KArm', 'yes'),
(3079, 'wpseo_sitemap_243_cache_validator', '4KAsq', 'yes'),
(3080, 'wpseo_sitemap_100_cache_validator', '4KAtB', 'yes'),
(3081, 'wpseo_sitemap_254_cache_validator', '4KAuC', 'yes'),
(3082, 'wpseo_sitemap_256_cache_validator', '4KAvG', 'yes'),
(3083, 'wpseo_sitemap_253_cache_validator', '4KAwJ', 'yes'),
(3084, 'wpseo_sitemap_252_cache_validator', '4KAy3', 'yes'),
(3085, 'wpseo_sitemap_245_cache_validator', '4KAz8', 'yes'),
(3088, '_site_transient_timeout_theme_roots', '1508211410', 'no'),
(3089, '_site_transient_theme_roots', 'a:4:{s:12:\"fish-n-chips\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:14:\"twentyfourteen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(3091, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.8.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.8.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.8.2\";s:7:\"version\";s:5:\"4.8.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.8.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.8.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.8.2\";s:7:\"version\";s:5:\"4.8.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.8.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.8.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.8.2\";s:7:\"version\";s:5:\"4.8.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.7.6.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.7.6.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.7.6-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.7.6-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.7.6\";s:7:\"version\";s:5:\"4.7.6\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1508209620;s:15:\"version_checked\";s:5:\"4.6.7\";s:12:\"translations\";a:0:{}}', 'no'),
(3092, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1508209622;s:7:\"checked\";a:4:{s:12:\"fish-n-chips\";s:0:\"\";s:13:\"twentyfifteen\";s:3:\"1.5\";s:14:\"twentyfourteen\";s:3:\"1.7\";s:13:\"twentysixteen\";s:3:\"1.2\";}s:8:\"response\";a:3:{s:13:\"twentyfifteen\";a:4:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"1.8\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.1.8.zip\";}s:14:\"twentyfourteen\";a:4:{s:5:\"theme\";s:14:\"twentyfourteen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentyfourteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentyfourteen.2.0.zip\";}s:13:\"twentysixteen\";a:4:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"1.3\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.1.3.zip\";}}s:12:\"translations\";a:0:{}}', 'no'),
(3093, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1508209621;s:8:\"response\";a:10:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:3:\"4.0\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/akismet.4.0.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:7:\"default\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";s:7:\"default\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:45:\"taxonomy-terms-order/taxonomy-terms-order.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:34:\"w.org/plugins/taxonomy-terms-order\";s:4:\"slug\";s:20:\"taxonomy-terms-order\";s:6:\"plugin\";s:45:\"taxonomy-terms-order/taxonomy-terms-order.php\";s:11:\"new_version\";s:7:\"1.5.2.2\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/taxonomy-terms-order/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/taxonomy-terms-order.1.5.2.2.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:73:\"https://ps.w.org/taxonomy-terms-order/assets/icon-128x128.png?rev=1564412\";s:2:\"2x\";s:73:\"https://ps.w.org/taxonomy-terms-order/assets/icon-256x256.png?rev=1564412\";s:7:\"default\";s:73:\"https://ps.w.org/taxonomy-terms-order/assets/icon-256x256.png?rev=1564412\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:76:\"https://ps.w.org/taxonomy-terms-order/assets/banner-1544x500.png?rev=1564412\";s:2:\"1x\";s:75:\"https://ps.w.org/taxonomy-terms-order/assets/banner-772x250.png?rev=1564412\";s:7:\"default\";s:76:\"https://ps.w.org/taxonomy-terms-order/assets/banner-1544x500.png?rev=1564412\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:4:\"2.87\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/disqus-comment-system.2.87.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448\";s:3:\"svg\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";s:7:\"default\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";s:7:\"default\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.6.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:3:\"3.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:7:\"default\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";s:7:\"default\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:55:\"<p>new website + WPML compatibility + various fixes</p>\";s:6:\"tested\";s:3:\"4.8\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:6:\"2.7.13\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.7.13.zip\";s:5:\"icons\";a:4:{s:2:\"1x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-128x128.png?rev=1703780\";s:2:\"2x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780\";s:3:\"svg\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";s:7:\"default\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";s:2:\"1x\";s:73:\"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780\";s:7:\"default\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.8.2\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.12.3\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:51:\"https://downloads.wordpress.org/plugin/meta-box.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";s:7:\"default\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1626382\";s:7:\"default\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1626382\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.3.5\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.5.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";s:7:\"default\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";s:7:\"default\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:7:\"3.6.7.7\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.7.7.zip\";s:5:\"icons\";a:4:{s:2:\"1x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-128x128.png?rev=995554\";s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:7:\"default\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";s:7:\"default\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"4.9\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:6:\"0.10.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.0.10.1.zip\";s:5:\"icons\";a:0:{}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:54:\"<p>Addition of Pepipost and cleanup of admin page.</p>\";s:6:\"tested\";s:5:\"4.7.5\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:5:\"5.6.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/wordpress-seo.5.6.1.zip\";s:5:\"icons\";a:4:{s:2:\"1x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-128x128.png?rev=1550389\";s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1550389\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1203032\";s:7:\"default\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1203032\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1695112\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1695112\";s:7:\"default\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1695112\";}s:11:\"banners_rtl\";a:3:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1695112\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1695112\";s:7:\"default\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1695112\";}s:6:\"tested\";s:5:\"4.8.2\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:39:\"categories-images/categories-images.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/categories-images\";s:4:\"slug\";s:17:\"categories-images\";s:6:\"plugin\";s:39:\"categories-images/categories-images.php\";s:11:\"new_version\";s:5:\"2.5.3\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/categories-images/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/categories-images.2.5.3.zip\";s:5:\"icons\";a:0:{}s:7:\"banners\";a:2:{s:2:\"1x\";s:71:\"https://ps.w.org/categories-images/assets/banner-772x250.png?rev=715586\";s:7:\"default\";s:71:\"https://ps.w.org/categories-images/assets/banner-772x250.png?rev=715586\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":11:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:3:\"4.9\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-7.4.9.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:7:\"default\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";s:7:\"default\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.8.2\";s:13:\"compatibility\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:7:\"default\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";s:7:\"default\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_postmeta`
--

CREATE TABLE `fc_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_postmeta`
--

INSERT INTO `fc_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(8, 37, '_edit_last', '1'),
(9, 37, '_edit_lock', '1490296565:1'),
(10, 53, '_wp_attached_file', '2016/06/foto-destaque-cardapio.png'),
(11, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:693;s:4:\"file\";s:34:\"2016/06/foto-destaque-cardapio.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-300x193.png\";s:5:\"width\";i:300;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-768x493.png\";s:5:\"width\";i:768;s:6:\"height\";i:493;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"foto-destaque-cardapio-1024x657.png\";s:5:\"width\";i:1024;s:6:\"height\";i:657;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-600x385.png\";s:5:\"width\";i:600;s:6:\"height\";i:385;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(17, 56, '_edit_last', '1'),
(18, 56, '_edit_lock', '1466168619:1'),
(19, 56, '_wp_page_template', 'pages/inicial.php'),
(20, 59, '_edit_last', '1'),
(21, 59, '_edit_lock', '1466168773:1'),
(23, 61, '_wp_attached_file', '2016/05/banner-blog.png'),
(24, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1074;s:6:\"height\";i:263;s:4:\"file\";s:23:\"2016/05/banner-blog.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"banner-blog-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"banner-blog-300x73.png\";s:5:\"width\";i:300;s:6:\"height\";i:73;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"banner-blog-768x188.png\";s:5:\"width\";i:768;s:6:\"height\";i:188;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"banner-blog-1024x251.png\";s:5:\"width\";i:1024;s:6:\"height\";i:251;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:23:\"banner-blog-600x147.png\";s:5:\"width\";i:600;s:6:\"height\";i:147;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(25, 62, '_wp_attached_file', '2016/05/blog-foto.png'),
(26, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:268;s:6:\"height\";i:233;s:4:\"file\";s:21:\"2016/05/blog-foto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"blog-foto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(27, 63, '_wp_attached_file', '2016/05/foto-blog.png'),
(28, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:378;s:6:\"height\";i:239;s:4:\"file\";s:21:\"2016/05/foto-blog.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"foto-blog-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"foto-blog-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 64, '_wp_attached_file', '2016/05/foto-blogg.png'),
(30, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:290;s:6:\"height\";i:243;s:4:\"file\";s:22:\"2016/05/foto-blogg.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"foto-blogg-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 65, '_wp_attached_file', '2016/05/fotocarrossel.png'),
(32, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:457;s:6:\"height\";i:577;s:4:\"file\";s:25:\"2016/05/fotocarrossel.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"fotocarrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"fotocarrossel-238x300.png\";s:5:\"width\";i:238;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(33, 66, '_wp_attached_file', '2016/05/foto-carrossel.png'),
(34, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:417;s:6:\"height\";i:695;s:4:\"file\";s:26:\"2016/05/foto-carrossel.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-carrossel-180x300.png\";s:5:\"width\";i:180;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(35, 67, '_wp_attached_file', '2016/05/foto-club.png'),
(36, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:547;s:6:\"height\";i:619;s:4:\"file\";s:21:\"2016/05/foto-club.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"foto-club-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"foto-club-265x300.png\";s:5:\"width\";i:265;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 68, '_wp_attached_file', '2016/05/foto-contato.png'),
(38, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1135;s:6:\"height\";i:1040;s:4:\"file\";s:24:\"2016/05/foto-contato.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-contato-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-contato-300x275.png\";s:5:\"width\";i:300;s:6:\"height\";i:275;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"foto-contato-768x704.png\";s:5:\"width\";i:768;s:6:\"height\";i:704;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"foto-contato-1024x938.png\";s:5:\"width\";i:1024;s:6:\"height\";i:938;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:24:\"foto-contato-600x550.png\";s:5:\"width\";i:600;s:6:\"height\";i:550;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(39, 69, '_wp_attached_file', '2016/05/foto-destaque.png'),
(40, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1074;s:6:\"height\";i:936;s:4:\"file\";s:25:\"2016/05/foto-destaque.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"foto-destaque-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"foto-destaque-300x261.png\";s:5:\"width\";i:300;s:6:\"height\";i:261;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"foto-destaque-768x669.png\";s:5:\"width\";i:768;s:6:\"height\";i:669;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"foto-destaque-1024x892.png\";s:5:\"width\";i:1024;s:6:\"height\";i:892;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"foto-destaque-600x523.png\";s:5:\"width\";i:600;s:6:\"height\";i:523;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(41, 70, '_wp_attached_file', '2016/05/foto-destaque-cardapio.png'),
(42, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:693;s:4:\"file\";s:34:\"2016/05/foto-destaque-cardapio.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-300x193.png\";s:5:\"width\";i:300;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-768x493.png\";s:5:\"width\";i:768;s:6:\"height\";i:493;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"foto-destaque-cardapio-1024x657.png\";s:5:\"width\";i:1024;s:6:\"height\";i:657;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:34:\"foto-destaque-cardapio-600x385.png\";s:5:\"width\";i:600;s:6:\"height\";i:385;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(43, 71, '_wp_attached_file', '2016/05/foto-hambuger.png'),
(44, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1074;s:6:\"height\";i:1111;s:4:\"file\";s:25:\"2016/05/foto-hambuger.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-290x300.png\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-768x794.png\";s:5:\"width\";i:768;s:6:\"height\";i:794;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"foto-hambuger-990x1024.png\";s:5:\"width\";i:990;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-600x621.png\";s:5:\"width\";i:600;s:6:\"height\";i:621;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(45, 72, '_wp_attached_file', '2016/05/foto-perfil.png'),
(46, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:806;s:6:\"height\";i:623;s:4:\"file\";s:23:\"2016/05/foto-perfil.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"foto-perfil-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"foto-perfil-300x232.png\";s:5:\"width\";i:300;s:6:\"height\";i:232;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"foto-perfil-768x594.png\";s:5:\"width\";i:768;s:6:\"height\";i:594;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:23:\"foto-perfil-600x464.png\";s:5:\"width\";i:600;s:6:\"height\";i:464;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(47, 73, '_wp_attached_file', '2016/05/foto-postagem.png'),
(48, 73, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:806;s:6:\"height\";i:310;s:4:\"file\";s:25:\"2016/05/foto-postagem.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"foto-postagem-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"foto-postagem-300x115.png\";s:5:\"width\";i:300;s:6:\"height\";i:115;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"foto-postagem-768x295.png\";s:5:\"width\";i:768;s:6:\"height\";i:295;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"foto-postagem-600x231.png\";s:5:\"width\";i:600;s:6:\"height\";i:231;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(49, 74, '_wp_attached_file', '2016/05/foto-quem-somos.png'),
(50, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:406;s:6:\"height\";i:610;s:4:\"file\";s:27:\"2016/05/foto-quem-somos.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"foto-quem-somos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"foto-quem-somos-200x300.png\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(51, 75, '_wp_attached_file', '2016/05/foto-quem-somos1.png'),
(52, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:656;s:6:\"height\";i:439;s:4:\"file\";s:28:\"2016/05/foto-quem-somos1.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"foto-quem-somos1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"foto-quem-somos1-300x201.png\";s:5:\"width\";i:300;s:6:\"height\";i:201;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:28:\"foto-quem-somos1-600x402.png\";s:5:\"width\";i:600;s:6:\"height\";i:402;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(53, 76, '_wp_attached_file', '2016/05/foto-quem-somos3.png'),
(54, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:442;s:6:\"height\";i:309;s:4:\"file\";s:28:\"2016/05/foto-quem-somos3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"foto-quem-somos3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"foto-quem-somos3-300x210.png\";s:5:\"width\";i:300;s:6:\"height\";i:210;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(55, 77, '_wp_attached_file', '2016/05/fundo-forme.png'),
(56, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:632;s:6:\"height\";i:714;s:4:\"file\";s:23:\"2016/05/fundo-forme.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"fundo-forme-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"fundo-forme-266x300.png\";s:5:\"width\";i:266;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:23:\"fundo-forme-600x678.png\";s:5:\"width\";i:600;s:6:\"height\";i:678;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(57, 78, '_wp_attached_file', '2016/05/fundo-logo.png'),
(58, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1074;s:6:\"height\";i:356;s:4:\"file\";s:22:\"2016/05/fundo-logo.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"fundo-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"fundo-logo-300x99.png\";s:5:\"width\";i:300;s:6:\"height\";i:99;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"fundo-logo-768x255.png\";s:5:\"width\";i:768;s:6:\"height\";i:255;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"fundo-logo-1024x339.png\";s:5:\"width\";i:1024;s:6:\"height\";i:339;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"fundo-logo-600x199.png\";s:5:\"width\";i:600;s:6:\"height\";i:199;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(59, 79, '_wp_attached_file', '2016/05/fundo-menu.png'),
(60, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:145;s:6:\"height\";i:211;s:4:\"file\";s:22:\"2016/05/fundo-menu.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"fundo-menu-145x150.png\";s:5:\"width\";i:145;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(61, 80, '_wp_attached_file', '2016/05/logo.png'),
(62, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:213;s:6:\"height\";i:104;s:4:\"file\";s:16:\"2016/05/logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x104.png\";s:5:\"width\";i:150;s:6:\"height\";i:104;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(63, 81, '_wp_attached_file', '2016/05/logo-branca.png'),
(64, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:133;s:4:\"file\";s:23:\"2016/05/logo-branca.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"logo-branca-150x133.png\";s:5:\"width\";i:150;s:6:\"height\";i:133;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"logo-branca-300x133.png\";s:5:\"width\";i:300;s:6:\"height\";i:133;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(65, 82, '_wp_attached_file', '2016/05/logo-branca-oficial.png'),
(66, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:173;s:6:\"height\";i:85;s:4:\"file\";s:31:\"2016/05/logo-branca-oficial.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"logo-branca-oficial-150x85.png\";s:5:\"width\";i:150;s:6:\"height\";i:85;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(67, 83, '_wp_attached_file', '2016/05/logo-marca-preta.png'),
(68, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:447;s:6:\"height\";i:197;s:4:\"file\";s:28:\"2016/05/logo-marca-preta.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"logo-marca-preta-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"logo-marca-preta-300x132.png\";s:5:\"width\";i:300;s:6:\"height\";i:132;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(69, 84, '_wp_attached_file', '2016/05/logo-preta.png'),
(70, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:479;s:6:\"height\";i:211;s:4:\"file\";s:22:\"2016/05/logo-preta.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"logo-preta-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"logo-preta-300x132.png\";s:5:\"width\";i:300;s:6:\"height\";i:132;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(71, 85, '_wp_attached_file', '2016/05/palupaLogo.png'),
(72, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:897;s:6:\"height\";i:279;s:4:\"file\";s:22:\"2016/05/palupaLogo.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"palupaLogo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"palupaLogo-300x93.png\";s:5:\"width\";i:300;s:6:\"height\";i:93;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"palupaLogo-768x239.png\";s:5:\"width\";i:768;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"palupaLogo-600x187.png\";s:5:\"width\";i:600;s:6:\"height\";i:187;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(73, 86, '_wp_attached_file', '2016/05/palupaLogoCinza.png'),
(74, 86, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:897;s:6:\"height\";i:279;s:4:\"file\";s:27:\"2016/05/palupaLogoCinza.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"palupaLogoCinza-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"palupaLogoCinza-300x93.png\";s:5:\"width\";i:300;s:6:\"height\";i:93;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"palupaLogoCinza-768x239.png\";s:5:\"width\";i:768;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:27:\"palupaLogoCinza-600x187.png\";s:5:\"width\";i:600;s:6:\"height\";i:187;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(75, 87, '_wp_attached_file', '2016/05/prato-promocoes.png'),
(76, 87, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1074;s:6:\"height\";i:429;s:4:\"file\";s:27:\"2016/05/prato-promocoes.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"prato-promocoes-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"prato-promocoes-300x120.png\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"prato-promocoes-768x307.png\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"prato-promocoes-1024x409.png\";s:5:\"width\";i:1024;s:6:\"height\";i:409;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:27:\"prato-promocoes-600x240.png\";s:5:\"width\";i:600;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(77, 37, '_thumbnail_id', '325'),
(78, 89, '_edit_last', '1'),
(79, 89, '_edit_lock', '1490296302:1'),
(80, 89, '_thumbnail_id', '340'),
(81, 90, '_edit_last', '1'),
(82, 90, '_edit_lock', '1490120040:1'),
(83, 90, '_thumbnail_id', '68'),
(84, 91, '_edit_last', '1'),
(85, 91, '_edit_lock', '1490296271:1'),
(86, 91, '_thumbnail_id', '318'),
(144, 114, '_edit_last', '1'),
(146, 114, '_edit_lock', '1490124772:1'),
(147, 114, '_wp_page_template', 'pages/quem-somos.php'),
(165, 122, '_edit_last', '1'),
(166, 122, '_edit_lock', '1490125275:1'),
(167, 122, '_thumbnail_id', '74'),
(168, 122, 'Fishnchips_integrante_funcao', 'Chef e marujo que faz acontecer.'),
(169, 122, '_dp_original', '118'),
(170, 123, '_edit_last', '1'),
(171, 123, '_edit_lock', '1490125257:1'),
(172, 123, '_thumbnail_id', '76'),
(173, 123, 'Fishnchips_integrante_funcao', 'Nutricionista e maruja atenciosa.'),
(174, 123, '_dp_original', '117'),
(175, 124, '_edit_last', '1'),
(176, 124, '_edit_lock', '1490125237:1'),
(177, 124, '_thumbnail_id', '75'),
(178, 124, 'Fishnchips_integrante_funcao', 'Gerente e marujo prestativo.  '),
(179, 124, '_dp_original', '119'),
(180, 125, '_edit_last', '1'),
(181, 125, '_edit_lock', '1490126032:1'),
(182, 125, '_thumbnail_id', '74'),
(183, 125, 'Fishnchips_integrante_funcao', 'Operação e marujo analista. '),
(184, 125, '_dp_original', '118'),
(185, 126, '_edit_last', '1'),
(186, 126, '_edit_lock', '1490126032:1'),
(187, 126, '_thumbnail_id', '74'),
(188, 126, 'Fishnchips_integrante_funcao', 'Marketing e marujo estratégico.'),
(189, 126, '_dp_original', '117'),
(190, 127, '_edit_last', '1'),
(191, 127, '_edit_lock', '1490041138:1'),
(192, 127, '_wp_page_template', 'pages/contato.php'),
(193, 127, '_thumbnail_id', '68'),
(194, 129, '_form', '<div class=\"form\"><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">nome</label>[text* Nome id:form-control class:form-control placeholder \"Nome\"]\n</div><div class=\"form-group\"><label class=\"hidden\" for=\"telefone\">telefone</label>[tel* Telefone id:form-control class:form-control placeholder \"Telefone\"]\n</div><div class=\"form-group\"><label class=\"hidden\" for=\"email\">email</label>[email* email id:form-control class:form-control placeholder \"Email\"]</div>\n<div class=\"form-group area-mesagem\"><label class=\"hidden\" for=\"mensagem\">mensagem</label>[textarea* Mensagem id:mensagem class:mesagem placeholder \"Mensagem\"]\n</div>[submit id:enviar class:enviar \"Enviar\"]</div>'),
(195, 129, '_mail', 'a:8:{s:7:\"subject\";s:76:\"Mensagem enviada pelo formulario de Assistência técnica do site fishnchips\";s:6:\"sender\";s:33:\"[Nome] <fishnchips@palupa.com.br>\";s:4:\"body\";s:158:\"De:[Nome]\nEmail:[email]\nTelefone:[Telefone]\nCorpo da mensagem:\n[Mensagem]\n\n--\nEste e-mail foi enviado de um formulário de Assistência técnica em fishnchips\";s:9:\"recipient\";s:98:\"christiano.cornehl@pierdovictor.com.br,gerencia2@pierdovictor.com.br,francisco@pierdovictor.com.br\";s:18:\"additional_headers\";s:7:\"[email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(196, 129, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"Fish n\' Chips \"[your-subject]\"\";s:6:\"sender\";s:40:\"Fish n\' Chips <fishnchips@palupa.com.br>\";s:4:\"body\";s:144:\"Corpo da mensagem:\n[your-message]\n\n--\nEste e-mail foi enviado de um formulário de contato em Fish n&#039; Chips (http://fishnchips.pixd.com.br)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:34:\"Reply-To: fishnchips@palupa.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(197, 129, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:34:\"A mensagem foi enviada com sucesso\";s:12:\"mail_sent_ng\";s:85:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\";s:16:\"validation_error\";s:70:\"Um ou mais campos têm um erro. Por favor verifique e tente novamente.\";s:4:\"spam\";s:86:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde..\";s:12:\"accept_terms\";s:74:\"Você deve aceitar os termos e condições antes de enviar a sua mensagem.\";s:16:\"invalid_required\";s:25:\"O campo é obrigatório .\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:52:\"Formato da data que o remetente digitou é inválido\";s:14:\"date_too_early\";s:48:\"A data é mais adiantada do que o limite mínimo\";s:13:\"date_too_late\";s:43:\"A data é posterior do que o limite máximo\";s:13:\"upload_failed\";s:47:\"O upload de um arquivo falhou por alguma razão\";s:24:\"upload_file_type_invalid\";s:42:\"Uploaded file is not allowed for file type\";s:21:\"upload_file_too_large\";s:31:\"Arquivo enviado é muito grande\";s:23:\"upload_failed_php_error\";s:47:\"O upload de um arquivo falhou por alguma razão\";s:14:\"invalid_number\";s:55:\"Formato de número que o remetente digitou é inválido\";s:16:\"number_too_small\";s:40:\"Número é menor do que o limite mínimo\";s:16:\"number_too_large\";s:40:\"Número é maior do que o limite máximo\";s:23:\"quiz_answer_not_correct\";s:53:\"Você não inseriu a resposta correta para a pergunta\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:56:\"Endereço de e-mail que o remetente digitou é inválido\";s:11:\"invalid_url\";s:40:\"URL que o remetente digitou é inválido\";s:11:\"invalid_tel\";s:56:\"Número de telefone que o remetente digitou é inválido\";}'),
(198, 129, '_additional_settings', ''),
(199, 129, '_locale', 'pt_BR'),
(236, 138, '_edit_last', '1'),
(237, 138, '_wp_page_template', 'pages/clube.php'),
(238, 138, '_edit_lock', '1490128447:1'),
(242, 141, '_wp_attached_file', '2016/06/calendario.svg'),
(243, 142, '_wp_attached_file', '2016/06/cherry-top.svg'),
(244, 143, '_wp_attached_file', '2016/06/circular-faces.svg'),
(245, 144, '_wp_attached_file', '2016/06/crab-brancos.svg'),
(246, 145, '_wp_attached_file', '2016/06/discount-label.svg'),
(247, 146, '_wp_attached_file', '2016/06/duascarinhas.svg'),
(248, 147, '_wp_attached_file', '2016/06/giftbox.svg'),
(249, 148, '_wp_attached_file', '2016/06/hamburger-branco.svg'),
(250, 149, '_wp_attached_file', '2016/06/hamburger-meal.svg'),
(251, 150, '_wp_attached_file', '2016/06/hot-fish-bone.svg'),
(252, 151, '_wp_attached_file', '2016/06/hot-fish-branco.svg'),
(253, 152, '_wp_attached_file', '2016/06/info.svg'),
(254, 153, '_wp_attached_file', '2016/06/knife-and-fork-azul.svg'),
(255, 154, '_wp_attached_file', '2016/06/porcentagem.svg'),
(256, 155, '_wp_attached_file', '2016/06/prawn.svg'),
(257, 156, '_wp_attached_file', '2016/06/prawn-branco.svg'),
(258, 157, '_wp_attached_file', '2016/06/presente.svg'),
(292, 170, '_form', '<div class=\"form-group\"><label class=\"hidden\" for=\"nome\">nome</label>[text* nome id:place class:form-control class:place placeholder \"Nome\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"tel\">tel</label>[tel* Telefone id:place class:form-control class:place placeholder \"Telefone\"]</div><div class=\"form-group\">\n<label class=\"hidden\" for=\"email\">Email</label>[email* Email id:place class:form-control class:place placeholder \"Email\"]\n</div>[submit id:participar class:participar \"participar\"]'),
(293, 170, '_mail', 'a:8:{s:7:\"subject\";s:49:\"Mensagem enviada pelo formulario Clube fishnchips\";s:6:\"sender\";s:33:\"[Nome] <fishnchips@palupa.com.br>\";s:4:\"body\";s:109:\"De:[nome]\nEmail:[Email]\nTelefone:[Telefone]\n--\nEste e-mail foi enviado de um formulário Clube  em fishnchips\";s:9:\"recipient\";s:98:\"christiano.cornehl@pierdovictor.com.br,gerencia2@pierdovictor.com.br,francisco@pierdovictor.com.br\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(294, 170, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"Fish n\' Chips \"[your-subject]\"\";s:6:\"sender\";s:40:\"Fish n\' Chips <fishnchips@palupa.com.br>\";s:4:\"body\";s:144:\"Corpo da mensagem:\n[your-message]\n\n--\nEste e-mail foi enviado de um formulário de contato em Fish n&#039; Chips (http://fishnchips.pixd.com.br)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:34:\"Reply-To: fishnchips@palupa.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(295, 170, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:34:\"A mensagem foi enviada com sucesso\";s:12:\"mail_sent_ng\";s:85:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\";s:16:\"validation_error\";s:70:\"Um ou mais campos têm um erro. Por favor verifique e tente novamente.\";s:4:\"spam\";s:85:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\";s:12:\"accept_terms\";s:74:\"Você deve aceitar os termos e condições antes de enviar a sua mensagem.\";s:16:\"invalid_required\";s:25:\"O campo é obrigatório .\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:52:\"Formato da data que o remetente digitou é inválido\";s:14:\"date_too_early\";s:48:\"A data é mais adiantada do que o limite mínimo\";s:13:\"date_too_late\";s:42:\"A data é posterior do que o limite máxim\";s:13:\"upload_failed\";s:47:\"O upload de um arquivo falhou por alguma razão\";s:24:\"upload_file_type_invalid\";s:42:\"Uploaded file is not allowed for file type\";s:21:\"upload_file_too_large\";s:31:\"Arquivo enviado é muito grande\";s:23:\"upload_failed_php_error\";s:47:\"O upload de um arquivo falhou por alguma razão\";s:14:\"invalid_number\";s:55:\"Formato de número que o remetente digitou é inválido\";s:16:\"number_too_small\";s:40:\"Número é menor do que o limite mínimo\";s:16:\"number_too_large\";s:40:\"Número é maior do que o limite máximo\";s:23:\"quiz_answer_not_correct\";s:53:\"Você não inseriu a resposta correta para a pergunta\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:56:\"Endereço de e-mail que o remetente digitou é inválido\";s:11:\"invalid_url\";s:40:\"URL que o remetente digitou é inválido\";s:11:\"invalid_tel\";s:56:\"Número de telefone que o remetente digitou é inválido\";}'),
(296, 170, '_additional_settings', ''),
(297, 170, '_locale', 'pt_BR'),
(307, 172, '_wp_attached_file', '2016/06/hamburger-branco-1.svg'),
(312, 174, '_wp_attached_file', '2016/06/as.png'),
(313, 174, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:14:\"2016/06/as.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"as-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"as-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"as-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:14:\"as-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(316, 175, '_wp_attached_file', '2016/06/duascarinhas-1.svg'),
(322, 177, '_wp_attached_file', '2016/06/s.jpg'),
(323, 177, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:13:\"2016/06/s.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"s-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"s-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:13:\"s-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:13:\"s-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(326, 178, '_wp_attached_file', '2016/06/prawn-1.svg'),
(330, 180, '_wp_attached_file', '2016/06/13238992_885967388197585_7706280536372095337_n.png'),
(331, 180, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:58:\"2016/06/13238992_885967388197585_7706280536372095337_n.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:58:\"13238992_885967388197585_7706280536372095337_n-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:58:\"13238992_885967388197585_7706280536372095337_n-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:58:\"13238992_885967388197585_7706280536372095337_n-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:58:\"13238992_885967388197585_7706280536372095337_n-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(334, 181, '_wp_attached_file', '2016/06/hamburger-branco-2.svg'),
(339, 183, '_wp_attached_file', '2016/06/asasasasa.jpg'),
(340, 183, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:21:\"2016/06/asasasasa.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"asasasasa-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"asasasasa-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"asasasasa-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:21:\"asasasasa-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(343, 184, '_wp_attached_file', '2016/06/duascarinhas-2.svg'),
(348, 186, '_wp_attached_file', '2016/06/sssss.jpg'),
(349, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:480;s:4:\"file\";s:17:\"2016/06/sssss.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"sssss-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"sssss-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(352, 187, '_wp_attached_file', '2016/06/prawn-2.svg'),
(355, 188, '_wp_attached_file', '2016/06/crab-brancos-1.svg'),
(357, 189, '_wp_attached_file', '2016/06/crab-brancos-2.svg'),
(359, 190, '_wp_attached_file', '2016/06/prawn-branco-1.svg'),
(361, 191, '_wp_attached_file', '2016/06/hot-fish-branco-1.svg'),
(363, 192, '_wp_attached_file', '2016/06/hamburger-branco-3.svg'),
(365, 193, '_wp_attached_file', '2016/06/crab-brancos-3.svg'),
(367, 194, '_wp_attached_file', '2016/06/prawn-branco-2.svg'),
(369, 195, '_wp_attached_file', '2016/06/hamburger-branco-4.svg'),
(371, 196, '_wp_attached_file', '2016/06/prawn-branco-3.svg'),
(375, 197, '_wp_attached_file', '2016/06/crab-brancos-4.svg'),
(377, 198, '_wp_attached_file', '2016/06/prawn-branco-4.svg'),
(379, 199, '_wp_attached_file', '2016/06/hot-fish-branco-2.svg'),
(381, 200, '_wp_attached_file', '2016/06/hamburger-branco-5.svg'),
(383, 201, '_wp_attached_file', '2016/06/crab-brancos-5.svg'),
(385, 202, '_edit_last', '1'),
(386, 202, '_edit_lock', '1490125981:1'),
(387, 202, '_wp_page_template', 'pages/promocoes.php'),
(388, 206, '_wp_attached_file', '2016/06/sd.png'),
(389, 206, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:14:\"2016/06/sd.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"sd-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"sd-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"sd-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:14:\"sd-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(393, 207, '_wp_attached_file', '2016/06/hamburger-branco-6.svg'),
(399, 209, '_wp_attached_file', '2016/06/b.png'),
(400, 209, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:13:\"2016/06/b.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"b-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"b-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:13:\"b-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:13:\"b-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(403, 210, '_wp_attached_file', '2016/06/prawn-branco-5.svg'),
(408, 212, '_wp_attached_file', '2016/06/ZZZ.png'),
(409, 212, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:15:\"2016/06/ZZZ.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"ZZZ-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"ZZZ-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"ZZZ-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:15:\"ZZZ-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(412, 213, '_wp_attached_file', '2016/06/hamburger-branco-7.svg'),
(415, 214, '_edit_last', '1'),
(416, 214, '_edit_lock', '1490279110:1'),
(417, 214, '_thumbnail_id', '322');
INSERT INTO `fc_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(419, 215, '_edit_last', '1'),
(421, 215, '_edit_lock', '1490298278:1'),
(422, 214, '_yoast_wpseo_primary_categoriaCardapio', '10'),
(423, 215, '_thumbnail_id', '318'),
(424, 214, '_dp_original', '211'),
(425, 215, 'Fishnchips_prato_preco', 'De R$ 24,9 Por R$ 20'),
(427, 215, '_yoast_wpseo_primary_categoriaCardapio', '9'),
(428, 215, '_dp_original', '208'),
(429, 217, '_edit_last', '1'),
(430, 217, '_edit_lock', '1490280434:1'),
(431, 217, '_thumbnail_id', '321'),
(432, 217, 'Fishnchips_prato_preco', 'De R$ 29,9 Por R$ 25'),
(433, 217, '_yoast_wpseo_primary_categoriaCardapio', '11'),
(436, 217, '_dp_original', '185'),
(458, 220, '_edit_last', '1'),
(459, 220, '_edit_lock', '1490295788:1'),
(460, 220, '_yoast_wpseo_primary_categoriaCardapio', '12'),
(461, 220, '_thumbnail_id', '320'),
(462, 220, 'Fishnchips_prato_preco', 'De R$ 29, Por R$ 25'),
(464, 220, '_dp_original', '176'),
(507, 227, '_edit_last', '1'),
(508, 227, '_edit_lock', '1490282183:1'),
(509, 228, '_edit_last', '1'),
(510, 228, '_edit_lock', '1490282182:1'),
(511, 229, '_edit_last', '1'),
(512, 229, '_edit_lock', '1490281913:1'),
(513, 230, '_edit_last', '1'),
(514, 230, '_edit_lock', '1490282181:1'),
(515, 230, '_yoast_wpseo_primary_categoriaClube', '15'),
(516, 229, '_yoast_wpseo_primary_categoriaClube', '15'),
(517, 228, '_yoast_wpseo_primary_categoriaClube', '15'),
(518, 227, '_yoast_wpseo_primary_categoriaClube', '15'),
(519, 235, '_wp_attached_file', '2016/06/cherry-top-1.svg'),
(521, 236, '_wp_attached_file', '2016/06/prawn-3.svg'),
(523, 237, '_wp_attached_file', '2016/06/presente-1.svg'),
(525, 238, '_wp_attached_file', '2016/06/porcentagem-1.svg'),
(527, 239, '_wp_attached_file', '2016/06/hot-fish-bone-1.svg'),
(529, 240, '_wp_attached_file', '2016/06/giftbox-1.svg'),
(530, 230, 'Fishnchips_club_beneficio', '240'),
(531, 241, '_wp_attached_file', '2016/06/duascarinhas-3.svg'),
(533, 242, '_wp_attached_file', '2016/06/prawn-4.svg'),
(659, 213, '_edit_lock', '1472146844:1'),
(660, 236, '_edit_lock', '1472147081:1'),
(670, 278, '_edit_last', '1'),
(671, 278, '_edit_lock', '1490279275:1'),
(672, 278, '_thumbnail_id', '323'),
(673, 278, 'Fishnchips_prato_preco', 'De 29,50 Por R$ 25,00'),
(674, 279, '_wp_attached_file', '2016/08/hamburger-branco-7.svg'),
(676, 278, '_yoast_wpseo_primary_categoriaCardapio', '7'),
(677, 280, '_edit_last', '1'),
(678, 280, '_edit_lock', '1490124891:1'),
(679, 280, '_thumbnail_id', '72'),
(680, 280, 'Fishnchips_integrante_funcao', 'Gestão e marujo visionário.'),
(681, 281, '_edit_last', '1'),
(682, 281, '_edit_lock', '1490128632:1'),
(684, 282, '_wp_attached_file', '2016/08/prawn-3.svg'),
(686, 281, '_yoast_wpseo_primary_categoriaClube', '13'),
(691, 170, '_config_errors', 'a:2:{s:11:\"mail.sender\";i:103;s:23:\"mail.additional_headers\";i:102;}'),
(692, 129, '_config_errors', 'a:2:{s:11:\"mail.sender\";i:103;s:23:\"mail.additional_headers\";i:102;}'),
(705, 91, '_wp_old_slug', 'arroz-de-camarao-com-rucula__trashed'),
(706, 90, '_wp_old_slug', 'conheca__trashed'),
(707, 89, '_wp_old_slug', 'salada-irlandesa__trashed'),
(708, 37, '_wp_old_slug', 'destaque-1__trashed'),
(712, 91, '_wp_old_slug', 'arroz-de-camarao-com-rucula'),
(713, 91, '_yoast_wpseo_content_score', '60'),
(714, 37, '_yoast_wpseo_content_score', '60'),
(715, 90, '_yoast_wpseo_content_score', '60'),
(716, 90, '_wp_old_slug', 'conheca'),
(717, 89, '_wp_old_slug', 'salada-irlandesa'),
(718, 89, '_yoast_wpseo_content_score', '30'),
(805, 278, '_wp_old_slug', 'novo-prato__trashed'),
(806, 278, '_wp_old_slug', 'novo-prato'),
(807, 278, '_yoast_wpseo_content_score', '60'),
(808, 215, '_wp_old_slug', 'ora-uma-batata-ora-um-peixe-2__trashed'),
(809, 215, '_wp_old_slug', 'ora-uma-batata-ora-um-peixe-2'),
(810, 215, '_yoast_wpseo_content_score', '60'),
(811, 214, '_wp_old_slug', 'pao-de-limao-hamburguer-2__trashed'),
(812, 214, '_wp_old_slug', 'pao-de-limao-hamburguer-2'),
(813, 214, '_yoast_wpseo_content_score', '60'),
(815, 217, '_wp_old_slug', 'almoco-2__trashed'),
(816, 217, '_yoast_wpseo_content_score', '60'),
(817, 220, '_wp_old_slug', 'bolinhos-de-bacalhau-2__trashed'),
(818, 220, '_wp_old_slug', 'bolinhos-de-bacalhau-2'),
(819, 220, '_yoast_wpseo_content_score', '60'),
(820, 217, '_wp_old_slug', 'almoco-2'),
(821, 286, '_edit_last', '1'),
(822, 286, '_edit_lock', '1490293922:1'),
(823, 287, '_wp_attached_file', '2017/03/96a961b229b14e759bb7d69f387fb4aa.png'),
(824, 287, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:474;s:6:\"height\";i:147;s:4:\"file\";s:44:\"2017/03/96a961b229b14e759bb7d69f387fb4aa.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"96a961b229b14e759bb7d69f387fb4aa-150x147.png\";s:5:\"width\";i:150;s:6:\"height\";i:147;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:43:\"96a961b229b14e759bb7d69f387fb4aa-300x93.png\";s:5:\"width\";i:300;s:6:\"height\";i:93;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(825, 286, '_thumbnail_id', '287'),
(826, 286, 'Fishnchips_prato_preco', 'De R$ 19,5 Por R$ 16,5'),
(827, 286, '_yoast_wpseo_primary_categoriaCardapio', '20'),
(828, 286, '_yoast_wpseo_content_score', '60'),
(829, 227, '_wp_old_slug', 'beneficios-b'),
(830, 227, '_yoast_wpseo_content_score', '30'),
(831, 228, '_wp_old_slug', 'beneficios-c'),
(832, 228, '_yoast_wpseo_content_score', '30'),
(833, 229, '_yoast_wpseo_content_score', '60'),
(834, 230, '_yoast_wpseo_content_score', '60'),
(862, 114, '_yoast_wpseo_content_score', '60'),
(863, 280, '_yoast_wpseo_content_score', '30'),
(864, 292, '_wp_attached_file', '2016/06/1381308_10152262216923895_21848970_n.jpg'),
(865, 292, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:260;s:6:\"height\";i:556;s:4:\"file\";s:48:\"2016/06/1381308_10152262216923895_21848970_n.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:48:\"1381308_10152262216923895_21848970_n-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:48:\"1381308_10152262216923895_21848970_n-140x300.jpg\";s:5:\"width\";i:140;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(866, 126, '_wp_old_slug', 'marcos-de-souza-da-silva-3'),
(867, 126, '_yoast_wpseo_content_score', '60'),
(868, 125, '_yoast_wpseo_content_score', '60'),
(869, 124, '_yoast_wpseo_content_score', '60'),
(870, 123, '_yoast_wpseo_content_score', '60'),
(871, 122, '_yoast_wpseo_content_score', '60'),
(884, 202, '_yoast_wpseo_content_score', '60'),
(885, 278, '_wp_old_slug', 'spaghetti-com-camarao'),
(886, 214, '_wp_old_slug', 'penne-com-salmao'),
(888, 295, '_edit_last', '1'),
(889, 295, '_edit_lock', '1490293916:1'),
(890, 295, '_thumbnail_id', '287'),
(891, 295, '_yoast_wpseo_primary_categoriaCardapio', '24'),
(892, 295, '_yoast_wpseo_content_score', '30'),
(893, 296, '_wp_attached_file', '2017/03/foto-hambuger.png'),
(894, 296, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1074;s:6:\"height\";i:1111;s:4:\"file\";s:25:\"2017/03/foto-hambuger.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-290x300.png\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-768x794.png\";s:5:\"width\";i:768;s:6:\"height\";i:794;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"foto-hambuger-990x1024.png\";s:5:\"width\";i:990;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"foto-hambuger-600x621.png\";s:5:\"width\";i:600;s:6:\"height\";i:621;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(896, 217, '_wp_old_slug', 'promocao-de-quinta-feira'),
(897, 295, 'Fishnchips_prato_preco', '.'),
(898, 214, 'Fishnchips_prato_preco', 'R$ 25,50'),
(899, 297, '_edit_last', '1'),
(900, 297, '_edit_lock', '1490298238:1'),
(901, 297, '_thumbnail_id', '324'),
(902, 297, 'Fishnchips_prato_preco', 'R$ 19,90'),
(903, 297, '_yoast_wpseo_primary_categoriaCardapio', '2'),
(904, 297, '_yoast_wpseo_content_score', '30'),
(905, 298, '_edit_last', '1'),
(906, 298, '_edit_lock', '1490298255:1'),
(907, 298, '_thumbnail_id', '328'),
(908, 298, '_yoast_wpseo_primary_categoriaCardapio', ''),
(909, 298, '_yoast_wpseo_content_score', '60'),
(910, 299, '_edit_last', '1'),
(911, 299, '_edit_lock', '1490275560:1'),
(912, 299, '_thumbnail_id', '317'),
(913, 299, 'Fishnchips_prato_preco', 'R$ 24,9'),
(914, 299, '_yoast_wpseo_primary_categoriaCardapio', ''),
(915, 299, '_yoast_wpseo_content_score', '60'),
(916, 298, 'Fishnchips_prato_preco', 'R$ 27,9'),
(917, 300, '_edit_last', '1'),
(918, 300, '_edit_lock', '1490299269:1'),
(919, 300, '_thumbnail_id', '325'),
(920, 300, 'Fishnchips_prato_preco', 'R$ 19,5'),
(921, 300, '_yoast_wpseo_primary_categoriaCardapio', ''),
(922, 300, '_yoast_wpseo_content_score', '30'),
(923, 301, '_edit_last', '1'),
(924, 301, '_edit_lock', '1490298342:1'),
(925, 301, '_thumbnail_id', '319'),
(926, 301, 'Fishnchips_prato_preco', 'R$ 9,5'),
(927, 301, '_yoast_wpseo_primary_categoriaCardapio', '4'),
(928, 301, '_yoast_wpseo_content_score', '60'),
(929, 302, '_edit_last', '1'),
(930, 302, '_edit_lock', '1490298359:1'),
(931, 302, '_thumbnail_id', '327'),
(932, 302, 'Fishnchips_prato_preco', 'R$ 9,5'),
(933, 302, '_yoast_wpseo_primary_categoriaCardapio', ''),
(934, 302, '_yoast_wpseo_content_score', '60'),
(935, 303, '_edit_last', '1'),
(936, 303, '_edit_lock', '1490298384:1'),
(937, 303, '_thumbnail_id', '186'),
(938, 303, 'Fishnchips_prato_preco', 'R$ 9,5'),
(939, 303, '_yoast_wpseo_primary_categoriaCardapio', ''),
(940, 303, '_yoast_wpseo_content_score', '60'),
(941, 304, '_edit_last', '1'),
(942, 304, '_edit_lock', '1490298402:1'),
(943, 304, '_thumbnail_id', '316'),
(944, 304, 'Fishnchips_prato_preco', 'R$ 25,50'),
(945, 304, '_yoast_wpseo_primary_categoriaCardapio', '2'),
(946, 304, '_yoast_wpseo_content_score', '30'),
(947, 305, '_edit_last', '1'),
(948, 305, '_edit_lock', '1490299275:1'),
(949, 305, '_thumbnail_id', '315'),
(950, 305, 'Fishnchips_prato_preco', 'R$ 18, (6 un.)'),
(951, 305, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(952, 305, '_yoast_wpseo_content_score', '60'),
(953, 306, '_edit_last', '1'),
(954, 306, '_edit_lock', '1490300090:1'),
(955, 306, '_thumbnail_id', '336'),
(956, 306, 'Fishnchips_prato_preco', 'R$ 26,9'),
(957, 306, '_yoast_wpseo_primary_categoriaCardapio', '5'),
(958, 306, '_yoast_wpseo_content_score', '30'),
(959, 138, '_yoast_wpseo_content_score', '60'),
(960, 281, '_yoast_wpseo_content_score', '60'),
(961, 308, '_edit_last', '1'),
(962, 308, '_edit_lock', '1490128645:1'),
(963, 308, '_yoast_wpseo_primary_categoriaClube', '13'),
(964, 308, '_yoast_wpseo_content_score', '60'),
(965, 309, '_edit_last', '1'),
(966, 309, '_edit_lock', '1490206273:1'),
(967, 309, '_yoast_wpseo_primary_categoriaClube', '13'),
(968, 309, '_yoast_wpseo_content_score', '60'),
(969, 310, '_edit_last', '1'),
(970, 310, '_edit_lock', '1490128711:1'),
(971, 310, '_yoast_wpseo_primary_categoriaClube', '14'),
(972, 310, '_yoast_wpseo_content_score', '30'),
(973, 311, '_edit_last', '1'),
(974, 311, '_edit_lock', '1490128771:1'),
(975, 311, '_yoast_wpseo_primary_categoriaClube', '14'),
(976, 311, '_yoast_wpseo_content_score', '30'),
(977, 312, '_edit_last', '1'),
(978, 312, '_edit_lock', '1490282184:1'),
(979, 312, '_yoast_wpseo_primary_categoriaClube', '15'),
(980, 312, '_yoast_wpseo_content_score', '30'),
(1050, 312, '_thumbnail_id', '282'),
(1051, 313, '_wp_attached_file', '2017/03/foto-hambuger-1.png'),
(1052, 313, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1074;s:6:\"height\";i:1111;s:4:\"file\";s:27:\"2017/03/foto-hambuger-1.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"foto-hambuger-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"foto-hambuger-1-290x300.png\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"foto-hambuger-1-768x794.png\";s:5:\"width\";i:768;s:6:\"height\";i:794;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"foto-hambuger-1-990x1024.png\";s:5:\"width\";i:990;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:27:\"foto-hambuger-1-600x621.png\";s:5:\"width\";i:600;s:6:\"height\";i:621;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1056, 312, 'Fishnchips_club_beneficio', '237'),
(1060, 303, 'Fishnchips_prato_icone', '201'),
(1070, 314, '_wp_attached_file', '2017/03/BagueteMignon_Alta-min.jpg'),
(1071, 314, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:34:\"2017/03/BagueteMignon_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"BagueteMignon_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"BagueteMignon_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"BagueteMignon_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"BagueteMignon_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:34:\"BagueteMignon_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1073, 315, '_wp_attached_file', '2017/03/BolinhoBacalhau_Alta-min.jpg'),
(1074, 315, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:36:\"2017/03/BolinhoBacalhau_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"BolinhoBacalhau_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"BolinhoBacalhau_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"BolinhoBacalhau_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"BolinhoBacalhau_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"BolinhoBacalhau_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1076, 316, '_wp_attached_file', '2017/03/Fish-Express-min.jpg'),
(1077, 316, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:28:\"2017/03/Fish-Express-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"Fish-Express-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"Fish-Express-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"Fish-Express-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"Fish-Express-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:28:\"Fish-Express-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1079, 317, '_wp_attached_file', '2017/03/HambúrguerCamarão_Alta-min.jpg'),
(1080, 317, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6017;s:6:\"height\";i:4016;s:4:\"file\";s:40:\"2017/03/HambúrguerCamarão_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"HambúrguerCamarão_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"HambúrguerCamarão_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:40:\"HambúrguerCamarão_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:41:\"HambúrguerCamarão_Alta-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:40:\"HambúrguerCamarão_Alta-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1082, 318, '_wp_attached_file', '2017/03/HambúrguerCamarão2-min.jpg'),
(1083, 318, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:36:\"2017/03/HambúrguerCamarão2-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"HambúrguerCamarão2-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"HambúrguerCamarão2-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"HambúrguerCamarão2-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"HambúrguerCamarão2-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"HambúrguerCamarão2-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1085, 319, '_wp_attached_file', '2017/03/Duetto-min.jpg'),
(1086, 319, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:22:\"2017/03/Duetto-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"Duetto-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"Duetto-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"Duetto-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"Duetto-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"Duetto-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1089, 320, '_wp_attached_file', '2016/06/HambúrguerSalmãoCombo_Alta-min.jpg'),
(1090, 320, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:44:\"2016/06/HambúrguerSalmãoCombo_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"HambúrguerSalmãoCombo_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:44:\"HambúrguerSalmãoCombo_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:44:\"HambúrguerSalmãoCombo_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:45:\"HambúrguerSalmãoCombo_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:44:\"HambúrguerSalmãoCombo_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1092, 321, '_wp_attached_file', '2016/06/PeixeComLegumes_Alta-min.jpg'),
(1093, 321, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:36:\"2016/06/PeixeComLegumes_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"PeixeComLegumes_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"PeixeComLegumes_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"PeixeComLegumes_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"PeixeComLegumes_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"PeixeComLegumes_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1095, 322, '_wp_attached_file', '2016/06/PenneSalmão-min.jpg'),
(1096, 322, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6017;s:6:\"height\";i:4016;s:4:\"file\";s:28:\"2016/06/PenneSalmão-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"PenneSalmão-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"PenneSalmão-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"PenneSalmão-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"PenneSalmão-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:28:\"PenneSalmão-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1100, 323, '_wp_attached_file', '2016/08/SpaghettiCamarão-min.jpg'),
(1101, 323, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6017;s:6:\"height\";i:4016;s:4:\"file\";s:33:\"2016/08/SpaghettiCamarão-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"SpaghettiCamarão-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"SpaghettiCamarão-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"SpaghettiCamarão-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"SpaghettiCamarão-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:33:\"SpaghettiCamarão-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1103, 324, '_wp_attached_file', '2017/03/ArrozSiri_Alta-min.jpg'),
(1104, 324, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6017;s:6:\"height\";i:4016;s:4:\"file\";s:30:\"2017/03/ArrozSiri_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"ArrozSiri_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"ArrozSiri_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"ArrozSiri_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"ArrozSiri_Alta-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:30:\"ArrozSiri_Alta-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1105, 325, '_wp_attached_file', '2017/03/HambúrguerSiri_Alta-min.jpg'),
(1106, 325, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:36:\"2017/03/HambúrguerSiri_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"HambúrguerSiri_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"HambúrguerSiri_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"HambúrguerSiri_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"HambúrguerSiri_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"HambúrguerSiri_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1107, 300, 'Fishnchips_prato_icone', '200'),
(1108, 327, '_wp_attached_file', '2017/03/WrapCamarão_Alta-min.jpg'),
(1109, 327, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:7359;s:6:\"height\";i:4912;s:4:\"file\";s:33:\"2017/03/WrapCamarão_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"WrapCamarão_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"WrapCamarão_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"WrapCamarão_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"WrapCamarão_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:33:\"WrapCamarão_Alta-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1110, 302, 'Fishnchips_prato_icone', '188'),
(1111, 328, '_wp_attached_file', '2017/03/FishNChipsCombo_Alta-min.jpg'),
(1112, 328, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:36:\"2017/03/FishNChipsCombo_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"FishNChipsCombo_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"FishNChipsCombo_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"FishNChipsCombo_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"FishNChipsCombo_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"FishNChipsCombo_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1113, 329, '_edit_last', '1'),
(1114, 329, '_edit_lock', '1490283199:1'),
(1115, 330, '_wp_attached_file', '2017/03/SaladaInglesaLula_Alta-min.jpg'),
(1116, 330, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:38:\"2017/03/SaladaInglesaLula_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"SaladaInglesaLula_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"SaladaInglesaLula_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"SaladaInglesaLula_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:39:\"SaladaInglesaLula_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:38:\"SaladaInglesaLula_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1117, 331, '_wp_attached_file', '2017/03/SaladaIrlandesaLula-min.jpg'),
(1118, 331, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:35:\"2017/03/SaladaIrlandesaLula-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"SaladaIrlandesaLula-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"SaladaIrlandesaLula-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"SaladaIrlandesaLula-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:36:\"SaladaIrlandesaLula-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:35:\"SaladaIrlandesaLula-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1119, 329, '_thumbnail_id', '331'),
(1120, 329, 'Fishnchips_prato_preco', 'R$ 13,9'),
(1121, 329, '_yoast_wpseo_primary_categoriaCardapio', ''),
(1122, 329, '_yoast_wpseo_content_score', '60'),
(1123, 332, '_wp_attached_file', '2017/03/salad-bowl-hand-drawn-food.png'),
(1124, 332, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:38:\"2017/03/salad-bowl-hand-drawn-food.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1128, 333, '_edit_last', '1'),
(1129, 333, '_edit_lock', '1490283197:1'),
(1130, 334, '_wp_attached_file', '2017/03/SaladaInglesaCamarão-min.jpg'),
(1131, 334, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:37:\"2017/03/SaladaInglesaCamarão-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"SaladaInglesaCamarão-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"SaladaInglesaCamarão-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"SaladaInglesaCamarão-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"SaladaInglesaCamarão-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:37:\"SaladaInglesaCamarão-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1132, 333, '_thumbnail_id', '334'),
(1133, 333, 'Fishnchips_prato_preco', 'R$ 15,9'),
(1135, 333, '_yoast_wpseo_primary_categoriaCardapio', ''),
(1136, 333, '_yoast_wpseo_content_score', '60'),
(1138, 335, '_edit_last', '1'),
(1139, 335, '_edit_lock', '1490283133:1'),
(1140, 335, '_thumbnail_id', '314'),
(1141, 335, 'Fishnchips_prato_preco', 'R$ 30,9'),
(1142, 335, '_yoast_wpseo_primary_categoriaCardapio', '5'),
(1143, 335, '_yoast_wpseo_content_score', '60'),
(1144, 336, '_wp_attached_file', '2017/03/BagueteSalmão_Alta-min.jpg'),
(1145, 336, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:7361;s:6:\"height\";i:4912;s:4:\"file\";s:35:\"2017/03/BagueteSalmão_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"BagueteSalmão_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"BagueteSalmão_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"BagueteSalmão_Alta-min-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:36:\"BagueteSalmão_Alta-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:35:\"BagueteSalmão_Alta-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1148, 299, 'Fishnchips_prato_icone', '200'),
(1149, 215, 'Fishnchips_prato_icone', '200'),
(1150, 338, '_edit_last', '1'),
(1151, 338, '_edit_lock', '1490298276:1'),
(1152, 339, '_edit_last', '1'),
(1153, 339, '_edit_lock', '1490298709:1'),
(1154, 340, '_wp_attached_file', '2017/03/BananaPie_Alta-min.jpg'),
(1155, 340, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:30:\"2017/03/BananaPie_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"BananaPie_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"BananaPie_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"BananaPie_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"BananaPie_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:30:\"BananaPie_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1156, 339, '_thumbnail_id', '340'),
(1157, 339, 'Fishnchips_prato_preco', 'R$ 8,5'),
(1158, 339, '_yoast_wpseo_primary_categoriaCardapio', '4'),
(1159, 339, '_yoast_wpseo_content_score', '30'),
(1160, 341, '_wp_attached_file', '2017/03/Hamb£rguerCamar∆oCombo_Alta-min.jpg'),
(1161, 341, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6017;s:6:\"height\";i:4016;s:4:\"file\";s:46:\"2017/03/Hamb£rguerCamar∆oCombo_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:46:\"Hamb£rguerCamar∆oCombo_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:46:\"Hamb£rguerCamar∆oCombo_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:46:\"Hamb£rguerCamar∆oCombo_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:47:\"Hamb£rguerCamar∆oCombo_Alta-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:46:\"Hamb£rguerCamar∆oCombo_Alta-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1162, 338, '_thumbnail_id', '341'),
(1163, 338, 'Fishnchips_prato_preco', 'R$ 37,90'),
(1164, 338, '_yoast_wpseo_primary_categoriaCardapio', '25'),
(1165, 338, '_yoast_wpseo_content_score', '30'),
(1166, 342, '_edit_last', '1'),
(1167, 342, '_edit_lock', '1490283071:1'),
(1168, 343, '_edit_last', '1'),
(1169, 343, '_edit_lock', '1490283057:1'),
(1170, 344, '_wp_attached_file', '2017/03/Pudim-min.jpg'),
(1171, 344, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:21:\"2017/03/Pudim-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"Pudim-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"Pudim-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"Pudim-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"Pudim-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:21:\"Pudim-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1172, 342, '_thumbnail_id', '328'),
(1173, 342, 'Fishnchips_prato_preco', 'R$ 29,90'),
(1174, 342, '_yoast_wpseo_primary_categoriaCardapio', '25'),
(1175, 342, '_yoast_wpseo_content_score', '30'),
(1176, 343, '_thumbnail_id', '344'),
(1177, 343, 'Fishnchips_prato_preco', 'R$ 5,00'),
(1178, 343, '_yoast_wpseo_primary_categoriaCardapio', '4'),
(1179, 343, '_yoast_wpseo_content_score', '30'),
(1180, 345, '_edit_last', '1'),
(1181, 345, '_edit_lock', '1490283001:1'),
(1182, 347, '_wp_attached_file', '2017/03/ComboHambSiri_Altajpg-min.jpg'),
(1183, 347, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:37:\"2017/03/ComboHambSiri_Altajpg-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"ComboHambSiri_Altajpg-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"ComboHambSiri_Altajpg-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"ComboHambSiri_Altajpg-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"ComboHambSiri_Altajpg-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:37:\"ComboHambSiri_Altajpg-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1184, 346, '_edit_last', '1'),
(1185, 346, '_edit_lock', '1490297375:1'),
(1186, 345, '_thumbnail_id', '347'),
(1187, 345, 'Fishnchips_prato_preco', 'R$ 29,00'),
(1188, 345, '_yoast_wpseo_primary_categoriaCardapio', '25'),
(1189, 345, '_yoast_wpseo_content_score', '30'),
(1190, 348, '_edit_last', '1'),
(1191, 348, '_edit_lock', '1490282988:1'),
(1192, 348, '_thumbnail_id', '320'),
(1193, 348, 'Fishnchips_prato_preco', 'R$ 29,00'),
(1194, 348, '_yoast_wpseo_primary_categoriaCardapio', '25'),
(1195, 348, '_yoast_wpseo_content_score', '30'),
(1196, 349, '_edit_last', '1'),
(1197, 349, '_edit_lock', '1490282976:1'),
(1198, 350, '_wp_attached_file', '2017/03/IscasPeixeCombinado_Alta-min.jpg'),
(1199, 350, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6017;s:6:\"height\";i:4016;s:4:\"file\";s:40:\"2017/03/IscasPeixeCombinado_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"IscasPeixeCombinado_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"IscasPeixeCombinado_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:40:\"IscasPeixeCombinado_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:41:\"IscasPeixeCombinado_Alta-min-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:40:\"IscasPeixeCombinado_Alta-min-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1200, 349, '_thumbnail_id', '350'),
(1201, 349, 'Fishnchips_prato_preco', 'R$ 29,90'),
(1202, 349, '_yoast_wpseo_primary_categoriaCardapio', '25'),
(1203, 349, '_yoast_wpseo_content_score', '30'),
(1204, 351, '_edit_last', '1'),
(1205, 351, '_edit_lock', '1490282858:1'),
(1206, 352, '_wp_attached_file', '2017/03/BolinhoSiriCombo_Alta-min.jpg'),
(1207, 353, '_wp_attached_file', '2017/03/BolinhoBacalhau_Alta-min-1.jpg');
INSERT INTO `fc_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1208, 352, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:37:\"2017/03/BolinhoSiriCombo_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"BolinhoSiriCombo_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"BolinhoSiriCombo_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"BolinhoSiriCombo_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"BolinhoSiriCombo_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:37:\"BolinhoSiriCombo_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1209, 353, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:38:\"2017/03/BolinhoBacalhau_Alta-min-1.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"BolinhoBacalhau_Alta-min-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"BolinhoBacalhau_Alta-min-1-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"BolinhoBacalhau_Alta-min-1-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:39:\"BolinhoBacalhau_Alta-min-1-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:38:\"BolinhoBacalhau_Alta-min-1-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1210, 346, '_thumbnail_id', '353'),
(1211, 351, '_thumbnail_id', '352'),
(1212, 346, 'Fishnchips_prato_preco', '(1 un) 9,'),
(1213, 346, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(1214, 346, '_yoast_wpseo_content_score', '30'),
(1215, 351, 'Fishnchips_prato_preco', 'R$ 19,90'),
(1216, 351, '_yoast_wpseo_primary_categoriaCardapio', '25'),
(1217, 351, '_yoast_wpseo_content_score', '30'),
(1218, 354, '_edit_last', '1'),
(1219, 354, '_edit_lock', '1490299276:1'),
(1220, 354, '_thumbnail_id', '353'),
(1221, 354, 'Fishnchips_prato_preco', '(6 un) 18,'),
(1222, 354, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(1223, 354, '_yoast_wpseo_content_score', '30'),
(1224, 355, '_edit_last', '1'),
(1225, 355, '_edit_lock', '1490282618:1'),
(1226, 356, '_edit_last', '1'),
(1227, 356, '_edit_lock', '1490297377:1'),
(1228, 357, '_wp_attached_file', '2017/03/IscasPeixe_Alta-min.jpg'),
(1229, 357, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:31:\"2017/03/IscasPeixe_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"IscasPeixe_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"IscasPeixe_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"IscasPeixe_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"IscasPeixe_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:31:\"IscasPeixe_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1230, 356, '_thumbnail_id', '357'),
(1231, 356, 'Fishnchips_prato_preco', ' (150g)19,'),
(1232, 356, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(1233, 356, '_yoast_wpseo_content_score', '30'),
(1234, 356, '_wp_old_slug', 'espeto-de-peixe'),
(1235, 360, '_wp_attached_file', '2017/03/Strogonoff_Alta-min.jpg'),
(1236, 360, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:31:\"2017/03/Strogonoff_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"Strogonoff_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"Strogonoff_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"Strogonoff_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"Strogonoff_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:31:\"Strogonoff_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1237, 361, '_wp_attached_file', '2017/03/EspetoCamarão_Alta-min.jpg'),
(1238, 361, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:35:\"2017/03/EspetoCamarão_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"EspetoCamarão_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"EspetoCamarão_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"EspetoCamarão_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:36:\"EspetoCamarão_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:35:\"EspetoCamarão_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1239, 359, '_edit_last', '1'),
(1240, 359, '_edit_lock', '1490282596:1'),
(1241, 355, '_thumbnail_id', '360'),
(1242, 355, 'Fishnchips_prato_preco', 'R$ 25,50'),
(1243, 355, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(1244, 355, '_yoast_wpseo_content_score', '60'),
(1245, 359, '_thumbnail_id', '361'),
(1246, 359, 'Fishnchips_prato_preco', 'R$ 12,'),
(1247, 359, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(1248, 359, '_yoast_wpseo_content_score', '30'),
(1249, 364, '_wp_attached_file', '2017/03/MaioneseCamarão-min.jpg'),
(1250, 364, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:32:\"2017/03/MaioneseCamarão-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"MaioneseCamarão-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"MaioneseCamarão-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"MaioneseCamarão-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"MaioneseCamarão-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:32:\"MaioneseCamarão-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1251, 362, '_edit_last', '1'),
(1252, 362, '_edit_lock', '1490282562:1'),
(1253, 362, '_thumbnail_id', '364'),
(1254, 362, 'Fishnchips_prato_preco', 'R$ 11,9'),
(1255, 362, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(1256, 362, '_yoast_wpseo_content_score', '30'),
(1257, 304, 'Fishnchips_prato_icone', '213'),
(1258, 367, '_wp_attached_file', '2017/03/AnelLula_Alta-min.jpg'),
(1259, 367, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:6016;s:6:\"height\";i:4016;s:4:\"file\";s:29:\"2017/03/AnelLula_Alta-min.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"AnelLula_Alta-min-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"AnelLula_Alta-min-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"AnelLula_Alta-min-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"AnelLula_Alta-min-1024x684.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:684;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:29:\"AnelLula_Alta-min-600x401.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1260, 365, '_edit_last', '1'),
(1261, 365, '_edit_lock', '1490282515:1'),
(1262, 365, '_thumbnail_id', '367'),
(1263, 365, 'Fishnchips_prato_preco', 'R$ 16,5'),
(1264, 365, '_yoast_wpseo_primary_categoriaCardapio', '27'),
(1265, 365, '_yoast_wpseo_content_score', '30'),
(1269, 370, '_wp_attached_file', '2017/03/sweet-cake-piece-1.png'),
(1270, 370, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:30:\"2017/03/sweet-cake-piece-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1271, 371, '_wp_attached_file', '2017/03/sweet-cake-piece-2.png'),
(1272, 371, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:30:\"2017/03/sweet-cake-piece-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1273, 372, '_wp_attached_file', '2017/03/one-hamburguer.png'),
(1274, 372, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:26:\"2017/03/one-hamburguer.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1275, 373, '_wp_attached_file', '2017/03/tray.png'),
(1276, 373, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:16:\"2017/03/tray.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1277, 374, '_wp_attached_file', '2017/03/dish-spoon-and-fork.png'),
(1278, 374, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:31:\"2017/03/dish-spoon-and-fork.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1279, 375, '_wp_attached_file', '2017/03/dish-spoon-and-fork-1.png'),
(1280, 375, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:33:\"2017/03/dish-spoon-and-fork-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1281, 376, '_wp_attached_file', '2016/06/spaghetti.png'),
(1282, 376, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:21:\"2016/06/spaghetti.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1283, 214, 'Fishnchips_prato_icone', '376'),
(1284, 377, '_wp_attached_file', '2016/08/spaghetti-1.png'),
(1285, 377, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:23:\"2016/08/spaghetti-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1286, 278, 'Fishnchips_prato_icone', '377'),
(1287, 378, '_wp_attached_file', '2016/06/fast-food.png'),
(1288, 378, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:21:\"2016/06/fast-food.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1290, 379, '_wp_attached_file', '2016/06/salad.png'),
(1291, 379, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:17:\"2016/06/salad.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1293, 217, 'Fishnchips_prato_icone', '379'),
(1294, 380, '_wp_attached_file', '2017/03/wine-glasses.png'),
(1295, 380, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:24:\"2017/03/wine-glasses.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1296, 295, 'Fishnchips_prato_icone', '380'),
(1297, 381, '_wp_attached_file', '2017/03/treasure.png'),
(1298, 381, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:20:\"2017/03/treasure.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1299, 286, 'Fishnchips_prato_icone', '381'),
(1300, 382, '_wp_attached_file', '2016/06/two-wine-bottles.png'),
(1301, 382, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:28:\"2016/06/two-wine-bottles.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1302, 229, 'Fishnchips_club_beneficio', '382'),
(1303, 383, '_wp_attached_file', '2016/06/birthday-card.png'),
(1304, 383, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:25:\"2016/06/birthday-card.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1305, 228, 'Fishnchips_club_beneficio', '383'),
(1306, 384, '_wp_attached_file', '2016/06/payment-method.png'),
(1307, 384, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:26:\"2016/06/payment-method.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1308, 227, 'Fishnchips_club_beneficio', '384'),
(1309, 385, '_wp_attached_file', '2017/03/nachos.png'),
(1310, 385, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:128;s:4:\"file\";s:18:\"2017/03/nachos.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1311, 333, 'Fishnchips_prato_icone', '332'),
(1312, 329, 'Fishnchips_prato_icone', '332'),
(1313, 306, 'Fishnchips_prato_icone', '189'),
(1314, 305, 'Fishnchips_prato_icone', '279'),
(1315, 220, 'Fishnchips_prato_icone', '378'),
(1317, 356, '_wp_old_slug', 'bolinho-de-siri-2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_posts`
--

CREATE TABLE `fc_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_posts`
--

INSERT INTO `fc_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(2, 1, '2016-05-16 11:51:11', '2016-05-16 14:51:11', 'Esta é uma página de exemplo. É diferente de um post porque ela ficará em um local e será exibida na navegação do seu site (na maioria dos temas). A maioria das pessoas começa com uma página de introdução aos potenciais visitantes do site. Ela pode ser assim:\n\n<blockquote>Olá! Eu sou um bike courrier de dia, ator amador à noite e este é meu blog. Eu moro em São Paulo, tenho um cachorro chamado Tonico e eu gosto de caipirinhas. (E de ser pego pela chuva.)</blockquote>\n\nou assim:\n\n<blockquote>A XYZ foi fundada em 1971 e desde então vem proporcionando produtos de qualidade a seus clientes. Localizada em Valinhos, XYZ emprega mais de 2.000 pessoas e faz várias contribuições para a comunidade local.</blockquote>\nComo um novo usuário do WordPress, você deve ir até o <a href=\"http://fishnchips.pixd.com.br/wp-admin/\">seu painel</a> para excluir essa página e criar novas páginas com seu próprio conteúdo. Divirta-se!', 'Página de Exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2016-05-16 11:51:11', '2016-05-16 14:51:11', '', 0, 'http://fishnchips.pixd.com.br/?page_id=2', 0, 'page', '', 0),
(5, 1, '2016-05-16 12:01:04', '2016-05-16 15:01:04', '[wysija_page]', 'Confirmação de assinatura', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2016-05-16 12:01:04', '2016-05-16 15:01:04', '', 0, 'http://fishnchips.pixd.com.br/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(37, 1, '2016-05-16 17:18:54', '2016-05-16 20:18:54', '<span style=\"font-weight: 400;\">Tripulação, a natureza está ao nosso favor: pão de limão, molho tártaro, alface, tomate e o incomparável Hambúrguer de Siri. Diz a lenda, que seu sabor é capaz de matar a fome de centenas de marujos!</span>', 'Hambúrguer de Siri', '', 'publish', 'closed', 'closed', '', 'destaque-1', '', '', '2017-03-23 16:14:30', '2017-03-23 19:14:30', '', 0, 'http://fishnchips.pixd.com.br/?post_type=destaque&#038;p=37', 0, 'destaque', '', 0),
(53, 1, '2016-06-17 09:28:25', '2016-06-17 12:28:25', '', 'foto-destaque-cardapio', '', 'inherit', 'open', 'closed', '', 'foto-destaque-cardapio', '', '', '2016-06-17 09:28:25', '2016-06-17 12:28:25', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/foto-destaque-cardapio.png', 0, 'attachment', 'image/png', 0),
(56, 1, '2016-06-17 10:05:50', '2016-06-17 13:05:50', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2016-06-17 10:05:50', '2016-06-17 13:05:50', '', 0, 'http://fishnchips.pixd.com.br/?page_id=56', 0, 'page', '', 0),
(58, 1, '2016-06-17 10:05:50', '2016-06-17 13:05:50', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2016-06-17 10:05:50', '2016-06-17 13:05:50', '', 56, 'http://fishnchips.pixd.com.br/2016/06/17/56-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2016-06-17 10:06:13', '2016-06-17 13:06:13', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2016-06-17 10:06:13', '2016-06-17 13:06:13', '', 0, 'http://fishnchips.pixd.com.br/?page_id=59', 0, 'page', '', 0),
(60, 1, '2016-06-17 10:06:13', '2016-06-17 13:06:13', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2016-06-17 10:06:13', '2016-06-17 13:06:13', '', 59, 'http://fishnchips.pixd.com.br/2016/06/17/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2016-06-17 10:07:10', '2016-06-17 13:07:10', '', 'banner-blog', '', 'inherit', 'open', 'closed', '', 'banner-blog', '', '', '2016-06-17 10:07:10', '2016-06-17 13:07:10', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/banner-blog.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2016-06-17 10:07:12', '2016-06-17 13:07:12', '', 'blog-foto', '', 'inherit', 'open', 'closed', '', 'blog-foto', '', '', '2016-06-17 10:07:12', '2016-06-17 13:07:12', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/blog-foto.png', 0, 'attachment', 'image/png', 0),
(63, 1, '2016-06-17 10:07:13', '2016-06-17 13:07:13', '', 'foto-blog', '', 'inherit', 'open', 'closed', '', 'foto-blog', '', '', '2016-06-17 10:07:13', '2016-06-17 13:07:13', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-blog.png', 0, 'attachment', 'image/png', 0),
(64, 1, '2016-06-17 10:07:14', '2016-06-17 13:07:14', '', 'foto-blogg', '', 'inherit', 'open', 'closed', '', 'foto-blogg', '', '', '2016-06-17 10:07:14', '2016-06-17 13:07:14', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-blogg.png', 0, 'attachment', 'image/png', 0),
(65, 1, '2016-06-17 10:07:15', '2016-06-17 13:07:15', '', 'fotocarrossel', '', 'inherit', 'open', 'closed', '', 'fotocarrossel', '', '', '2016-06-17 10:07:15', '2016-06-17 13:07:15', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/fotocarrossel.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2016-06-17 10:07:17', '2016-06-17 13:07:17', '', 'foto-carrossel', '', 'inherit', 'open', 'closed', '', 'foto-carrossel', '', '', '2016-06-17 10:07:17', '2016-06-17 13:07:17', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-carrossel.png', 0, 'attachment', 'image/png', 0),
(67, 1, '2016-06-17 10:07:18', '2016-06-17 13:07:18', '', 'foto-club', '', 'inherit', 'open', 'closed', '', 'foto-club', '', '', '2016-06-17 10:07:18', '2016-06-17 13:07:18', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-club.png', 0, 'attachment', 'image/png', 0),
(68, 1, '2016-06-17 10:07:20', '2016-06-17 13:07:20', '', 'foto-contato', '', 'inherit', 'open', 'closed', '', 'foto-contato', '', '', '2016-06-17 10:07:20', '2016-06-17 13:07:20', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-contato.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2016-06-17 10:07:23', '2016-06-17 13:07:23', '', 'foto-destaque', '', 'inherit', 'open', 'closed', '', 'foto-destaque', '', '', '2016-06-17 10:07:23', '2016-06-17 13:07:23', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-destaque.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2016-06-17 10:07:25', '2016-06-17 13:07:25', '', 'foto-destaque-cardapio', '', 'inherit', 'open', 'closed', '', 'foto-destaque-cardapio-2', '', '', '2016-06-17 10:07:25', '2016-06-17 13:07:25', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-destaque-cardapio.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2016-06-17 10:07:27', '2016-06-17 13:07:27', '', 'foto-hambuger', '', 'inherit', 'open', 'closed', '', 'foto-hambuger', '', '', '2016-06-17 10:07:27', '2016-06-17 13:07:27', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-hambuger.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2016-06-17 10:07:30', '2016-06-17 13:07:30', '', 'foto-perfil', '', 'inherit', 'open', 'closed', '', 'foto-perfil', '', '', '2016-06-17 10:07:30', '2016-06-17 13:07:30', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-perfil.png', 0, 'attachment', 'image/png', 0),
(73, 1, '2016-06-17 10:07:31', '2016-06-17 13:07:31', '', 'foto-postagem', '', 'inherit', 'open', 'closed', '', 'foto-postagem', '', '', '2016-06-17 10:07:31', '2016-06-17 13:07:31', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-postagem.png', 0, 'attachment', 'image/png', 0),
(74, 1, '2016-06-17 10:07:33', '2016-06-17 13:07:33', '', 'foto-quem-somos', '', 'inherit', 'open', 'closed', '', 'foto-quem-somos', '', '', '2016-06-17 10:07:33', '2016-06-17 13:07:33', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-quem-somos.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2016-06-17 10:07:35', '2016-06-17 13:07:35', '', 'foto-quem-somos1', '', 'inherit', 'open', 'closed', '', 'foto-quem-somos1', '', '', '2016-06-17 10:07:35', '2016-06-17 13:07:35', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-quem-somos1.png', 0, 'attachment', 'image/png', 0),
(76, 1, '2016-06-17 10:07:37', '2016-06-17 13:07:37', '', 'foto-quem-somos3', '', 'inherit', 'open', 'closed', '', 'foto-quem-somos3', '', '', '2016-06-17 10:07:37', '2016-06-17 13:07:37', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/foto-quem-somos3.png', 0, 'attachment', 'image/png', 0),
(77, 1, '2016-06-17 10:07:38', '2016-06-17 13:07:38', '', 'fundo-forme', '', 'inherit', 'open', 'closed', '', 'fundo-forme', '', '', '2016-06-17 10:07:38', '2016-06-17 13:07:38', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/fundo-forme.png', 0, 'attachment', 'image/png', 0),
(78, 1, '2016-06-17 10:07:40', '2016-06-17 13:07:40', '', 'fundo-logo', '', 'inherit', 'open', 'closed', '', 'fundo-logo', '', '', '2016-06-17 10:07:40', '2016-06-17 13:07:40', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/fundo-logo.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2016-06-17 10:07:42', '2016-06-17 13:07:42', '', 'fundo-menu', '', 'inherit', 'open', 'closed', '', 'fundo-menu', '', '', '2016-06-17 10:07:42', '2016-06-17 13:07:42', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/fundo-menu.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2016-06-17 10:07:43', '2016-06-17 13:07:43', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2016-06-17 10:07:43', '2016-06-17 13:07:43', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2016-06-17 10:07:44', '2016-06-17 13:07:44', '', 'logo-branca', '', 'inherit', 'open', 'closed', '', 'logo-branca', '', '', '2016-06-17 10:07:44', '2016-06-17 13:07:44', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-branca.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2016-06-17 10:07:46', '2016-06-17 13:07:46', '', 'logo-branca-oficial', '', 'inherit', 'open', 'closed', '', 'logo-branca-oficial', '', '', '2016-06-17 10:07:46', '2016-06-17 13:07:46', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-branca-oficial.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2016-06-17 10:07:47', '2016-06-17 13:07:47', '', 'logo-marca-preta', '', 'inherit', 'open', 'closed', '', 'logo-marca-preta', '', '', '2016-06-17 10:07:47', '2016-06-17 13:07:47', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-marca-preta.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2016-06-17 10:07:48', '2016-06-17 13:07:48', '', 'logo-preta', '', 'inherit', 'open', 'closed', '', 'logo-preta', '', '', '2016-06-17 10:07:48', '2016-06-17 13:07:48', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/logo-preta.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2016-06-17 10:07:49', '2016-06-17 13:07:49', '', 'palupaLogo', '', 'inherit', 'open', 'closed', '', 'palupalogo', '', '', '2016-06-17 10:07:49', '2016-06-17 13:07:49', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/palupaLogo.png', 0, 'attachment', 'image/png', 0),
(86, 1, '2016-06-17 10:07:50', '2016-06-17 13:07:50', '', 'palupaLogoCinza', '', 'inherit', 'open', 'closed', '', 'palupalogocinza', '', '', '2016-06-17 10:07:50', '2016-06-17 13:07:50', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/palupaLogoCinza.png', 0, 'attachment', 'image/png', 0),
(87, 1, '2016-06-17 10:07:52', '2016-06-17 13:07:52', '', 'prato-promocoes', '', 'inherit', 'open', 'closed', '', 'prato-promocoes', '', '', '2016-06-17 10:07:52', '2016-06-17 13:07:52', '', 37, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/05/prato-promocoes.png', 0, 'attachment', 'image/png', 0),
(88, 1, '2016-06-17 10:08:58', '2016-06-17 13:08:58', 'O Victor Fish \'n\' Chips traz o conhecido hambúrguer de siri á sua mesa , para degustar um dos melhores sanduíches com frutos do mar em Curitiba', 'O HAMBURGUER DE SIRI', '', 'inherit', 'closed', 'closed', '', '37-autosave-v1', '', '', '2016-06-17 10:08:58', '2016-06-17 13:08:58', '', 37, 'http://fishnchips.pixd.com.br/2016/06/17/37-autosave-v1/', 0, 'revision', '', 0),
(89, 1, '2016-06-17 10:09:59', '2016-06-17 13:09:59', '<span style=\"font-weight: 400;\">Alguém a viu a exuberante torta recheada com banana, doce de leite, chocolate meio amargo e coberta com chocolate branco por aí? Sim, estou falando com vocês, Marujos Famintos!</span>', 'Banana Pie', '', 'publish', 'closed', 'closed', '', 'banana-pie', '', '', '2017-03-23 16:14:02', '2017-03-23 19:14:02', '', 0, 'http://fishnchips.pixd.com.br/?post_type=destaque&#038;p=89', 0, 'destaque', '', 0),
(90, 1, '2016-06-17 10:11:01', '2016-06-17 13:11:01', '<span style=\"font-weight: 400;\">Delícias ao mar, capitão! Diversas bocas famintas atacam aos sobreviventes: ora uma batata, ora um peixe, ora outra batata, ora outro peixe. O que fazemos? Deixe, marujo. Deixe! </span>', 'Fish ‘n’ Chips', '', 'publish', 'closed', 'closed', '', 'fish-n-chips', '', '', '2017-03-21 15:16:19', '2017-03-21 18:16:19', '', 0, 'http://fishnchips.pixd.com.br/?post_type=destaque&#038;p=90', 0, 'destaque', '', 0),
(91, 1, '2016-06-17 10:12:19', '2016-06-17 13:12:19', '<span style=\"font-weight: 400;\">Hambúrguer de Camarão à vista, marujo! A descoberta das “profundezas” da cozinha do Fish ‘n’ Chips vai ficar frente a frente com a sua fome!</span>', 'Hambúrguer de Camarão', '', 'publish', 'closed', 'closed', '', 'hamburguer-de-camarao', '', '', '2017-03-23 16:13:32', '2017-03-23 19:13:32', '', 0, 'http://fishnchips.pixd.com.br/?post_type=destaque&#038;p=91', 0, 'destaque', '', 0),
(114, 1, '2016-06-17 12:29:19', '2016-06-17 15:29:19', 'Ahoy, tripulação! Lembro-me muito bem de quando ele ancorou sua embarcação na Ilha da Alimentação do Shopping Mueller, em 2012. Quem, capitão? O rebelde Long Victor, marujo faminto e cozinheiro do navio. Dizem que ele enfrentou constantemente, em anos de aprimoramento, uma força maior que ele mesmo: a vontade de comer o que preparava. \r\n\r\n“Por Mil Frutos do Mar Crocantes!” Então essa embarcação pertence a ele, capitão? Yo ho ho... E nas profundezas de sua cozinha dizem que ele sabe o tempo de preparação de seus pratos por meio do cheiro de sua chapa, do balanço de suas espátulas e, principalmente, pelas caras de famintos de seus tripulantes. Victor sabia muito bem que seus “Cabeças de Ostras” gostavam de comer bem sem perder muito tempo.   \r\n\r\nE mais: dizem também que Long Victor ficou desaparecido por mil dias e mil noites e, quando voltou, bradava em voz bem alta, apoiado no mastro de seu navio construído com peixe e fritas: Casual Dinning, Casual Dinning... A partir daí, começou a servir coisas incríveis, ágeis, sustentáveis, mas sem perder o velho e conhecido temperamento de marujo.\r\n\r\nCoragem, tripulantes, e experimentai os pratos que nos esperam. Os ventos estão ao nosso favor... Digo, o cheirinho dos frutos do mar.', 'Quem somos', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2017-03-21 16:30:37', '2017-03-21 19:30:37', '', 0, 'http://fishnchips.pixd.com.br/?page_id=114', 0, 'page', '', 0),
(115, 1, '2016-06-17 12:29:19', '2016-06-17 15:29:19', '', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2016-06-17 12:29:19', '2016-06-17 15:29:19', '', 114, 'http://fishnchips.pixd.com.br/2016/06/17/114-revision-v1/', 0, 'revision', '', 0),
(116, 1, '2016-06-17 12:31:05', '2016-06-17 15:31:05', 'Lorem Ipsum adalah text contoh digunakan didalam industri pencetakan dan typesetting. Lorem Ipsum telah menjadi text contoh semenjak tahun ke 1500an, apabila pencetak yang kurang terkenal mengambil sebuah galeri cetak industri pencetakan dan typesetting. Lorem Ipsum telah menjadi text contoh semenjak tahun ke 1500an, apabila pencetak yang kurang terkenal mengambil sebuah galeri ceta industri pencetakan dan typesetting. Lorem Ipsum telah menjadi text contoh semenjak tahun ke 1500an, apabila pencetak yang kurang terkenal mengambil sebuah galeri ceta industri pencetakan dan typesetting. Lorem Ipsum telah menjadi text contoh semenjak tahun ke 1500an, apabila pencetak yang kurang terkenal mengambil sebuah galeri ceta dan merobakanya menjadi satu buku spesimen. Ia telah bertahan bukan hanya selama lima kurun, tetapi telah melonjak ke era typesetting elektronik, dengan tiada perubahan ketara. Ia telah dipopularkan pada tahun 1960an dengan penerbitan Letraset yang mebawa kangungan Lorem Ipsum, dan lebih terkini dengan sofwer pencetakan desktop seperti Aldus PageMaker yang telah menyertakan satu versi Lorem Ipsum.', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2016-06-17 12:31:05', '2016-06-17 15:31:05', '', 114, 'http://fishnchips.pixd.com.br/2016/06/17/114-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2016-06-17 12:47:33', '2016-06-17 15:47:33', 'Praesent at facilisis metus. Morbi ultricies lorem sollicitudin turpis hendrerit fermentum. Nullam facilisis felis eget eros varius sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus quis augue sed augue faucibus auctor', 'Fernando', '', 'publish', 'closed', 'closed', '', 'roberto-2', '', '', '2017-03-21 16:43:34', '2017-03-21 19:43:34', '', 0, 'http://fishnchips.pixd.com.br/?post_type=equipe&#038;p=122', 0, 'equipe', '', 0),
(123, 1, '2016-06-17 12:47:35', '2016-06-17 15:47:35', 'Praesent at facilisis metus. Morbi ultricies lorem sollicitudin turpis hendrerit fermentum. Nullam facilisis felis eget eros varius sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus quis augue sed augue faucibus auctor', 'Valéria', '', 'publish', 'closed', 'closed', '', 'marcos-de-souza-da-silva-2', '', '', '2017-03-21 16:43:15', '2017-03-21 19:43:15', '', 0, 'http://fishnchips.pixd.com.br/?post_type=equipe&#038;p=123', 0, 'equipe', '', 0),
(124, 1, '2016-06-17 12:47:37', '2016-06-17 15:47:37', 'Praesent at facilisis metus. Morbi ultricies lorem sollicitudin turpis hendrerit fermentum. Nullam facilisis felis eget eros varius sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus quis augue sed augue faucibus auctor', 'Mauro', '', 'publish', 'closed', 'closed', '', 'diego-3', '', '', '2017-03-21 16:42:53', '2017-03-21 19:42:53', '', 0, 'http://fishnchips.pixd.com.br/?post_type=equipe&#038;p=124', 0, 'equipe', '', 0),
(125, 1, '2016-06-17 12:47:39', '2016-06-17 15:47:39', 'Praesent at facilisis metus. Morbi ultricies lorem sollicitudin turpis hendrerit fermentum. Nullam facilisis felis eget eros varius sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus quis augue sed augue faucibus auctor', 'Francisco', '', 'publish', 'closed', 'closed', '', 'roberto-3', '', '', '2017-03-21 16:42:19', '2017-03-21 19:42:19', '', 0, 'http://fishnchips.pixd.com.br/?post_type=equipe&#038;p=125', 0, 'equipe', '', 0),
(126, 1, '2016-06-17 12:47:41', '2016-06-17 15:47:41', 'Praesent at facilisis metus. Morbi ultricies lorem sollicitudin turpis hendrerit fermentum. Nullam facilisis felis eget eros varius sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus quis augue sed augue faucibus auctor', 'Christiano', '', 'publish', 'closed', 'closed', '', 'christiano', '', '', '2017-03-21 16:44:37', '2017-03-21 19:44:37', '', 0, 'http://fishnchips.pixd.com.br/?post_type=equipe&#038;p=126', 0, 'equipe', '', 0),
(127, 1, '2016-06-17 14:23:55', '2016-06-17 17:23:55', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato-fish-n-chips', '', '', '2017-03-20 17:13:31', '2017-03-20 20:13:31', '', 0, 'http://fishnchips.pixd.com.br/?page_id=127', 0, 'page', '', 0),
(128, 1, '2016-06-17 14:23:55', '2016-06-17 17:23:55', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2016-06-17 14:23:55', '2016-06-17 17:23:55', '', 127, 'http://fishnchips.pixd.com.br/2016/06/17/127-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2016-06-17 14:41:06', '2016-06-17 17:41:06', '<div class=\"form\"><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">nome</label>[text* Nome id:form-control class:form-control placeholder \"Nome\"]\r\n</div><div class=\"form-group\"><label class=\"hidden\" for=\"telefone\">telefone</label>[tel* Telefone id:form-control class:form-control placeholder \"Telefone\"]\r\n</div><div class=\"form-group\"><label class=\"hidden\" for=\"email\">email</label>[email* email id:form-control class:form-control placeholder \"Email\"]</div>\r\n<div class=\"form-group area-mesagem\"><label class=\"hidden\" for=\"mensagem\">mensagem</label>[textarea* Mensagem id:mensagem class:mesagem placeholder \"Mensagem\"]\r\n</div>[submit id:enviar class:enviar \"Enviar\"]</div>\nMensagem enviada pelo formulario de Assistência técnica do site fishnchips\n[Nome] <fishnchips@palupa.com.br>\nDe:[Nome]\r\nEmail:[email]\r\nTelefone:[Telefone]\r\nCorpo da mensagem:\r\n[Mensagem]\r\n\r\n--\r\nEste e-mail foi enviado de um formulário de Assistência técnica em fishnchips\nchristiano.cornehl@pierdovictor.com.br,gerencia2@pierdovictor.com.br,francisco@pierdovictor.com.br\n[email]\n\n\n\n\nFish n\' Chips \"[your-subject]\"\nFish n\' Chips <fishnchips@palupa.com.br>\nCorpo da mensagem:\r\n[your-message]\r\n\r\n--\r\nEste e-mail foi enviado de um formulário de contato em Fish n&#039; Chips (http://fishnchips.pixd.com.br)\n[your-email]\nReply-To: fishnchips@palupa.com.br\n\n\n\nA mensagem foi enviada com sucesso\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\nUm ou mais campos têm um erro. Por favor verifique e tente novamente.\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde..\nVocê deve aceitar os termos e condições antes de enviar a sua mensagem.\nO campo é obrigatório .\nO campo é muito longo.\nO campo é muito curto.\nFormato da data que o remetente digitou é inválido\nA data é mais adiantada do que o limite mínimo\nA data é posterior do que o limite máximo\nO upload de um arquivo falhou por alguma razão\nUploaded file is not allowed for file type\nArquivo enviado é muito grande\nO upload de um arquivo falhou por alguma razão\nFormato de número que o remetente digitou é inválido\nNúmero é menor do que o limite mínimo\nNúmero é maior do que o limite máximo\nVocê não inseriu a resposta correta para a pergunta\nO código digitado está incorreto.\nEndereço de e-mail que o remetente digitou é inválido\nURL que o remetente digitou é inválido\nNúmero de telefone que o remetente digitou é inválido', 'Formulário de contato', '', 'publish', 'closed', 'closed', '', 'formulario-de-contato', '', '', '2017-03-20 16:38:19', '2017-03-20 19:38:19', '', 0, 'http://fishnchips.pixd.com.br/?post_type=wpcf7_contact_form&#038;p=129', 0, 'wpcf7_contact_form', '', 0),
(138, 1, '2016-06-17 15:32:41', '2016-06-17 18:32:41', 'Eis que fazer parte da tripulação nunca foi tão fácil, marujo. Tenha benefícios exclusivos, sendo um fiel tripulante da embarcação Fish ‘n’ Chips. \r\n', 'Clube', '', 'publish', 'closed', 'closed', '', 'clube-fishn-chips', '', '', '2017-03-21 17:34:13', '2017-03-21 20:34:13', '', 0, 'http://fishnchips.pixd.com.br/?page_id=138', 0, 'page', '', 0),
(139, 1, '2016-06-17 15:32:41', '2016-06-17 18:32:41', '', 'Clube Fishn Chips', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2016-06-17 15:32:41', '2016-06-17 18:32:41', '', 138, 'http://fishnchips.pixd.com.br/138-revision-v1/', 0, 'revision', '', 0),
(141, 1, '2016-06-17 16:23:14', '2016-06-17 19:23:14', '', 'calendario', '', 'inherit', 'open', 'closed', '', 'calendario', '', '', '2016-06-17 16:23:14', '2016-06-17 19:23:14', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/calendario.svg', 0, 'attachment', 'image/svg+xml', 0),
(142, 1, '2016-06-17 16:23:15', '2016-06-17 19:23:15', '', 'cherry top', '', 'inherit', 'open', 'closed', '', 'cherry-top', '', '', '2016-06-17 16:23:15', '2016-06-17 19:23:15', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/cherry-top.svg', 0, 'attachment', 'image/svg+xml', 0),
(143, 1, '2016-06-17 16:23:16', '2016-06-17 19:23:16', '', 'circular-faces', '', 'inherit', 'open', 'closed', '', 'circular-faces', '', '', '2016-06-17 16:23:16', '2016-06-17 19:23:16', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/circular-faces.svg', 0, 'attachment', 'image/svg+xml', 0),
(144, 1, '2016-06-17 16:23:17', '2016-06-17 19:23:17', '', 'crab-brancos', '', 'inherit', 'open', 'closed', '', 'crab-brancos', '', '', '2016-06-17 16:23:17', '2016-06-17 19:23:17', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/crab-brancos.svg', 0, 'attachment', 'image/svg+xml', 0),
(145, 1, '2016-06-17 16:23:18', '2016-06-17 19:23:18', '', 'discount-label', '', 'inherit', 'open', 'closed', '', 'discount-label', '', '', '2016-06-17 16:23:18', '2016-06-17 19:23:18', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/discount-label.svg', 0, 'attachment', 'image/svg+xml', 0),
(146, 1, '2016-06-17 16:23:19', '2016-06-17 19:23:19', '', 'duascarinhas', '', 'inherit', 'open', 'closed', '', 'duascarinhas', '', '', '2016-06-17 16:23:19', '2016-06-17 19:23:19', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/duascarinhas.svg', 0, 'attachment', 'image/svg+xml', 0),
(147, 1, '2016-06-17 16:23:20', '2016-06-17 19:23:20', '', 'giftbox', '', 'inherit', 'open', 'closed', '', 'giftbox', '', '', '2016-06-17 16:23:20', '2016-06-17 19:23:20', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/giftbox.svg', 0, 'attachment', 'image/svg+xml', 0),
(148, 1, '2016-06-17 16:23:21', '2016-06-17 19:23:21', '', 'hamburger-branco', '', 'inherit', 'open', 'closed', '', 'hamburger-branco', '', '', '2016-06-17 16:23:21', '2016-06-17 19:23:21', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco.svg', 0, 'attachment', 'image/svg+xml', 0),
(149, 1, '2016-06-17 16:23:22', '2016-06-17 19:23:22', '', 'hamburger-meal', '', 'inherit', 'open', 'closed', '', 'hamburger-meal', '', '', '2016-06-17 16:23:22', '2016-06-17 19:23:22', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-meal.svg', 0, 'attachment', 'image/svg+xml', 0),
(150, 1, '2016-06-17 16:23:23', '2016-06-17 19:23:23', '', 'hot-fish-bone', '', 'inherit', 'open', 'closed', '', 'hot-fish-bone', '', '', '2016-06-17 16:23:23', '2016-06-17 19:23:23', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hot-fish-bone.svg', 0, 'attachment', 'image/svg+xml', 0),
(151, 1, '2016-06-17 16:23:23', '2016-06-17 19:23:23', '', 'hot-fish-branco', '', 'inherit', 'open', 'closed', '', 'hot-fish-branco', '', '', '2016-06-17 16:23:23', '2016-06-17 19:23:23', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hot-fish-branco.svg', 0, 'attachment', 'image/svg+xml', 0),
(152, 1, '2016-06-17 16:23:24', '2016-06-17 19:23:24', '', 'info', '', 'inherit', 'open', 'closed', '', 'info', '', '', '2016-06-17 16:23:24', '2016-06-17 19:23:24', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/info.svg', 0, 'attachment', 'image/svg+xml', 0),
(153, 1, '2016-06-17 16:23:25', '2016-06-17 19:23:25', '', 'knife-and-fork-azul', '', 'inherit', 'open', 'closed', '', 'knife-and-fork-azul', '', '', '2016-06-17 16:23:25', '2016-06-17 19:23:25', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/knife-and-fork-azul.svg', 0, 'attachment', 'image/svg+xml', 0),
(154, 1, '2016-06-17 16:23:26', '2016-06-17 19:23:26', '', 'porcentagem', '', 'inherit', 'open', 'closed', '', 'porcentagem', '', '', '2016-06-17 16:23:26', '2016-06-17 19:23:26', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/porcentagem.svg', 0, 'attachment', 'image/svg+xml', 0),
(155, 1, '2016-06-17 16:23:27', '2016-06-17 19:23:27', '', 'prawn', '', 'inherit', 'open', 'closed', '', 'prawn', '', '', '2016-06-17 16:23:27', '2016-06-17 19:23:27', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn.svg', 0, 'attachment', 'image/svg+xml', 0),
(156, 1, '2016-06-17 16:23:28', '2016-06-17 19:23:28', '', 'prawn-branco', '', 'inherit', 'open', 'closed', '', 'prawn-branco', '', '', '2016-06-17 16:23:28', '2016-06-17 19:23:28', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-branco.svg', 0, 'attachment', 'image/svg+xml', 0),
(157, 1, '2016-06-17 16:23:29', '2016-06-17 19:23:29', '', 'presente', '', 'inherit', 'open', 'closed', '', 'presente', '', '', '2016-06-17 16:23:29', '2016-06-17 19:23:29', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/presente.svg', 0, 'attachment', 'image/svg+xml', 0),
(167, 1, '2016-06-17 16:44:35', '2016-06-17 19:44:35', 'Torquent per conubia nostra lorem ipsum dolor amet per inceptos himenaeos', 'Clube Fishn Chips', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2016-06-17 16:44:35', '2016-06-17 19:44:35', '', 138, 'http://fishnchips.pixd.com.br/138-revision-v1/', 0, 'revision', '', 0),
(168, 1, '2016-06-17 16:44:52', '2016-06-17 19:44:52', 'Torquent per conubia nostra lorem ipsum dolor amet per inceptos himenaeos', 'Conheça Nosso Clube', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2016-06-17 16:44:52', '2016-06-17 19:44:52', '', 138, 'http://fishnchips.pixd.com.br/138-revision-v1/', 0, 'revision', '', 0),
(169, 1, '2016-06-17 16:45:57', '2016-06-17 19:45:57', 'Torquent per conubia nostra lorem ipsum dolor amet per inceptos himenaeos', 'Clube', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2016-06-17 16:45:57', '2016-06-17 19:45:57', '', 138, 'http://fishnchips.pixd.com.br/138-revision-v1/', 0, 'revision', '', 0),
(170, 1, '2016-06-17 16:58:08', '2016-06-17 19:58:08', '<div class=\"form-group\"><label class=\"hidden\" for=\"nome\">nome</label>[text* nome id:place class:form-control class:place placeholder \"Nome\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"tel\">tel</label>[tel* Telefone id:place class:form-control class:place placeholder \"Telefone\"]</div><div class=\"form-group\">\r\n<label class=\"hidden\" for=\"email\">Email</label>[email* Email id:place class:form-control class:place placeholder \"Email\"]\r\n</div>[submit id:participar class:participar \"participar\"]\nMensagem enviada pelo formulario Clube fishnchips\n[Nome] <fishnchips@palupa.com.br>\nDe:[nome]\r\nEmail:[Email]\r\nTelefone:[Telefone]\r\n--\r\nEste e-mail foi enviado de um formulário Clube  em fishnchips\nchristiano.cornehl@pierdovictor.com.br,gerencia2@pierdovictor.com.br,francisco@pierdovictor.com.br\nReply-To: [your-email]\n\n\n\n\nFish n\' Chips \"[your-subject]\"\nFish n\' Chips <fishnchips@palupa.com.br>\nCorpo da mensagem:\r\n[your-message]\r\n\r\n--\r\nEste e-mail foi enviado de um formulário de contato em Fish n&#039; Chips (http://fishnchips.pixd.com.br)\n[your-email]\nReply-To: fishnchips@palupa.com.br\n\n\n\nA mensagem foi enviada com sucesso\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\nUm ou mais campos têm um erro. Por favor verifique e tente novamente.\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar a sua mensagem.\nO campo é obrigatório .\nO campo é muito longo.\nO campo é muito curto.\nFormato da data que o remetente digitou é inválido\nA data é mais adiantada do que o limite mínimo\nA data é posterior do que o limite máxim\nO upload de um arquivo falhou por alguma razão\nUploaded file is not allowed for file type\nArquivo enviado é muito grande\nO upload de um arquivo falhou por alguma razão\nFormato de número que o remetente digitou é inválido\nNúmero é menor do que o limite mínimo\nNúmero é maior do que o limite máximo\nVocê não inseriu a resposta correta para a pergunta\nO código digitado está incorreto.\nEndereço de e-mail que o remetente digitou é inválido\nURL que o remetente digitou é inválido\nNúmero de telefone que o remetente digitou é inválido', 'Formulário página Clube', '', 'publish', 'closed', 'closed', '', 'formulario-pagina-clube', '', '', '2017-03-20 16:38:14', '2017-03-20 19:38:14', '', 0, 'http://fishnchips.pixd.com.br/?post_type=wpcf7_contact_form&#038;p=170', 0, 'wpcf7_contact_form', '', 0),
(172, 1, '2016-06-20 12:07:26', '2016-06-20 15:07:26', '', 'hamburger-branco-1', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-1', '', '', '2016-06-20 12:07:26', '2016-06-20 15:07:26', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(174, 1, '2016-06-20 12:10:40', '2016-06-20 15:10:40', '', 'as', '', 'inherit', 'open', 'closed', '', 'as', '', '', '2016-06-20 12:10:40', '2016-06-20 15:10:40', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/as.png', 0, 'attachment', 'image/png', 0),
(175, 1, '2016-06-20 12:11:04', '2016-06-20 15:11:04', '', 'duascarinhas-1', '', 'inherit', 'open', 'closed', '', 'duascarinhas-1', '', '', '2016-06-20 12:11:04', '2016-06-20 15:11:04', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/duascarinhas-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(177, 1, '2016-06-20 12:12:50', '2016-06-20 15:12:50', '', 's', '', 'inherit', 'open', 'closed', '', 's', '', '', '2016-06-20 12:12:50', '2016-06-20 15:12:50', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/s.jpg', 0, 'attachment', 'image/jpeg', 0),
(178, 1, '2016-06-20 12:13:15', '2016-06-20 15:13:15', '', 'prawn-1', '', 'inherit', 'open', 'closed', '', 'prawn-1', '', '', '2016-06-20 12:13:15', '2016-06-20 15:13:15', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(180, 1, '2016-06-20 12:14:29', '2016-06-20 15:14:29', '', '13238992_885967388197585_7706280536372095337_n', '', 'inherit', 'open', 'closed', '', '13238992_885967388197585_7706280536372095337_n', '', '', '2016-06-20 12:14:29', '2016-06-20 15:14:29', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/13238992_885967388197585_7706280536372095337_n.png', 0, 'attachment', 'image/png', 0),
(181, 1, '2016-06-20 12:14:47', '2016-06-20 15:14:47', '', 'hamburger-branco-2', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-2', '', '', '2016-06-20 12:14:47', '2016-06-20 15:14:47', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(183, 1, '2016-06-20 12:25:27', '2016-06-20 15:25:27', '', 'asasasasa', '', 'inherit', 'open', 'closed', '', 'asasasasa', '', '', '2016-06-20 12:25:27', '2016-06-20 15:25:27', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/asasasasa.jpg', 0, 'attachment', 'image/jpeg', 0),
(184, 1, '2016-06-20 12:25:31', '2016-06-20 15:25:31', '', 'duascarinhas-2', '', 'inherit', 'open', 'closed', '', 'duascarinhas-2', '', '', '2016-06-20 12:25:31', '2016-06-20 15:25:31', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/duascarinhas-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(186, 1, '2016-06-20 12:27:19', '2016-06-20 15:27:19', '', 'sssss', '', 'inherit', 'open', 'closed', '', 'sssss', '', '', '2016-06-20 12:27:19', '2016-06-20 15:27:19', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/sssss.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 1, '2016-06-20 12:27:22', '2016-06-20 15:27:22', '', 'prawn-2', '', 'inherit', 'open', 'closed', '', 'prawn-2', '', '', '2016-06-20 12:27:22', '2016-06-20 15:27:22', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(188, 1, '2016-06-20 13:24:36', '2016-06-20 16:24:36', '', 'crab-brancos-1', '', 'inherit', 'open', 'closed', '', 'crab-brancos-1', '', '', '2016-06-20 13:24:36', '2016-06-20 16:24:36', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/crab-brancos-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(189, 1, '2016-06-20 13:24:47', '2016-06-20 16:24:47', '', 'crab-brancos-2', '', 'inherit', 'open', 'closed', '', 'crab-brancos-2', '', '', '2016-06-20 13:24:47', '2016-06-20 16:24:47', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/crab-brancos-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(190, 1, '2016-06-20 13:25:04', '2016-06-20 16:25:04', '', 'prawn-branco-1', '', 'inherit', 'open', 'closed', '', 'prawn-branco-1', '', '', '2016-06-20 13:25:04', '2016-06-20 16:25:04', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-branco-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(191, 1, '2016-06-20 13:25:15', '2016-06-20 16:25:15', '', 'hot-fish-branco-1', '', 'inherit', 'open', 'closed', '', 'hot-fish-branco-1', '', '', '2016-06-20 13:25:15', '2016-06-20 16:25:15', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hot-fish-branco-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(192, 1, '2016-06-20 13:25:27', '2016-06-20 16:25:27', '', 'hamburger-branco-3', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-3', '', '', '2016-06-20 13:25:27', '2016-06-20 16:25:27', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(193, 1, '2016-06-20 13:25:42', '2016-06-20 16:25:42', '', 'crab-brancos-3', '', 'inherit', 'open', 'closed', '', 'crab-brancos-3', '', '', '2016-06-20 13:25:42', '2016-06-20 16:25:42', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/crab-brancos-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(194, 1, '2016-06-20 13:25:48', '2016-06-20 16:25:48', '', 'prawn-branco-2', '', 'inherit', 'open', 'closed', '', 'prawn-branco-2', '', '', '2016-06-20 13:25:48', '2016-06-20 16:25:48', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-branco-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(195, 1, '2016-06-20 13:27:03', '2016-06-20 16:27:03', '', 'hamburger-branco-4', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-4', '', '', '2016-06-20 13:27:03', '2016-06-20 16:27:03', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco-4.svg', 0, 'attachment', 'image/svg+xml', 0),
(196, 1, '2016-06-20 13:31:50', '2016-06-20 16:31:50', '', 'prawn-branco-3', '', 'inherit', 'open', 'closed', '', 'prawn-branco-3', '', '', '2016-06-20 13:31:50', '2016-06-20 16:31:50', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-branco-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(197, 1, '2016-06-20 14:34:36', '2016-06-20 17:34:36', '', 'crab-brancos-4', '', 'inherit', 'open', 'closed', '', 'crab-brancos-4', '', '', '2016-06-20 14:34:36', '2016-06-20 17:34:36', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/crab-brancos-4.svg', 0, 'attachment', 'image/svg+xml', 0),
(198, 1, '2016-06-20 14:36:11', '2016-06-20 17:36:11', '', 'prawn-branco-4', '', 'inherit', 'open', 'closed', '', 'prawn-branco-4', '', '', '2016-06-20 14:36:11', '2016-06-20 17:36:11', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-branco-4.svg', 0, 'attachment', 'image/svg+xml', 0),
(199, 1, '2016-06-20 14:36:18', '2016-06-20 17:36:18', '', 'hot-fish-branco-2', '', 'inherit', 'open', 'closed', '', 'hot-fish-branco-2', '', '', '2016-06-20 14:36:18', '2016-06-20 17:36:18', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hot-fish-branco-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(200, 1, '2016-06-20 14:36:24', '2016-06-20 17:36:24', '', 'hamburger-branco-5', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-5', '', '', '2016-06-20 14:36:24', '2016-06-20 17:36:24', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco-5.svg', 0, 'attachment', 'image/svg+xml', 0),
(201, 1, '2016-06-20 14:36:31', '2016-06-20 17:36:31', '', 'crab-brancos-5', '', 'inherit', 'open', 'closed', '', 'crab-brancos-5', '', '', '2016-06-20 14:36:31', '2016-06-20 17:36:31', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/crab-brancos-5.svg', 0, 'attachment', 'image/svg+xml', 0),
(202, 1, '2016-06-20 15:20:13', '2016-06-20 18:20:13', 'O cheiro nasce na segunda e desaparece na sexta, marujos! De quê, capitão? Yo ho ho, dos <strong>Pratos do Dia</strong>, rapazes.', 'Promoções', '', 'publish', 'closed', 'closed', '', 'promocoes', '', '', '2017-03-21 16:54:47', '2017-03-21 19:54:47', '', 0, 'http://fishnchips.pixd.com.br/?page_id=202', 0, 'page', '', 0),
(203, 1, '2016-06-20 15:20:13', '2016-06-20 18:20:13', '', 'Promoções', '', 'inherit', 'closed', 'closed', '', '202-revision-v1', '', '', '2016-06-20 15:20:13', '2016-06-20 18:20:13', '', 202, 'http://fishnchips.pixd.com.br/202-revision-v1/', 0, 'revision', '', 0),
(204, 1, '2016-06-20 15:22:24', '2016-06-20 18:22:24', 'Duis congue aliquam porttitor. Nam vel dolor scelerisque tellus pharetra aliquam.', 'Promoções', '', 'inherit', 'closed', 'closed', '', '202-revision-v1', '', '', '2016-06-20 15:22:24', '2016-06-20 18:22:24', '', 202, 'http://fishnchips.pixd.com.br/202-revision-v1/', 0, 'revision', '', 0),
(206, 1, '2016-06-20 17:24:38', '2016-06-20 20:24:38', '', 'sd', '', 'inherit', 'open', 'closed', '', 'sd', '', '', '2016-06-20 17:24:38', '2016-06-20 20:24:38', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/sd.png', 0, 'attachment', 'image/png', 0),
(207, 1, '2016-06-20 17:25:07', '2016-06-20 20:25:07', '', 'hamburger-branco-6', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-6', '', '', '2016-06-20 17:25:07', '2016-06-20 20:25:07', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco-6.svg', 0, 'attachment', 'image/svg+xml', 0),
(209, 1, '2016-06-20 17:27:08', '2016-06-20 20:27:08', '', 'b', '', 'inherit', 'open', 'closed', '', 'b', '', '', '2016-06-20 17:27:08', '2016-06-20 20:27:08', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/b.png', 0, 'attachment', 'image/png', 0),
(210, 1, '2016-06-20 17:27:13', '2016-06-20 20:27:13', '', 'prawn-branco-5', '', 'inherit', 'open', 'closed', '', 'prawn-branco-5', '', '', '2016-06-20 17:27:13', '2016-06-20 20:27:13', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-branco-5.svg', 0, 'attachment', 'image/svg+xml', 0),
(212, 1, '2016-06-20 17:29:47', '2016-06-20 20:29:47', '', 'ZZZ', '', 'inherit', 'open', 'closed', '', 'zzz', '', '', '2016-06-20 17:29:47', '2016-06-20 20:29:47', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/ZZZ.png', 0, 'attachment', 'image/png', 0),
(213, 1, '2016-06-20 17:29:51', '2016-06-20 20:29:51', '', 'hamburger-branco-7', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-7', '', '', '2016-06-20 17:29:51', '2016-06-20 20:29:51', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-branco-7.svg', 0, 'attachment', 'image/svg+xml', 0),
(214, 1, '2016-06-20 17:34:36', '2016-06-20 20:34:36', 'Na compra do Prato do Dia, ganhe um Pudim de Leite.\r\n\r\nPromoção válida apenas para as quartas-feiras, durante o horário de atendimento do Fish ‘n’ Chips, marujos!', 'Penne de Salmão', '', 'publish', 'closed', 'closed', '', 'penne-de-salmao', '', '', '2017-03-23 11:25:58', '2017-03-23 14:25:58', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=214', 0, 'cardapio', '', 0),
(215, 1, '2016-06-20 17:34:38', '2016-06-20 20:34:38', 'Hambúrguer de Camarão à vista! Alface, tomate, cream cheese e molho london sauce no pão artesanal de iogurte. ', 'Hambúrguer de Camarão', '', 'publish', 'closed', 'closed', '', 'hamburguer-de-camarao', '', '', '2017-03-23 10:23:55', '2017-03-23 13:23:55', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=215', 0, 'cardapio', '', 0),
(217, 1, '2016-06-20 17:34:34', '2016-06-20 20:34:34', 'Preço válido apenas para as quintas-feiras, durante o horário de atendimento do Fish ‘n’ Chips, marujos!', 'Peixe com Legumes.', '', 'publish', 'closed', 'closed', '', 'peixe-com-legumes', '', '', '2017-03-23 11:47:25', '2017-03-23 14:47:25', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=217', 0, 'cardapio', '', 0),
(220, 1, '2016-06-20 17:34:26', '2016-06-20 20:34:26', 'Preço válido apenas para as sextas-feiras, durante o horário de atendimento do Fish ‘n’ Chips, marujos!', 'Combo Hambúrguer de Siri + Batata + Molho + Refrigerante', '', 'publish', 'closed', 'closed', '', 'combo-de-hamburguer-de-salmao-batata-molho-refrigerante', '', '', '2017-03-23 16:05:26', '2017-03-23 19:05:26', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=220', 0, 'cardapio', '', 0),
(227, 1, '2016-06-21 10:11:34', '2016-06-21 13:11:34', '5% Cashback no Fish’n’Chips.\r\n', '5% Cashback no Fish’n’Chips.', '', 'publish', 'closed', 'closed', '', '5-cashback-no-fishnchips', '', '', '2017-03-23 12:17:33', '2017-03-23 15:17:33', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=227', 0, 'clube', '', 0),
(228, 1, '2016-06-21 10:11:32', '2016-06-21 13:11:32', 'Pré-convites para eventos e festivais internos.', 'Pré-convites para eventos e festivais internos.', '', 'publish', 'closed', 'closed', '', 'pre-convites-para-eventos-e-festivais-internos', '', '', '2017-03-23 12:16:06', '2017-03-23 15:16:06', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=228', 0, 'clube', '', 0),
(229, 1, '2016-06-21 10:11:31', '2016-06-21 13:11:31', 'Adega individual para até quatro (04) garrafas - consultar unidade Victor com adega. ', 'Adega individual para até quatro (04) garrafas - consultar unidade Victor com adega.', '', 'publish', 'closed', 'closed', '', 'beneficios-d', '', '', '2017-03-23 12:13:52', '2017-03-23 15:13:52', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=229', 0, 'clube', '', 0),
(230, 1, '2016-06-21 10:11:28', '2016-06-21 13:11:28', 'R$ 30 de crédito para usar no mês de seu aniversário.', 'R$ 30 de crédito para usar no mês de seu aniversário.', '', 'publish', 'closed', 'closed', '', 'beneficios-e', '', '', '2017-03-21 17:43:03', '2017-03-21 20:43:03', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=230', 0, 'clube', '', 0),
(235, 1, '2016-06-21 10:23:39', '2016-06-21 13:23:39', '', 'cherry-top-1', '', 'inherit', 'open', 'closed', '', 'cherry-top-1', '', '', '2016-06-21 10:23:39', '2016-06-21 13:23:39', '', 227, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/cherry-top-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(236, 1, '2016-06-21 10:24:22', '2016-06-21 13:24:22', '', 'prawn-3', '', 'inherit', 'open', 'closed', '', 'prawn-3', '', '', '2016-06-21 10:24:22', '2016-06-21 13:24:22', '', 227, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(237, 1, '2016-06-21 10:24:34', '2016-06-21 13:24:34', '', 'presente-1', '', 'inherit', 'open', 'closed', '', 'presente-1', '', '', '2016-06-21 10:24:34', '2016-06-21 13:24:34', '', 228, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/presente-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(238, 1, '2016-06-21 10:24:48', '2016-06-21 13:24:48', '', 'porcentagem-1', '', 'inherit', 'open', 'closed', '', 'porcentagem-1', '', '', '2016-06-21 10:24:48', '2016-06-21 13:24:48', '', 229, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/porcentagem-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(239, 1, '2016-06-21 10:25:04', '2016-06-21 13:25:04', '', 'hot-fish-bone-1', '', 'inherit', 'open', 'closed', '', 'hot-fish-bone-1', '', '', '2016-06-21 10:25:04', '2016-06-21 13:25:04', '', 230, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hot-fish-bone-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(240, 1, '2016-06-21 10:25:26', '2016-06-21 13:25:26', '', 'giftbox-1', '', 'inherit', 'open', 'closed', '', 'giftbox-1', '', '', '2016-06-21 10:25:26', '2016-06-21 13:25:26', '', 230, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/giftbox-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(241, 1, '2016-06-21 10:25:39', '2016-06-21 13:25:39', '', 'duascarinhas-3', '', 'inherit', 'open', 'closed', '', 'duascarinhas-3', '', '', '2016-06-21 10:25:39', '2016-06-21 13:25:39', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/duascarinhas-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(242, 1, '2016-06-21 10:25:54', '2016-06-21 13:25:54', '', 'prawn-4', '', 'inherit', 'open', 'closed', '', 'prawn-4', '', '', '2016-06-21 10:25:54', '2016-06-21 13:25:54', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn-4.svg', 0, 'attachment', 'image/svg+xml', 0),
(278, 1, '2016-08-25 16:22:00', '2016-08-25 19:22:00', 'Preço válido apenas para as segundas-feiras, durante o horário de atendimento do Fish ‘n’ Chips, marujos!', 'Spaghetti de Camarão', '', 'publish', 'closed', 'closed', '', 'spaghetti-de-camarao', '', '', '2017-03-23 11:28:13', '2017-03-23 14:28:13', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=278', 0, 'cardapio', '', 0),
(279, 1, '2016-08-25 16:22:00', '2016-08-25 19:22:00', '', 'hamburger-branco-7', '', 'inherit', 'open', 'closed', '', 'hamburger-branco-7-2', '', '', '2016-08-25 16:22:00', '2016-08-25 19:22:00', '', 278, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/08/hamburger-branco-7.svg', 0, 'attachment', 'image/svg+xml', 0),
(280, 1, '2016-08-25 16:24:59', '2016-08-25 19:24:59', 'breve descrição....', 'Pedro', '', 'publish', 'closed', 'closed', '', 'membro-exemplo', '', '', '2017-03-21 16:36:15', '2017-03-21 19:36:15', '', 0, 'http://fishnchips.pixd.com.br/?post_type=equipe&#038;p=280', 0, 'equipe', '', 0),
(281, 1, '2016-08-25 16:28:06', '2016-08-25 19:28:06', 'É um programa de fidelidade pra marujo nenhum botar defeito.', 'É um programa de fidelidade pra marujo nenhum botar defeito.', '', 'publish', 'closed', 'closed', '', 'titulo-do-desconto', '', '', '2017-03-21 17:39:29', '2017-03-21 20:39:29', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=281', 0, 'clube', '', 0);
INSERT INTO `fc_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(282, 1, '2016-08-25 16:28:06', '2016-08-25 19:28:06', '', 'prawn-3', '', 'inherit', 'open', 'closed', '', 'prawn-3-2', '', '', '2016-08-25 16:28:06', '2016-08-25 19:28:06', '', 281, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/08/prawn-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(286, 1, '2017-03-21 15:56:39', '2017-03-21 18:56:39', 'Hambúrguer de Siri e Hambúrguer de Salmão\r\n\r\nUM BRINDE, MARUJOS\r\n\r\nChopp em Dobro: Heineken & Amstel\r\n\r\nDas 16h às 19h', 'TESOUROS ESCONDIDOS', '', 'draft', 'closed', 'closed', '', 'tesouros-escondidos', '', '', '2017-03-23 15:32:01', '2017-03-23 18:32:01', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=286', 0, 'cardapio', '', 0),
(287, 1, '2017-03-21 15:55:45', '2017-03-21 18:55:45', '', '96a961b229b14e759bb7d69f387fb4aa', '', 'inherit', 'open', 'closed', '', '96a961b229b14e759bb7d69f387fb4aa', '', '', '2017-03-21 15:55:45', '2017-03-21 18:55:45', '', 286, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/96a961b229b14e759bb7d69f387fb4aa.png', 0, 'attachment', 'image/png', 0),
(288, 1, '2017-03-21 16:01:21', '2017-03-21 19:01:21', 'Descrição', '5% Cashback no Fish’n’Chips.', '', 'inherit', 'closed', 'closed', '', '281-autosave-v1', '', '', '2017-03-21 16:01:21', '2017-03-21 19:01:21', '', 281, 'http://fishnchips.pixd.com.br/281-autosave-v1/', 0, 'revision', '', 0),
(289, 1, '2017-03-21 16:30:16', '2017-03-21 19:30:16', '<span style=\"font-weight: 400;\">Ahoy, tripulação! Lembro-me muito bem de quando ele ancorou sua embarcação na Ilha da Alimentação do Shopping Mueller, em 2012. Quem, capitão? O rebelde Long Victor, marujo faminto e cozinheiro do navio. Dizem que ele enfrentou constantemente, em anos de aprimoramento, uma força maior que ele mesmo: a vontade de comer o que preparava. </span>\r\n\r\n&nbsp;\r\n\r\n<span style=\"font-weight: 400;\">“Por Mil Frutos do Mar Crocantes!” Então essa embarcação pertence a ele, capitão? Yo ho ho... E nas profundezas de sua cozinha dizem que ele sabe o tempo de preparação de seus pratos por meio do cheiro de sua chapa, do balanço de suas espátulas e, principalmente, pelas caras de famintos de seus tripulantes. Victor sabia muito bem que seus “Cabeças de Ostras” gostavam de comer bem sem perder muito tempo.   </span>\r\n\r\n&nbsp;\r\n\r\n<span style=\"font-weight: 400;\">E mais: dizem também que Long Victor ficou desaparecido por mil dias e mil noites e, quando voltou, bradava em voz bem alta, apoiado no mastro de seu navio construído com peixe e fritas: Casual Dinning, Casual Dinning... A partir daí, começou a servir coisas incríveis, ágeis, sustentáveis, mas sem perder o velho e conhecido temperamento de marujo.</span>\r\n<span style=\"font-weight: 400;\">Coragem, tripulantes, e experimentai os pratos que nos esperam. Os ventos estão ao nosso favor... Digo, o cheirinho dos frutos do mar.</span>', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2017-03-21 16:30:16', '2017-03-21 19:30:16', '', 114, 'http://fishnchips.pixd.com.br/114-revision-v1/', 0, 'revision', '', 0),
(290, 1, '2017-03-21 16:30:37', '2017-03-21 19:30:37', 'Ahoy, tripulação! Lembro-me muito bem de quando ele ancorou sua embarcação na Ilha da Alimentação do Shopping Mueller, em 2012. Quem, capitão? O rebelde Long Victor, marujo faminto e cozinheiro do navio. Dizem que ele enfrentou constantemente, em anos de aprimoramento, uma força maior que ele mesmo: a vontade de comer o que preparava. \r\n\r\n“Por Mil Frutos do Mar Crocantes!” Então essa embarcação pertence a ele, capitão? Yo ho ho... E nas profundezas de sua cozinha dizem que ele sabe o tempo de preparação de seus pratos por meio do cheiro de sua chapa, do balanço de suas espátulas e, principalmente, pelas caras de famintos de seus tripulantes. Victor sabia muito bem que seus “Cabeças de Ostras” gostavam de comer bem sem perder muito tempo.   \r\n\r\nE mais: dizem também que Long Victor ficou desaparecido por mil dias e mil noites e, quando voltou, bradava em voz bem alta, apoiado no mastro de seu navio construído com peixe e fritas: Casual Dinning, Casual Dinning... A partir daí, começou a servir coisas incríveis, ágeis, sustentáveis, mas sem perder o velho e conhecido temperamento de marujo.\r\n\r\nCoragem, tripulantes, e experimentai os pratos que nos esperam. Os ventos estão ao nosso favor... Digo, o cheirinho dos frutos do mar.', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2017-03-21 16:30:37', '2017-03-21 19:30:37', '', 114, 'http://fishnchips.pixd.com.br/114-revision-v1/', 0, 'revision', '', 0),
(291, 1, '2017-03-21 16:38:20', '2017-03-21 19:38:20', 'Praesent at facilisis metus. Morbi ultricies lorem sollicitudin turpis hendrerit fermentum. Nullam facilisis felis eget eros varius sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus quis augue sed augue faucibus auctor', 'Christiano', '', 'inherit', 'closed', 'closed', '', '126-autosave-v1', '', '', '2017-03-21 16:38:20', '2017-03-21 19:38:20', '', 126, 'http://fishnchips.pixd.com.br/126-autosave-v1/', 0, 'revision', '', 0),
(292, 1, '2017-03-21 16:41:43', '2017-03-21 19:41:43', '', '1381308_10152262216923895_21848970_n', '', 'inherit', 'open', 'closed', '', '1381308_10152262216923895_21848970_n', '', '', '2017-03-21 16:41:43', '2017-03-21 19:41:43', '', 126, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/1381308_10152262216923895_21848970_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(293, 1, '2017-03-21 16:54:02', '2017-03-21 19:54:02', 'O cheiro nasce na segunda e desaparece na sexta, marujos! De quê, capitão? Yo ho ho, dos Pratos do Dia, rapazes.', 'Promoções', '', 'inherit', 'closed', 'closed', '', '202-revision-v1', '', '', '2017-03-21 16:54:02', '2017-03-21 19:54:02', '', 202, 'http://fishnchips.pixd.com.br/202-revision-v1/', 0, 'revision', '', 0),
(294, 1, '2017-03-21 16:54:47', '2017-03-21 19:54:47', 'O cheiro nasce na segunda e desaparece na sexta, marujos! De quê, capitão? Yo ho ho, dos <strong>Pratos do Dia</strong>, rapazes.', 'Promoções', '', 'inherit', 'closed', 'closed', '', '202-revision-v1', '', '', '2017-03-21 16:54:47', '2017-03-21 19:54:47', '', 202, 'http://fishnchips.pixd.com.br/202-revision-v1/', 0, 'revision', '', 0),
(295, 1, '2017-03-21 17:06:27', '2017-03-21 20:06:27', 'Chopp em Dobro: Heineken &amp; Amstel\r\n\r\nDas 16h às 19h', 'UM BRINDE, MARUJOS', '', 'draft', 'closed', 'closed', '', 'um-brinde-marujos', '', '', '2017-03-23 15:31:56', '2017-03-23 18:31:56', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=295', 0, 'cardapio', '', 0),
(296, 1, '2017-03-21 17:09:51', '2017-03-21 20:09:51', '', 'foto-hambuger', '', 'inherit', 'open', 'closed', '', 'foto-hambuger-2', '', '', '2017-03-21 17:09:51', '2017-03-21 20:09:51', '', 295, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/foto-hambuger.png', 0, 'attachment', 'image/png', 0),
(297, 1, '2017-03-21 17:22:52', '2017-03-21 20:22:52', 'Olhai para onde pisam, marujos! Parece-me que estamos em cima de um delicioso tesouro: arroz com carne de siri, ervas finas, tomate, pimentões e batata palha. \r\n', 'Arroz de Siri', '', 'publish', 'closed', 'closed', '', 'arroz-de-siri', '', '', '2017-03-23 10:46:42', '2017-03-23 13:46:42', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=297', 0, 'cardapio', '', 0),
(298, 1, '2017-03-21 17:23:39', '2017-03-21 20:23:39', 'São 150 gramas de iscas de peixe e de batatas, marujos! ', 'The Classic Fish’n’Chips', '', 'publish', 'closed', 'closed', '', 'the-classic-fishnchips', '', '', '2017-03-23 09:49:51', '2017-03-23 12:49:51', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=298', 0, 'cardapio', '', 0),
(299, 1, '2017-03-21 17:24:14', '2017-03-21 20:24:14', 'Acompanha rúcula, tomate e molho casa no pão de limão.\r\n', 'Hambúrguer de Salmão', '', 'publish', 'closed', 'closed', '', 'hamburguer-de-camarao-2', '', '', '2017-03-23 10:23:44', '2017-03-23 13:23:44', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=299', 0, 'cardapio', '', 0),
(300, 1, '2017-03-21 17:25:57', '2017-03-21 20:25:57', 'Diz a lenda que seu sabor é capaz de matar a fome de centenas de marujos, por meio de molho tártaro, alface e tomate no pão de limão. ', 'Hambúrguer de Siri', '', 'publish', 'closed', 'closed', '', 'hamburguer-de-siri', '', '', '2017-03-22 17:28:44', '2017-03-22 20:28:44', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=300', 0, 'cardapio', '', 0),
(301, 1, '2017-03-21 17:26:26', '2017-03-21 20:26:26', 'Cavem, cavem mais fundo, marujo! Tenho certeza que o tesouro está aí: sorvete de mousse de chocolate branco e preto, com pedaços de morango. ', 'Dueto de Chocolate', '', 'publish', 'closed', 'closed', '', 'dueto-de-chocolate', '', '', '2017-03-23 16:48:01', '2017-03-23 19:48:01', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=301', 0, 'cardapio', '', 0),
(302, 1, '2017-03-21 17:26:50', '2017-03-21 20:26:50', '“Vegetarianos me mordam!” Uma delícia vindo até nós, marujos: cream cheese, cenoura, tomate, brócolis e pimentão envoltos em uma tortilha de trigo. ', 'Wrap Veggie', '', 'publish', 'closed', 'closed', '', 'wrap-veggie', '', '', '2017-03-22 17:29:59', '2017-03-22 20:29:59', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=302', 0, 'cardapio', '', 0),
(303, 1, '2017-03-21 17:27:48', '2017-03-21 20:27:48', 'É o Mar Vermelho, marujos? Não, capitão! É uma torta de cream cheese coberta com muita calda de goiaba. ', 'Cheesecake Romeu e Julieta', '', 'publish', 'closed', 'closed', '', 'cheesecake-romeu-e-julieta', '', '', '2017-03-22 16:05:42', '2017-03-22 19:05:42', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=303', 0, 'cardapio', '', 0),
(304, 1, '2017-03-21 17:28:18', '2017-03-21 20:28:18', 'Carregai navio, rapazes! Nada como uma “carga rápida”, com lascas de peixe, arroz grego, minissalada de folhas verdes e tomates, batatas e molho de limão para encher esse casco rapidamente.', 'Fish Express', '', 'publish', 'closed', 'closed', '', 'fish-express', '', '', '2017-03-23 10:47:05', '2017-03-23 13:47:05', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=304', 0, 'cardapio', '', 0),
(305, 1, '2017-03-21 17:29:32', '2017-03-21 20:29:32', 'Hey! Somos os famintos piratas. Nossa fome é fenomenal. Atiramos com nossa bombardaaa... Os Bolinhos de Bacalhau. Hey! . . .\r\n', 'Bolinhos de Bacalhau', '', 'publish', 'closed', 'closed', '', 'bolinhos-de-bacalhau', '', '', '2017-03-23 12:35:32', '2017-03-23 15:35:32', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=305', 0, 'cardapio', '', 0),
(306, 1, '2017-03-21 17:29:54', '2017-03-21 20:29:54', 'Alinhai bocas famintas, marujos, aquele Baguete de Salmão está mais preparado do que nunca: cubos de salmão grelhados, cream cheese e rúcula no pão artesanal. Que os paladares nos protejam!', 'Baguete de Salmão', '', 'publish', 'closed', 'closed', '', 'baguete-de-salmao', '', '', '2017-03-23 12:35:16', '2017-03-23 15:35:16', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=306', 0, 'cardapio', '', 0),
(307, 1, '2017-03-21 17:34:13', '2017-03-21 20:34:13', 'Eis que fazer parte da tripulação nunca foi tão fácil, marujo. Tenha benefícios exclusivos, sendo um fiel tripulante da embarcação Fish ‘n’ Chips. \r\n', 'Clube', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2017-03-21 17:34:13', '2017-03-21 20:34:13', '', 138, 'http://fishnchips.pixd.com.br/138-revision-v1/', 0, 'revision', '', 0),
(308, 1, '2017-03-21 17:39:44', '2017-03-21 20:39:44', 'Sua dedicação é importante para seguirmos em frente com nossa embarcação.', 'Sua dedicação é importante para seguirmos em frente com nossa embarcação.', '', 'publish', 'closed', 'closed', '', 'sua-dedicacao-e-importante-para-seguirmos-em-frente-com-nossa-embarcacao', '', '', '2017-03-21 17:39:44', '2017-03-21 20:39:44', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=308', 0, 'clube', '', 0),
(309, 1, '2017-03-21 17:39:54', '2017-03-21 20:39:54', 'fiel tripulante que é fiel tripulante faz a adesão e aproveita dos benefícios quando quiser.', 'fiel tripulante que é fiel tripulante faz a adesão e aproveita dos benefícios quando quiser.', '', 'publish', 'closed', 'closed', '', 'seja-um-fiel-tripulante-e-ajude-aumentar-nosso-prestigio', '', '', '2017-03-22 15:11:26', '2017-03-22 18:11:26', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=309', 0, 'clube', '', 0),
(310, 1, '2017-03-21 17:40:49', '2017-03-21 20:40:49', 'Sem custo na adesão do Clube Victor.', 'Sem custo na adesão do Clube Victor.', '', 'publish', 'closed', 'closed', '', 'sem-custo-na-adesao-clube-victor', '', '', '2017-03-21 17:40:49', '2017-03-21 20:40:49', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=310', 0, 'clube', '', 0),
(311, 1, '2017-03-21 17:41:01', '2017-03-21 20:41:01', 'Por apenas R$ 30, você ganha uma camiseta do Clube Victor, se torna um marujo oficial e ainda contempla de um benefício especial toda última terça-feira de cada mês. ', 'Por apenas R$ 30, você ganha uma camiseta do Clube Victor, se torna um marujo oficial e ainda contempla de um benefício especial toda última terça-feira de cada mês.', '', 'publish', 'closed', 'closed', '', 'por-apenas-r-30-voce-ganha-uma-camiseta-clube-victor-se-torna-um-marujo-oficial-e-ainda-contempla-de-um-beneficio-especial-toda-ultima-terca-feira-de-cada-mes', '', '', '2017-03-21 17:41:01', '2017-03-21 20:41:01', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=311', 0, 'clube', '', 0),
(312, 1, '2017-03-21 17:43:29', '2017-03-21 20:43:29', ' Na última terça-feira do mês, o fiel tripulante que vier ao Fish ‘n’ Chips com sua camiseta ganha 50% de desconto (porém, não é aplicado o desconto de 5%).', 'Na última terçafeira do mês, o fiel tripulante que vier ao Fish ‘n’ Chips com sua camiseta ganha 50% de desconto (porém, não é aplicado o desconto de 5%).', '', 'publish', 'closed', 'closed', '', 'na-ultima-tercafeira-mes-o-fiel-tripulante-que-vier-ao-fish-n-chips-com-sua-camiseta-ganha-50-de-desconto-porem-nao-e-aplicado-o-desconto-de-5', '', '', '2017-03-22 16:03:32', '2017-03-22 19:03:32', '', 0, 'http://fishnchips.pixd.com.br/?post_type=clube&#038;p=312', 0, 'clube', '', 0),
(313, 1, '2017-03-22 15:59:15', '2017-03-22 18:59:15', '', 'foto-hambuger-1', '', 'inherit', 'open', 'closed', '', 'foto-hambuger-1', '', '', '2017-03-22 15:59:15', '2017-03-22 18:59:15', '', 312, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/foto-hambuger-1.png', 0, 'attachment', 'image/png', 0),
(314, 1, '2017-03-22 16:41:45', '2017-03-22 19:41:45', '', 'BagueteMignon_Alta-min', '', 'inherit', 'open', 'closed', '', 'baguetemignon_alta-min', '', '', '2017-03-22 16:41:45', '2017-03-22 19:41:45', '', 306, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/BagueteMignon_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(315, 1, '2017-03-22 16:42:54', '2017-03-22 19:42:54', '', 'BolinhoBacalhau_Alta-min', '', 'inherit', 'open', 'closed', '', 'bolinhobacalhau_alta-min', '', '', '2017-03-22 16:42:54', '2017-03-22 19:42:54', '', 305, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/BolinhoBacalhau_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(316, 1, '2017-03-22 16:43:36', '2017-03-22 19:43:36', '', 'Fish Express-min', '', 'inherit', 'open', 'closed', '', 'fish-express-min', '', '', '2017-03-22 16:43:36', '2017-03-22 19:43:36', '', 304, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/Fish-Express-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(317, 1, '2017-03-22 16:46:34', '2017-03-22 19:46:34', '', 'HambúrguerCamarão_Alta-min', '', 'inherit', 'open', 'closed', '', 'hamburguercamarao_alta-min', '', '', '2017-03-22 16:46:34', '2017-03-22 19:46:34', '', 300, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/HambúrguerCamarão_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(318, 1, '2017-03-22 16:47:30', '2017-03-22 19:47:30', '', 'HambúrguerCamarão2-min', '', 'inherit', 'open', 'closed', '', 'hamburguercamarao2-min', '', '', '2017-03-22 16:47:30', '2017-03-22 19:47:30', '', 299, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/HambúrguerCamarão2-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(319, 1, '2017-03-22 16:48:34', '2017-03-22 19:48:34', '', 'Duetto-min', '', 'inherit', 'open', 'closed', '', 'duetto-min', '', '', '2017-03-22 16:48:34', '2017-03-22 19:48:34', '', 301, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/Duetto-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(320, 1, '2017-03-22 17:23:02', '2017-03-22 20:23:02', '', 'HambúrguerSalmãoCombo_Alta-min', '', 'inherit', 'open', 'closed', '', 'hamburguersalmaocombo_alta-min', '', '', '2017-03-22 17:23:02', '2017-03-22 20:23:02', '', 220, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/HambúrguerSalmãoCombo_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(321, 1, '2017-03-22 17:24:23', '2017-03-22 20:24:23', '', 'PeixeComLegumes_Alta-min', '', 'inherit', 'open', 'closed', '', 'peixecomlegumes_alta-min', '', '', '2017-03-22 17:24:23', '2017-03-22 20:24:23', '', 217, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/PeixeComLegumes_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(322, 1, '2017-03-22 17:25:03', '2017-03-22 20:25:03', '', 'PenneSalmão-min', '', 'inherit', 'open', 'closed', '', 'pennesalmao-min', '', '', '2017-03-22 17:25:03', '2017-03-22 20:25:03', '', 214, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/PenneSalmão-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(323, 1, '2017-03-22 17:26:42', '2017-03-22 20:26:42', '', 'SpaghettiCamarão-min', '', 'inherit', 'open', 'closed', '', 'spaghetticamarao-min', '', '', '2017-03-22 17:26:42', '2017-03-22 20:26:42', '', 278, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/08/SpaghettiCamarão-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(324, 1, '2017-03-22 17:27:41', '2017-03-22 20:27:41', '', 'ArrozSiri_Alta-min', '', 'inherit', 'open', 'closed', '', 'arrozsiri_alta-min', '', '', '2017-03-22 17:27:41', '2017-03-22 20:27:41', '', 297, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/ArrozSiri_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(325, 1, '2017-03-22 17:28:35', '2017-03-22 20:28:35', '', 'HambúrguerSiri_Alta-min', '', 'inherit', 'open', 'closed', '', 'hamburguersiri_alta-min', '', '', '2017-03-22 17:28:35', '2017-03-22 20:28:35', '', 300, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/HambúrguerSiri_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(327, 1, '2017-03-22 17:29:38', '2017-03-22 20:29:38', '', 'WrapCamarão_Alta-min', '', 'inherit', 'open', 'closed', '', 'wrapcamarao_alta-min', '', '', '2017-03-22 17:29:38', '2017-03-22 20:29:38', '', 302, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/WrapCamarão_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(328, 1, '2017-03-23 09:49:40', '2017-03-23 12:49:40', '', 'FishNChipsCombo_Alta-min', '', 'inherit', 'open', 'closed', '', 'fishnchipscombo_alta-min', '', '', '2017-03-23 09:49:40', '2017-03-23 12:49:40', '', 298, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/FishNChipsCombo_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(329, 1, '2017-03-23 09:57:45', '2017-03-23 12:57:45', 'mix de folhas verdes, tomate, croutons, vinagrete de mostarda.', 'Salada Irlandesa', '', 'publish', 'closed', 'closed', '', 'salada-irlandesa', '', '', '2017-03-23 12:35:07', '2017-03-23 15:35:07', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=329', 0, 'cardapio', '', 0),
(330, 1, '2017-03-23 09:56:40', '2017-03-23 12:56:40', '', 'SaladaInglesaLula_Alta-min', '', 'inherit', 'open', 'closed', '', 'saladainglesalula_alta-min', '', '', '2017-03-23 09:56:40', '2017-03-23 12:56:40', '', 329, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/SaladaInglesaLula_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(331, 1, '2017-03-23 09:57:34', '2017-03-23 12:57:34', '', 'SaladaIrlandesaLula-min', '', 'inherit', 'open', 'closed', '', 'saladairlandesalula-min', '', '', '2017-03-23 09:57:34', '2017-03-23 12:57:34', '', 329, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/SaladaIrlandesaLula-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(332, 1, '2017-03-23 10:09:12', '2017-03-23 13:09:12', '', 'salad-bowl-hand-drawn-food', '', 'inherit', 'open', 'closed', '', 'salad-bowl-hand-drawn-food', '', '', '2017-03-23 10:09:12', '2017-03-23 13:09:12', '', 329, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/salad-bowl-hand-drawn-food.png', 0, 'attachment', 'image/png', 0),
(333, 1, '2017-03-23 10:13:41', '2017-03-23 13:13:41', 'Mix de folhas verdes, tomate, palmito, croutons, vinagre balsamico.', 'Salada inglesa', '', 'publish', 'closed', 'closed', '', 'salada-inglesa', '', '', '2017-03-23 12:34:57', '2017-03-23 15:34:57', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=333', 0, 'cardapio', '', 0),
(334, 1, '2017-03-23 10:13:02', '2017-03-23 13:13:02', '', 'SaladaInglesaCamarão-min', '', 'inherit', 'open', 'closed', '', 'saladainglesacamarao-min', '', '', '2017-03-23 10:13:02', '2017-03-23 13:13:02', '', 333, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/SaladaInglesaCamarão-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(335, 1, '2017-03-23 10:17:43', '2017-03-23 13:17:43', 'Pão artesanal com pedaços de mignon, queijo, maionese, alface e tomate.', 'Baguete de Mignon', '', 'publish', 'closed', 'closed', '', 'baguete-de-mignon', '', '', '2017-03-23 12:34:34', '2017-03-23 15:34:34', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=335', 0, 'cardapio', '', 0),
(336, 1, '2017-03-23 10:18:03', '2017-03-23 13:18:03', '', 'BagueteSalmão_Alta-min', '', 'inherit', 'open', 'closed', '', 'baguetesalmao_alta-min', '', '', '2017-03-23 10:18:03', '2017-03-23 13:18:03', '', 306, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/BagueteSalmão_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(337, 1, '2017-03-23 10:23:18', '2017-03-23 13:23:18', 'Acompanha rúcula, tomate e molho casa no pão de limão \n', 'Hambúrguer de Salmão', '', 'inherit', 'closed', 'closed', '', '299-autosave-v1', '', '', '2017-03-23 10:23:18', '2017-03-23 13:23:18', '', 299, 'http://fishnchips.pixd.com.br/299-autosave-v1/', 0, 'revision', '', 0),
(338, 1, '2017-03-23 10:29:02', '2017-03-23 13:29:02', '', 'Hambúrguer de Camarão +Chips Batata Doce +Molho +Refri', '', 'publish', 'closed', 'closed', '', 'hamburguer-de-camarao-chips-batata-doce-molho-refri', '', '', '2017-03-23 12:33:43', '2017-03-23 15:33:43', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=338', 0, 'cardapio', '', 0),
(339, 1, '2017-03-23 10:28:26', '2017-03-23 13:28:26', 'Alguém a viu a exuberante torta recheada com banana, doce de leite, chocolate meio amargo e coberta com chocolate branco por aí? Sim, estou falando com vocês, Marujos Famintos! ', 'Banana Pie', '', 'publish', 'closed', 'closed', '', 'banana-pie', '', '', '2017-03-23 16:53:46', '2017-03-23 19:53:46', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=339', 0, 'cardapio', '', 0),
(340, 1, '2017-03-23 10:27:43', '2017-03-23 13:27:43', '', 'BananaPie_Alta-min', '', 'inherit', 'open', 'closed', '', 'bananapie_alta-min', '', '', '2017-03-23 10:27:43', '2017-03-23 13:27:43', '', 339, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/BananaPie_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(341, 1, '2017-03-23 10:28:28', '2017-03-23 13:28:28', '', 'Hamb£rguerCamar∆oCombo_Alta-min', '', 'inherit', 'open', 'closed', '', 'hambrguercamar%e2%88%86ocombo_alta-min', '', '', '2017-03-23 10:28:28', '2017-03-23 13:28:28', '', 338, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/Hamb£rguerCamar∆oCombo_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(342, 1, '2017-03-23 10:31:56', '2017-03-23 13:31:56', '', 'Fish’n’Chips +Molho', '', 'publish', 'closed', 'closed', '', 'fishnchips-molho', '', '', '2017-03-23 12:33:31', '2017-03-23 15:33:31', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=342', 0, 'cardapio', '', 0),
(343, 1, '2017-03-23 10:31:57', '2017-03-23 13:31:57', '', 'Pudim Leite', '', 'publish', 'closed', 'closed', '', 'pudim-leite', '', '', '2017-03-23 12:33:06', '2017-03-23 15:33:06', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=343', 0, 'cardapio', '', 0),
(344, 1, '2017-03-23 10:31:19', '2017-03-23 13:31:19', '', 'Pudim-min', '', 'inherit', 'open', 'closed', '', 'pudim-min', '', '', '2017-03-23 10:31:19', '2017-03-23 13:31:19', '', 343, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/Pudim-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(345, 1, '2017-03-23 10:34:31', '2017-03-23 13:34:31', '', 'Hambúrguer de Siri +Batata +Molho +Refri', '', 'publish', 'closed', 'closed', '', 'hamburguer-de-siri-batata-molho-refri', '', '', '2017-03-23 12:32:20', '2017-03-23 15:32:20', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=345', 0, 'cardapio', '', 0),
(346, 1, '2017-03-23 10:40:06', '2017-03-23 13:40:06', '', 'Bolinho de Siri', '', 'publish', 'closed', 'closed', '', 'bolinho-de-siri', '', '', '2017-03-23 12:31:06', '2017-03-23 15:31:06', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=346', 0, 'cardapio', '', 0),
(347, 1, '2017-03-23 10:34:18', '2017-03-23 13:34:18', '', 'ComboHambSiri_Altajpg-min', '', 'inherit', 'open', 'closed', '', 'combohambsiri_altajpg-min', '', '', '2017-03-23 10:34:18', '2017-03-23 13:34:18', '', 345, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/ComboHambSiri_Altajpg-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(348, 1, '2017-03-23 10:35:56', '2017-03-23 13:35:56', '', 'Hambúrguer de Salmão +Batata +Molho +Refri', '', 'publish', 'closed', 'closed', '', 'hamburguer-de-salmao-batata-molho-refri', '', '', '2017-03-23 12:32:08', '2017-03-23 15:32:08', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=348', 0, 'cardapio', '', 0),
(349, 1, '2017-03-23 10:38:10', '2017-03-23 13:38:10', '', 'Iscas de Peixe (300g) +Molho', '', 'publish', 'closed', 'closed', '', 'iscas-de-peixe-300g-molho', '', '', '2017-03-23 12:31:55', '2017-03-23 15:31:55', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=349', 0, 'cardapio', '', 0),
(350, 1, '2017-03-23 10:37:54', '2017-03-23 13:37:54', '', 'IscasPeixeCombinado_Alta-min', '', 'inherit', 'open', 'closed', '', 'iscaspeixecombinado_alta-min', '', '', '2017-03-23 10:37:54', '2017-03-23 13:37:54', '', 349, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/IscasPeixeCombinado_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(351, 1, '2017-03-23 10:40:06', '2017-03-23 13:40:06', '', 'Bolinho de Siri +Batata +Molho', '', 'publish', 'closed', 'closed', '', 'bolinho-de-siri-batata-molho', '', '', '2017-03-23 12:29:52', '2017-03-23 15:29:52', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=351', 0, 'cardapio', '', 0),
(352, 1, '2017-03-23 10:39:38', '2017-03-23 13:39:38', '', 'BolinhoSiriCombo_Alta-min', '', 'inherit', 'open', 'closed', '', 'bolinhosiricombo_alta-min', '', '', '2017-03-23 10:39:38', '2017-03-23 13:39:38', '', 351, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/BolinhoSiriCombo_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(353, 1, '2017-03-23 10:39:42', '2017-03-23 13:39:42', '', 'BolinhoBacalhau_Alta-min', '', 'inherit', 'open', 'closed', '', 'bolinhobacalhau_alta-min-2', '', '', '2017-03-23 10:39:42', '2017-03-23 13:39:42', '', 346, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/BolinhoBacalhau_Alta-min-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(354, 1, '2017-03-23 10:40:53', '2017-03-23 13:40:53', '', 'Bolinho de bacalhau', '', 'private', 'closed', 'closed', '', 'bolinho-de-bacalhau', '', '', '2017-03-23 16:51:39', '2017-03-23 19:51:39', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=354', 0, 'cardapio', '', 0),
(355, 1, '2017-03-23 10:45:19', '2017-03-23 13:45:19', 'Strogonoff de camarão, arroz branco e batata palha.', 'Strogonoff de Camarão', '', 'publish', 'closed', 'closed', '', 'strogonoff-de-camarao', '', '', '2017-03-23 12:25:55', '2017-03-23 15:25:55', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=355', 0, 'cardapio', '', 0),
(356, 1, '2017-03-23 10:42:15', '2017-03-23 13:42:15', '', 'Iscas de Peixe', '', 'publish', 'closed', 'closed', '', 'iscas-de-peixe', '', '', '2017-03-23 16:22:42', '2017-03-23 19:22:42', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=356', 0, 'cardapio', '', 0),
(357, 1, '2017-03-23 10:42:05', '2017-03-23 13:42:05', '', 'IscasPeixe_Alta-min', '', 'inherit', 'open', 'closed', '', 'iscaspeixe_alta-min', '', '', '2017-03-23 10:42:05', '2017-03-23 13:42:05', '', 356, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/IscasPeixe_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(359, 1, '2017-03-23 10:45:20', '2017-03-23 13:45:20', '', 'Espeto de camarão', '', 'publish', 'closed', 'closed', '', 'espeto-de-camarao', '', '', '2017-03-23 12:25:36', '2017-03-23 15:25:36', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=359', 0, 'cardapio', '', 0),
(360, 1, '2017-03-23 10:44:42', '2017-03-23 13:44:42', '', 'Strogonoff_Alta-min', '', 'inherit', 'open', 'closed', '', 'strogonoff_alta-min', '', '', '2017-03-23 10:44:42', '2017-03-23 13:44:42', '', 355, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/Strogonoff_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(361, 1, '2017-03-23 10:45:00', '2017-03-23 13:45:00', '', 'EspetoCamarão_Alta-min', '', 'inherit', 'open', 'closed', '', 'espetocamarao_alta-min', '', '', '2017-03-23 10:45:00', '2017-03-23 13:45:00', '', 359, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/EspetoCamarão_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(362, 1, '2017-03-23 10:46:57', '2017-03-23 13:46:57', '', 'Maionese de Camarão', '', 'publish', 'closed', 'closed', '', 'maionese-de-camarao', '', '', '2017-03-23 12:24:57', '2017-03-23 15:24:57', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=362', 0, 'cardapio', '', 0),
(364, 1, '2017-03-23 10:46:23', '2017-03-23 13:46:23', '', 'MaioneseCamarão-min', '', 'inherit', 'open', 'closed', '', 'maionesecamarao-min', '', '', '2017-03-23 10:46:23', '2017-03-23 13:46:23', '', 362, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/MaioneseCamarão-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(365, 1, '2017-03-23 10:48:10', '2017-03-23 13:48:10', '', 'Lula á Dorê', '', 'publish', 'closed', 'closed', '', 'lula-a-dore', '', '', '2017-03-23 12:24:12', '2017-03-23 15:24:12', '', 0, 'http://fishnchips.pixd.com.br/?post_type=cardapio&#038;p=365', 0, 'cardapio', '', 0),
(366, 1, '2017-03-23 10:47:17', '2017-03-23 13:47:17', 'Strogonoff de camarão, arroz branco e batata palha.', 'Strogonoff de Camarão', '', 'inherit', 'closed', 'closed', '', '355-autosave-v1', '', '', '2017-03-23 10:47:17', '2017-03-23 13:47:17', '', 355, 'http://fishnchips.pixd.com.br/355-autosave-v1/', 0, 'revision', '', 0),
(367, 1, '2017-03-23 10:47:37', '2017-03-23 13:47:37', '', 'AnelLula_Alta-min', '', 'inherit', 'open', 'closed', '', 'anellula_alta-min', '', '', '2017-03-23 10:47:37', '2017-03-23 13:47:37', '', 365, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/AnelLula_Alta-min.jpg', 0, 'attachment', 'image/jpeg', 0),
(369, 1, '2017-03-23 10:58:22', '2017-03-23 13:58:22', 'Preço válido apenas para as segundas-feiras, durante o horário de atendimento do Fish ‘n’ Chips, marujos!', 'Spaghetti de Camarão', '', 'inherit', 'closed', 'closed', '', '278-autosave-v1', '', '', '2017-03-23 10:58:22', '2017-03-23 13:58:22', '', 278, 'http://fishnchips.pixd.com.br/278-autosave-v1/', 0, 'revision', '', 0),
(370, 1, '2017-03-23 11:06:18', '2017-03-23 14:06:18', '', 'sweet-cake-piece (1)', '', 'inherit', 'open', 'closed', '', 'sweet-cake-piece-1', '', '', '2017-03-23 11:06:18', '2017-03-23 14:06:18', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/sweet-cake-piece-1.png', 0, 'attachment', 'image/png', 0),
(371, 1, '2017-03-23 11:07:27', '2017-03-23 14:07:27', '', 'sweet-cake-piece (2)', '', 'inherit', 'open', 'closed', '', 'sweet-cake-piece-2', '', '', '2017-03-23 11:07:27', '2017-03-23 14:07:27', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/sweet-cake-piece-2.png', 0, 'attachment', 'image/png', 0),
(372, 1, '2017-03-23 11:12:09', '2017-03-23 14:12:09', '', 'one-hamburguer', '', 'inherit', 'open', 'closed', '', 'one-hamburguer', '', '', '2017-03-23 11:12:09', '2017-03-23 14:12:09', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/one-hamburguer.png', 0, 'attachment', 'image/png', 0),
(373, 1, '2017-03-23 11:14:38', '2017-03-23 14:14:38', '', 'tray', '', 'inherit', 'open', 'closed', '', 'tray', '', '', '2017-03-23 11:14:38', '2017-03-23 14:14:38', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/tray.png', 0, 'attachment', 'image/png', 0),
(374, 1, '2017-03-23 11:16:35', '2017-03-23 14:16:35', '', 'dish-spoon-and-fork', '', 'inherit', 'open', 'closed', '', 'dish-spoon-and-fork', '', '', '2017-03-23 11:16:35', '2017-03-23 14:16:35', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/dish-spoon-and-fork.png', 0, 'attachment', 'image/png', 0),
(375, 1, '2017-03-23 11:17:07', '2017-03-23 14:17:07', '', 'dish-spoon-and-fork (1)', '', 'inherit', 'open', 'closed', '', 'dish-spoon-and-fork-1', '', '', '2017-03-23 11:17:07', '2017-03-23 14:17:07', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/dish-spoon-and-fork-1.png', 0, 'attachment', 'image/png', 0),
(376, 1, '2017-03-23 11:25:41', '2017-03-23 14:25:41', '', 'spaghetti', '', 'inherit', 'open', 'closed', '', 'spaghetti', '', '', '2017-03-23 11:25:41', '2017-03-23 14:25:41', '', 214, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/spaghetti.png', 0, 'attachment', 'image/png', 0),
(377, 1, '2017-03-23 11:27:59', '2017-03-23 14:27:59', '', 'spaghetti (1)', '', 'inherit', 'open', 'closed', '', 'spaghetti-1', '', '', '2017-03-23 11:27:59', '2017-03-23 14:27:59', '', 278, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/08/spaghetti-1.png', 0, 'attachment', 'image/png', 0),
(378, 1, '2017-03-23 11:30:45', '2017-03-23 14:30:45', '', 'fast-food', '', 'inherit', 'open', 'closed', '', 'fast-food', '', '', '2017-03-23 11:30:45', '2017-03-23 14:30:45', '', 220, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/fast-food.png', 0, 'attachment', 'image/png', 0),
(379, 1, '2017-03-23 11:47:08', '2017-03-23 14:47:08', '', 'salad', '', 'inherit', 'open', 'closed', '', 'salad', '', '', '2017-03-23 11:47:08', '2017-03-23 14:47:08', '', 217, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/salad.png', 0, 'attachment', 'image/png', 0),
(380, 1, '2017-03-23 11:50:08', '2017-03-23 14:50:08', '', 'wine-glasses', '', 'inherit', 'open', 'closed', '', 'wine-glasses', '', '', '2017-03-23 11:50:08', '2017-03-23 14:50:08', '', 295, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/wine-glasses.png', 0, 'attachment', 'image/png', 0),
(381, 1, '2017-03-23 11:50:40', '2017-03-23 14:50:40', '', 'treasure', '', 'inherit', 'open', 'closed', '', 'treasure', '', '', '2017-03-23 11:50:40', '2017-03-23 14:50:40', '', 286, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/treasure.png', 0, 'attachment', 'image/png', 0),
(382, 1, '2017-03-23 12:13:48', '2017-03-23 15:13:48', '', 'two-wine-bottles', '', 'inherit', 'open', 'closed', '', 'two-wine-bottles', '', '', '2017-03-23 12:13:48', '2017-03-23 15:13:48', '', 229, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/two-wine-bottles.png', 0, 'attachment', 'image/png', 0),
(383, 1, '2017-03-23 12:16:01', '2017-03-23 15:16:01', '', 'birthday-card', '', 'inherit', 'open', 'closed', '', 'birthday-card', '', '', '2017-03-23 12:16:01', '2017-03-23 15:16:01', '', 228, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/birthday-card.png', 0, 'attachment', 'image/png', 0),
(384, 1, '2017-03-23 12:17:26', '2017-03-23 15:17:26', '', 'payment-method', '', 'inherit', 'open', 'closed', '', 'payment-method', '', '', '2017-03-23 12:17:26', '2017-03-23 15:17:26', '', 227, 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/payment-method.png', 0, 'attachment', 'image/png', 0),
(385, 1, '2017-03-23 12:23:19', '2017-03-23 15:23:19', '', 'nachos', '', 'inherit', 'open', 'closed', '', 'nachos', '', '', '2017-03-23 12:23:19', '2017-03-23 15:23:19', '', 0, 'http://fishnchips.pixd.com.br/wp-content/uploads/2017/03/nachos.png', 0, 'attachment', 'image/png', 0),
(386, 1, '2017-03-23 16:13:34', '2017-03-23 19:13:34', '<span style=\"font-weight: 400;\">Hambúrguer de Camarão à vista, marujo! A descoberta das “profundezas” da cozinha do Fish ‘n’ Chips vai ficar frente a frente com a sua fome!</span>', 'Hambúrguer de Camarão', '', 'inherit', 'closed', 'closed', '', '91-autosave-v1', '', '', '2017-03-23 16:13:34', '2017-03-23 19:13:34', '', 91, 'http://fishnchips.pixd.com.br/91-autosave-v1/', 0, 'revision', '', 0),
(387, 1, '2017-03-23 16:54:30', '2017-03-23 19:54:30', 'Diz a lenda que seu sabor é capaz de matar a fome de centenas de marujos, por meio de molho tártaro, alface e tomate no pão de limão. ', 'Hambúrguer de Siri', '', 'inherit', 'closed', 'closed', '', '300-autosave-v1', '', '', '2017-03-23 16:54:30', '2017-03-23 19:54:30', '', 300, 'http://fishnchips.pixd.com.br/300-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_termmeta`
--

CREATE TABLE `fc_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_terms`
--

CREATE TABLE `fc_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  `term_order` int(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_terms`
--

INSERT INTO `fc_terms` (`term_id`, `name`, `slug`, `term_group`, `term_order`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0, 0),
(3, 'Frutos do mar', 'frutos-do-mar', 0, 2),
(4, 'Doces e sobremesas', 'doces-e-sobremesas', 0, 3),
(5, 'burguers', 'hamburguer', 0, 4),
(7, 'Promoção', 'promocao', 0, 6),
(8, 'Segunda Feira', 'segunda-feira', 0, 1),
(9, 'Terça Feira', 'terca-feira', 0, 2),
(10, 'Quarta Feira', 'quarta-feira', 0, 3),
(11, 'Quinta Feira', 'quinta-feira', 0, 4),
(12, 'Sexta Feira', 'sexta-feira', 0, 5),
(13, 'O que é', 'o-que-e', 0, 0),
(14, 'Como participar', 'como-participar', 0, 0),
(15, 'Benefícios', 'beneficios', 0, 0),
(16, 'Notícias', 'noticias', 0, 0),
(17, 'Dicas', 'dicas', 0, 0),
(20, 'Tradicional', 'tradicional', 0, 5),
(24, 'segunda a Sexta', 'segunda-a-sexta', 0, 0),
(25, 'Combinados', 'combinados', 0, 0),
(26, 'Executivos', 'executivos', 0, 0),
(27, 'Petiscos', 'petiscos', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_term_relationships`
--

CREATE TABLE `fc_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_term_relationships`
--

INSERT INTO `fc_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(214, 7, 0),
(214, 10, 0),
(215, 3, 0),
(215, 7, 0),
(215, 9, 0),
(217, 7, 0),
(217, 11, 0),
(220, 7, 0),
(220, 12, 0),
(227, 15, 0),
(228, 15, 0),
(229, 15, 0),
(230, 15, 0),
(278, 7, 0),
(278, 8, 0),
(281, 13, 0),
(286, 20, 0),
(286, 24, 0),
(295, 24, 0),
(301, 4, 0),
(305, 27, 0),
(306, 5, 0),
(308, 13, 0),
(309, 13, 0),
(310, 14, 0),
(311, 14, 0),
(312, 15, 0),
(335, 5, 0),
(338, 25, 0),
(339, 4, 0),
(342, 25, 0),
(343, 4, 0),
(345, 25, 0),
(346, 27, 0),
(348, 25, 0),
(349, 25, 0),
(351, 25, 0),
(354, 27, 0),
(355, 27, 0),
(356, 27, 0),
(359, 27, 0),
(362, 27, 0),
(365, 27, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_term_taxonomy`
--

CREATE TABLE `fc_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_term_taxonomy`
--

INSERT INTO `fc_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(3, 3, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/duascarinhas.svg', 0, 1),
(4, 4, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn.svg', 0, 3),
(5, 5, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hamburger-meal.svg', 0, 2),
(7, 7, 'categoriaCardapio', '', 0, 5),
(8, 8, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn.svg|', 7, 1),
(9, 9, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn.svg|', 7, 1),
(10, 10, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/prawn.svg|', 7, 1),
(11, 11, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/duascarinhas.svg|', 7, 1),
(12, 12, 'categoriaCardapio', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/hot-fish-bone.svg|', 7, 1),
(13, 13, 'categoriaClube', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/presente.svg|“Por Mil Caracóis!” Ganhar brindes especiais e descontos imperdíveis na adesão é uma vantagem e tanto. ', 0, 3),
(14, 14, 'categoriaClube', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/duascarinhas.svg | Fiel tripulante que é fiel tripulante faz a adesão por um valor único e aproveita dos ', 0, 2),
(15, 15, 'categoriaClube', 'http://fishnchips.pixd.com.br/wp-content/uploads/2016/06/porcentagem.svg|Nullam at commodo ligula nulla rutrum finibus velit scelerisque ante imperdiet', 0, 5),
(16, 16, 'category', '', 0, 0),
(17, 17, 'category', '', 0, 0),
(20, 20, 'categoriaCardapio', '', 0, 0),
(24, 24, 'categoriaCardapio', '', 7, 0),
(25, 25, 'categoriaCardapio', '', 0, 6),
(26, 26, 'categoriaCardapio', '', 0, 0),
(27, 27, 'categoriaCardapio', '', 0, 7);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_usermeta`
--

CREATE TABLE `fc_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_usermeta`
--

INSERT INTO `fc_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'fishnchips'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'fc_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(11, 1, 'fc_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', ''),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'session_tokens', 'a:4:{s:64:\"71f012a390fcd3da41495e7830adc43d41ba53d2d05c3e2b2a70cce55d29e94e\";a:4:{s:10:\"expiration\";i:1490291399;s:2:\"ip\";s:14:\"187.95.127.204\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36\";s:5:\"login\";i:1490118599;}s:64:\"d9ef3bff290bbd17b4b4dbee5591bf037a691268f79f8e9c831745667c4795c1\";a:4:{s:10:\"expiration\";i:1490375944;s:2:\"ip\";s:14:\"187.95.127.204\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36\";s:5:\"login\";i:1490203144;}s:64:\"5ec931a5d465fde67a58081dbb77bf280270d1ac24a2e8e7edb5eae4934cfc6d\";a:4:{s:10:\"expiration\";i:1490386869;s:2:\"ip\";s:14:\"187.95.127.204\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36\";s:5:\"login\";i:1490214069;}s:64:\"21aa67b9b00c646c775afd43c42c515896bf9807f82b103f5e7b81d37f9af806\";a:4:{s:10:\"expiration\";i:1490447952;s:2:\"ip\";s:14:\"187.95.127.204\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36\";s:5:\"login\";i:1490275152;}}'),
(15, 1, 'fc_dashboard_quick_press_last_post_id', '283'),
(16, 1, 'wpseo_ignore_tour', '1'),
(17, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(18, 1, 'metaboxhidden_dashboard', 'a:3:{i:0;s:18:\"dashboard_activity\";i:1;s:21:\"dashboard_quick_press\";i:2;s:17:\"dashboard_primary\";}'),
(19, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:63:\"dashboard_right_now,wpseo-dashboard-overview,dashboard_activity\";s:4:\"side\";s:39:\"dashboard_quick_press,dashboard_primary\";s:7:\"column3\";s:0:\"\";s:7:\"column4\";s:0:\"\";}'),
(20, 1, 'closedpostboxes_eventos', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(21, 1, 'metaboxhidden_eventos', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(22, 1, 'meta-box-order_eventos', 'a:3:{s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:41:\"wpseo_meta,detalhesMetaboxEventos,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(23, 1, 'screen_layout_eventos', '2'),
(24, 1, 'closedpostboxes_equipe', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(25, 1, 'metaboxhidden_equipe', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(26, 1, 'fc_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1463424635;}'),
(27, 1, 'wysija_pref', 'YTowOnt9'),
(28, 1, 'fc_user-settings', 'libraryContent=browse&editor=html&post_dfw=off&hidetb=1&advImgDetails=show'),
(29, 1, 'fc_user-settings-time', '1490277985'),
(30, 1, 'closedpostboxes_cardapio', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(31, 1, 'metaboxhidden_cardapio', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(32, 1, 'wpseo_seen_about_version', '3.2.5'),
(33, 1, 'meta-box-order_cardapio', 'a:3:{s:4:\"side\";s:43:\"submitdiv,categoriaCardapiodiv,postimagediv\";s:6:\"normal\";s:42:\"detalhesMetaboxCardapio,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(34, 1, 'screen_layout_cardapio', '2'),
(35, 1, 'fc_yoast_notifications', 'a:3:{i:0;a:2:{s:7:\"message\";s:778:\"We\'ve noticed you\'ve been using Yoast SEO for some time now; we hope you love it! We\'d be thrilled if you could <a href=\"https://yoa.st/rate-yoast-seo?utm_content=4.5\">give us a 5 stars rating on WordPress.org</a>!\n\nIf you are experiencing issues, <a href=\"https://yoa.st/bugreport?utm_content=4.5\">please file a bug report</a> and we\'ll do our best to help you out.\n\nBy the way, did you know we also have a <a href=\'https://yoa.st/premium-notification?utm_content=4.5\'>Premium plugin</a>? It offers advanced features, like a redirect manager and support for multiple keywords. It also comes with 24/7 personal support.\n\n<a class=\"button\" href=\"http://fishnchips.pixd.com.br/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell\">Please don\'t show me this notification anymore</a>\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-upsell-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.8000000000000000444089209850062616169452667236328125;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:1;a:2:{s:7:\"message\";s:172:\"Don\'t miss your crawl errors: <a href=\"http://fishnchips.pixd.com.br/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">connect with Google Search Console here</a>.\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:2;a:2:{s:7:\"message\";s:226:\"<strong>Huge SEO Issue: You\'re blocking access to robots.</strong> You must <a href=\"http://fishnchips.pixd.com.br/wp-admin/options-reading.php\">go to your Reading Settings</a> and uncheck the box for Search Engine Visibility.\";s:7:\"options\";a:8:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:32:\"wpseo-dismiss-blog-public-notice\";s:5:\"nonce\";N;s:8:\"priority\";i:1;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}}'),
(36, 1, 'closedpostboxes_clube', 'a:2:{i:0;s:12:\"postimagediv\";i:1;s:10:\"wpseo_meta\";}'),
(37, 1, 'metaboxhidden_clube', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(38, 1, 'meta-box-order_clube', 'a:3:{s:4:\"side\";s:40:\"submitdiv,categoriaClubediv,postimagediv\";s:6:\"normal\";s:39:\"detalhesMetaboxClube,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(39, 1, 'screen_layout_clube', '2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_users`
--

CREATE TABLE `fc_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `fc_users`
--

INSERT INTO `fc_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'fishnchips', '$P$B2fPPsoTlIib6RTvyEeddV/MEYZhpZ1', 'fishnchips', 'fishnchips@palupa.com.br', '', '2016-05-16 14:51:10', '', 0, 'fishnchips');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_campaign`
--

CREATE TABLE `fc_wysija_campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_campaign_list`
--

CREATE TABLE `fc_wysija_campaign_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `filter` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_custom_field`
--

CREATE TABLE `fc_wysija_custom_field` (
  `id` mediumint(9) NOT NULL,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_email`
--

CREATE TABLE `fc_wysija_email` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `modified_at` int(10) UNSIGNED DEFAULT NULL,
  `sent_at` int(10) UNSIGNED DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_opened` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_unsub` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_bounce` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_forward` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `fc_wysija_email`
--

INSERT INTO `fc_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(2, 0, 'Confirme sua assinatura em Fish n&#039; Chips', 'Olá!\n\nLegal! Você se inscreveu como assinante em nosso site.\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \n\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\n\nObrigado,\n\nA equipe!\n', 1463410864, 1463410864, NULL, 'info@localhost', 'fishnchips', 'info@localhost', 'fishnchips', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL),
(3, 0, 'Confirme sua assinatura em Fish n\' Chips', 'Olá!\r\n\r\nLegal! Você se inscreveu como assinante em nosso site.\r\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \r\n\r\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\r\n\r\nObrigado,\r\n\r\nA equipe!\r\n', 1463410866, 1463410866, NULL, 'info@localhost', 'fishnchips', 'info@localhost', 'fishnchips', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_email_user_stat`
--

CREATE TABLE `fc_wysija_email_user_stat` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `sent_at` int(10) UNSIGNED NOT NULL,
  `opened_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_email_user_url`
--

CREATE TABLE `fc_wysija_email_user_url` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `clicked_at` int(10) UNSIGNED DEFAULT NULL,
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_form`
--

CREATE TABLE `fc_wysija_form` (
  `form_id` int(10) UNSIGNED NOT NULL,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `fc_wysija_form`
--

INSERT INTO `fc_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(3, 'Fish ‘n’ Chips', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjU6e3M6NzoiZm9ybV9pZCI7czoxOiIzIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjMiO31zOjEwOiJvbl9zdWNjZXNzIjtzOjc6Im1lc3NhZ2UiO3M6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo3MzoiVmVqYSBzdWEgY2FpeGEgZGUgZW50cmFkYSBvdSBwYXN0YSBkZSBzcGFtIHBhcmEgY29uZmlybWFyIHN1YSBhc3NpbmF0dXJhLiI7czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6Mzp7czo3OiJibG9jay0xIjthOjU6e3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6NDoiTm9tZSI7fXM6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6OToiZmlyc3RuYW1lIjtzOjQ6Im5hbWUiO3M6NDoiTm9tZSI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjY6InBhcmFtcyI7YToyOntzOjU6ImxhYmVsIjtzOjU6IkVtYWlsIjtzOjg6InJlcXVpcmVkIjtzOjE6IjEiO31zOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjU6ImlucHV0IjtzOjU6ImZpZWxkIjtzOjU6ImVtYWlsIjtzOjQ6Im5hbWUiO3M6NToiRW1haWwiO31zOjc6ImJsb2NrLTMiO2E6NTp7czo2OiJwYXJhbXMiO2E6MTp7czo1OiJsYWJlbCI7czo4OiJBc3NpbmFyISI7fXM6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6Njoic3VibWl0IjtzOjU6ImZpZWxkIjtzOjY6InN1Ym1pdCI7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7fX1zOjc6ImZvcm1faWQiO2k6Mzt9', NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_list`
--

CREATE TABLE `fc_wysija_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_public` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `fc_wysija_list`
--

INSERT INTO `fc_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(2, 'Usuários do WordPress', 'users', 'A lista criada automaticamente na importação dos assinantes do plugin : \"WordPress', 0, 0, 0, 0, 1463410863, 0),
(4, 'Novidades  Fish \'n\' Chips', 'novidades-fish-n-chips', '', 0, 0, 1, 0, 1466518137, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_queue`
--

CREATE TABLE `fc_wysija_queue` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `send_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_url`
--

CREATE TABLE `fc_wysija_url` (
  `url_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_url_mail`
--

CREATE TABLE `fc_wysija_url_mail` (
  `email_id` int(11) NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `unique_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_user`
--

CREATE TABLE `fc_wysija_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `wpuser_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) UNSIGNED DEFAULT NULL,
  `last_opened` int(10) UNSIGNED DEFAULT NULL,
  `last_clicked` int(10) UNSIGNED DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `fc_wysija_user`
--

INSERT INTO `fc_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, 'fishnchips@palupa.com.br', '', '', '', '0', NULL, NULL, NULL, '0665e122672f66910276c952cc66cb7d', 1463410864, 1, 'palupa.com.br'),
(2, 0, 'gabrielcarolinol@gmail.com', 'Hudson Gabriel', '', '127.0.0.1', '187.95.126.204', 1466622504, NULL, NULL, '7b639f01c297f48c5baa66a81ed5567e', 1466520018, 1, 'gmail.com');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_user_field`
--

CREATE TABLE `fc_wysija_user_field` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) UNSIGNED DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `fc_wysija_user_field`
--

INSERT INTO `fc_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Nome', 'firstname', 0, NULL, '', 0, 'Por favor, digite seu nome'),
(2, 'Sobrenome', 'lastname', 0, NULL, '', 0, 'Por favor, digite seu sobrenome');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_user_history`
--

CREATE TABLE `fc_wysija_user_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) UNSIGNED DEFAULT NULL,
  `executed_by` int(10) UNSIGNED DEFAULT NULL,
  `source` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `fc_wysija_user_list`
--

CREATE TABLE `fc_wysija_user_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_date` int(10) UNSIGNED DEFAULT '0',
  `unsub_date` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `fc_wysija_user_list`
--

INSERT INTO `fc_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(2, 1, 1463410863, 0),
(3, 2, 1466622504, 0);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `fc_commentmeta`
--
ALTER TABLE `fc_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `disqus_dupecheck` (`meta_key`(191),`meta_value`(11));

--
-- Índices de tabela `fc_comments`
--
ALTER TABLE `fc_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Índices de tabela `fc_links`
--
ALTER TABLE `fc_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Índices de tabela `fc_options`
--
ALTER TABLE `fc_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Índices de tabela `fc_postmeta`
--
ALTER TABLE `fc_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `fc_posts`
--
ALTER TABLE `fc_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Índices de tabela `fc_termmeta`
--
ALTER TABLE `fc_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `fc_terms`
--
ALTER TABLE `fc_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Índices de tabela `fc_term_relationships`
--
ALTER TABLE `fc_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Índices de tabela `fc_term_taxonomy`
--
ALTER TABLE `fc_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Índices de tabela `fc_usermeta`
--
ALTER TABLE `fc_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `fc_users`
--
ALTER TABLE `fc_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Índices de tabela `fc_wysija_campaign`
--
ALTER TABLE `fc_wysija_campaign`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Índices de tabela `fc_wysija_campaign_list`
--
ALTER TABLE `fc_wysija_campaign_list`
  ADD PRIMARY KEY (`list_id`,`campaign_id`);

--
-- Índices de tabela `fc_wysija_custom_field`
--
ALTER TABLE `fc_wysija_custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `fc_wysija_email`
--
ALTER TABLE `fc_wysija_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Índices de tabela `fc_wysija_email_user_stat`
--
ALTER TABLE `fc_wysija_email_user_stat`
  ADD PRIMARY KEY (`user_id`,`email_id`);

--
-- Índices de tabela `fc_wysija_email_user_url`
--
ALTER TABLE `fc_wysija_email_user_url`
  ADD PRIMARY KEY (`user_id`,`email_id`,`url_id`);

--
-- Índices de tabela `fc_wysija_form`
--
ALTER TABLE `fc_wysija_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Índices de tabela `fc_wysija_list`
--
ALTER TABLE `fc_wysija_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Índices de tabela `fc_wysija_queue`
--
ALTER TABLE `fc_wysija_queue`
  ADD PRIMARY KEY (`user_id`,`email_id`),
  ADD KEY `SENT_AT_INDEX` (`send_at`);

--
-- Índices de tabela `fc_wysija_url`
--
ALTER TABLE `fc_wysija_url`
  ADD PRIMARY KEY (`url_id`);

--
-- Índices de tabela `fc_wysija_url_mail`
--
ALTER TABLE `fc_wysija_url_mail`
  ADD PRIMARY KEY (`email_id`,`url_id`);

--
-- Índices de tabela `fc_wysija_user`
--
ALTER TABLE `fc_wysija_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `EMAIL_UNIQUE` (`email`);

--
-- Índices de tabela `fc_wysija_user_field`
--
ALTER TABLE `fc_wysija_user_field`
  ADD PRIMARY KEY (`field_id`);

--
-- Índices de tabela `fc_wysija_user_history`
--
ALTER TABLE `fc_wysija_user_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Índices de tabela `fc_wysija_user_list`
--
ALTER TABLE `fc_wysija_user_list`
  ADD PRIMARY KEY (`list_id`,`user_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `fc_commentmeta`
--
ALTER TABLE `fc_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_comments`
--
ALTER TABLE `fc_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_links`
--
ALTER TABLE `fc_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_options`
--
ALTER TABLE `fc_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3094;
--
-- AUTO_INCREMENT de tabela `fc_postmeta`
--
ALTER TABLE `fc_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1318;
--
-- AUTO_INCREMENT de tabela `fc_posts`
--
ALTER TABLE `fc_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=388;
--
-- AUTO_INCREMENT de tabela `fc_termmeta`
--
ALTER TABLE `fc_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_terms`
--
ALTER TABLE `fc_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de tabela `fc_term_taxonomy`
--
ALTER TABLE `fc_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de tabela `fc_usermeta`
--
ALTER TABLE `fc_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de tabela `fc_users`
--
ALTER TABLE `fc_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `fc_wysija_campaign`
--
ALTER TABLE `fc_wysija_campaign`
  MODIFY `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_wysija_custom_field`
--
ALTER TABLE `fc_wysija_custom_field`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_wysija_email`
--
ALTER TABLE `fc_wysija_email`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `fc_wysija_form`
--
ALTER TABLE `fc_wysija_form`
  MODIFY `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `fc_wysija_list`
--
ALTER TABLE `fc_wysija_list`
  MODIFY `list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `fc_wysija_url`
--
ALTER TABLE `fc_wysija_url`
  MODIFY `url_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_wysija_url_mail`
--
ALTER TABLE `fc_wysija_url_mail`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `fc_wysija_user`
--
ALTER TABLE `fc_wysija_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `fc_wysija_user_field`
--
ALTER TABLE `fc_wysija_user_field`
  MODIFY `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `fc_wysija_user_history`
--
ALTER TABLE `fc_wysija_user_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
