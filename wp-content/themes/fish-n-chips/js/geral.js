
	$(function(){
		
		$(document).ready(function(){
			var alturaMenu = $(document).height();
			$('#topo').css({"height":alturaMenu})

		});			
	
		// MENU SIDEBAR
		$(document).ready(function(){
			
			$("#botao-sidebarAbrir").click(function(e){
				$("#botao-sidebarFechar").css({"display":"block"});
				$("#botao-sidebarAbrir").css({"display":"none"});
				$(".topo").css({
					"left":"0",
					"transition":"All 1s ease",
					"-webkit-transition":"All 1s ease",
					"-moz-transition":"All 1s ease",
					"-o-transition":"All 1s ease",
				});


			});
			$("#botao-sidebarFechar").click(function(e){
				$("#botao-sidebarAbrir").css({"display":"block"});
				$("#botao-sidebarFechar").css({"display":"none"});
				$(".topo").css({"left":"-190px"});
			});


		});
		/*****************************************
			CARROSSEL DE DESTAQUE
		*******************************************/
		$(document).ready(function() {

	  		$("#carrossel-destaque-inicial").owlCarousel({
	 
				items : 1,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,			  
	  		});
	  		var carrossel_destaque = $("#carrossel-destaque-inicial").data('owlCarousel');
			$('.navegacaodestaqueTras').click(function(){ carrossel_destaque.prev(); });
			$('.navegacaodestaqueFrent').click(function(){ carrossel_destaque.next(); });

		});
		
		/*****************************************
				CARROSSEL DE PROMOÇÃO INICIAL
		*******************************************/
		$(document).ready(function() {

	  		$("#carrossel-promocoes-inicial").owlCarousel({
	 
				items : 1,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,			  
	  		});
	  		var carrossel_promocao = $("#carrossel-promocoes-inicial").data('owlCarousel');
			$('.navegacaoPromocoesFrent').click(function(){ carrossel_promocao.prev(); });
			$('.navegacaoPromocoesTras').click(function(){ carrossel_promocao.next(); });

	 	});

		/*****************************************
			CARROSSEL DE CARDÁPIO PÁGINA INICIAL
		*******************************************/
		$(document).ready(function() {

			$("#carrossel-cardapio-inicial").owlCarousel({

			 	items : 3,
		        dots: true,
		        loop: true,
		        lazyLoad: true,	         
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,	
			    responsiveClass:true,
			    responsive:{
			        0:{
			            items:1,
			            
			        },
			        768:{
			            items:2,
			            
			        },
			        1190:{
			            items:3,
			            
			        }
			    }		  
			});
			var carrossel_cardapio = $("#carrossel-cardapio-inicial").data('owlCarousel');
			$('.navegacaoCardapioFrent').click(function(){ carrossel_cardapio.prev(); });
			$('.navegacaoCardapioTras').click(function(){ carrossel_cardapio.next(); });

		});

	 	/*****************************************
			CARROSSEL DE CARDÁPIO PÁGINA CARDÁPIO
		*******************************************/
	 	$(document).ready(function() {
	 
	   		$("#carrossel-pratos-cardapio").owlCarousel({
	 
	   			items : 5,
	   			dots: true,	        
	   			lazyLoad: true,        

	   			autoplayTimeout:5000,
	   			autoplayHoverPause:true,
	   			animateOut: 'fadeOut',
	   			smartSpeed: 450,
	   			autoplaySpeed: 4000,	
	   			responsiveClass:true,
			    responsive:{
			         0:{
			            items:1,
			            
			        },
			        540:{
			            items:2,
			            
			        },
			        768:{
			            items:3,
			            
			        },
			        1190:{
			            items:5,
			            
			        }
			    }		  						  
	   		});
	   		var carrossel_pratos_cardapio = $("#carrossel-pratos-cardapio").data('owlCarousel');
			$('.navegacaoPGcardapioFrent').click(function(){ carrossel_pratos_cardapio.prev(); });
			$('.navegacaoPGcardapioTras').click(function(){ carrossel_pratos_cardapio.next(); });

	 	});

		//MORDALL PÁGINA INICIAL ÁREA CLUB
		$(document).ready(function(){
			var alturaModalClub = $('.clube-inicial').height();
			$('.lente-clube-inicial').css({"height":alturaModalClub})

		});

		/************************************************
			SCRIPTS PÁGINA CLUBE
		************************************************/
		//HOVER SIDEBAR CLUB
		$(document).ready(function(){
			
			// PEGANDO ALTURA DA SIDEBAR UL
			var alturaSidebarClub = $('.sidebar-club').height();
			//PEGANDO ALTURA DO INTENS DA SIDEBAR LI
			var alturaSidebaLIrClub = $('.sidebar-club').children().height();
			
			$('.caixa-overflow').css({"height":alturaSidebaLIrClub})
			
			// EFEITO HOVER
			$('.caixa-overflow').hover(function(e){

				$(this).css({
					"height":alturaSidebarClub,
					"transition":"All .5s ease",
					"-webkit-transition":"All .5s ease",
					"-moz-transition":"All .5s ease",
					"-o-transition":"All .5s ease",
				});
			
			},
			
			function(){
				$('.caixa-overflow').css({"height":alturaSidebaLIrClub})

			});
			
		});

		

		//HOVER SIDEBAR CLUB NA AREA INTEIRA DA PÁGINA
		$(document).ready(function(){
			
			// PEGANDO ALTURA DA SIDEBAR UL
			var alturaSidebarClubBaixo = $('.sidebar-clubBaixo').height();
			//PEGANDO ALTURA DO INTENS DA SIDEBAR LI
			var alturaSidebaLIrClubBaixo = $('.sidebar-clubBaixo').children().height();
			
			$('.caixa-overflowBaixo').css({"height":alturaSidebaLIrClubBaixo})
			
			// EFEITO HOVER
			$('.caixa-overflowBaixo').hover(function(){

				$(this).css({
					"height":alturaSidebarClubBaixo,
					"transition":"All 1s ease",
					"-webkit-transition":"All 1s ease",
					"-moz-transition":"All 1s ease",
					"-o-transition":"All 1s ease",
				});
			
			},
			
			function(){
				$('.caixa-overflowBaixo').css({"height":alturaSidebaLIrClubBaixo})

			});
		
		});

		//MODAL VALORES CLUBE PÁGINA INICIAL
		$(document).ready(function(){
			
			$(".club-valores-text-inicial").click(function(e){
				$(this).fadeIn().css({
					"width":"100%",					
					"height":"100%",					
					"background":"#55b0ff",	
					"transition":"All 1s ease",
					"-webkit-transition":"All 1s ease",
					"-moz-transition":"All 1s ease",
					"-o-transition":"All 1s ease",
					
				});
				$(this).children('p').hide();
				$(this).children('.texto-modal').fadeIn(2000);		

				$("#lente-clube-inicial").css({
					"visibility":"visible",
					"opacity":"1",	
					"transition":"All 1s ease",
					"-webkit-transition":"All .4s ease",
					"-moz-transition":"All .4s ease",
					"-o-transition":"All .4s ease",				
				});


			});
			// FECHAR MODAL E LENTE 
			$("#lente-clube-inicial").click(function(e){
					$("#lente-clube-inicial").css({
					"visibility":"hidden",
					"opacity":"0",	
					"transition":"All 1s ease",
					"-webkit-transition":"All .4s ease",
					"-moz-transition":"All .4s ease",
					"-o-transition":"All .4s ease",				
				});

				$('.club-valores-text-inicial').fadeIn().css({
					"width":" 85px",					
					"height":"325px",					
					"background":"#fff",	
					"transition":"All 1s ease",
					"-webkit-transition":"All 1s ease",
					"-moz-transition":"All 1s ease",
					"-o-transition":"All 1s ease",
					
				});
				$('.club-valores-text-inicial').children('p').fadeIn(3000);

				$('.club-valores-text-inicial').children('.texto-modal').hide();

			});				

		});

	 	/*****************************************
			CARROSSEL DE DIAS DA SEMANA PÁGINA PROMOÇÃO
		*******************************************/
		$(document).ready(function() {
	 
	   		$("#carrossel-dias-promocoes").owlCarousel({
	 
	   			items : 4,
	   			dots: true,	        
	   			lazyLoad: true,      
	   			loop: true,
	   			autoplayTimeout:5000,
	   			autoplayHoverPause:true,
	   			animateOut: 'fadeOut',
	   			smartSpeed: 450,
	   			autoplaySpeed: 4000,
	   			responsiveClass:true,
			    responsive:{
			         0:{
			            items:1,
			            
			        },
			        540:{
			            items:2,
			            
			        },
			        768:{
			            items:2,
			            
			        },
			        1190:{
			            items:4,
			            
			        }
			    }		  				  
	   		});
	   		var carrossel_dias_promocao = $("#carrossel-dias-promocoes").data('owlCarousel');
			 $('.navegacaoPGPromocoesFrent').click(function(){ carrossel_dias_promocao.prev(); });
			 $('.navegacaoPGPromocoesTras').click(function(){ carrossel_dias_promocao.next(); });

	 	});

		/*****************************************
			CARROSSEL DE VALORES ÁREA CLUBE  PÁGINA INICIAL
		*******************************************/
		$(document).ready(function() {
	 
	   		$("#club-valores-ul-inicial").owlCarousel({
	 
			 	items : 7,
		        dots: true,
		        loop: true,
		        lazyLoad: true,	         
		        autoplay:true,
		        mouseDrag: true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,	
			    responsiveClass:true,
			    responsive:{	       
			    	0:{
			    		items:1,
			    	},
			    	320:{
			    		items:1,
			    	},
			    	400:{
			    		items:3,
			    	},
			    	768:{
			    		items:4,
			    	},
			        1090:{
			            items:5,		            
			        },
			        1450:{
			            items:7,		            
			        }
			    }		  
	   		});
	   		var carrossel_cardapio = $("#club-valores-ul-inicial").data('owlCarousel');
			 $('.navegacaoCardapioFrent').click(function(){ carrossel_cardapio.prev(); });
			 $('.navegacaoCardapioTras').click(function(){ carrossel_cardapio.next(); });

	 	});

	 	// MORDALL QUEM SOMOS PEGANDO TAMANHO
		$(document).ready(function(){
			var alturaModal = $('.itegrnates-quem-somos').height();
			$('.lente-itegrnates-quem-somos').css({"height":alturaModal})

		});

		/****************************************************
			SCIRPTS PÁGINA QUEM SOMOS
		****************************************************/
		$(document).ready(function(){
			
			$(".perfil-integrantes").click(function(e){
				
				$(this).children('.modal-quem-somos').css({
					"visibility":"visible",
					"opacity":"1",
					"transition":"All 1s ease",
					"-webkit-transition":"All 1s ease",
					"-moz-transition":"All 1s ease",
					"-o-transition":"All 1s ease",
				});
				
				$("#modal").fadeIn().css({
					"visibility":"visible",
					"opacity":"1",
					"transition":"All 1s ease",
					"-webkit-transition":"All 1s ease",
					"-moz-transition":"All 1s ease",
					"-o-transition":"All 1s ease",
				});
				
			});
			$("#modal").click(function(e){
				$("#modal").fadeIn().css({
					"visibility":"hidden",
					"opacity":"0",					
				});
				$(".modal-quem-somos").fadeIn().css({
					"visibility":"hidden",
					"opacity":"0",					
				});
			});


		});

		$('a#botao-direcao-modal').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});

		/***************************************************
			SCRIPT PÁGINA PROMOÇÕES 
		****************************************************/
		$(document).ready(function() {

			$('.data-categoriadata').hide();

			var hoje=new Date();
			var dia= hoje.getDay();
			var semana=new Array(6);
			semana[0]='domingo';
			semana[1]='segunda-feira';
			semana[2]='terca-feira';
			semana[3]='quarta-feira';
			semana[4]='quinta-feira';
			semana[5]='sexta-feira';
			semana[6]='sábado';
			var diaPtBr = semana[dia];
			$('#' +diaPtBr).show();
			$('.' + diaPtBr).css('background','#000');
			$(".dia-semana-promocoes").click(function(e){
				var slugDias =	$(this).attr("data-nomeDias");
				$('.data-categoriadata').hide();
				$('#' +slugDias).show();

				$(".dia-semana-promocoes").css('background','#42a7ff');
				$(this).css('background','#000');
			});


		});

		
	});


	