<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fish_n\'_Chips
 */

$fotoBlog = $configuracao['opt-blog-foto']['url'];
get_header(); ?>

	<!-- PÁGINA BLOG -->
	<div class="pg pg-blog">
		
		<div class="bg-blog-topo" style="background:url(<?php echo $fotoBlog ?>)"></div>

		<!-- SUB TÍTULOS PROMOÇÕES -->
		<div class="text-center">
			<p class="sub-titulo-descricao-paginas">últimas<b>do blog</b><span></span></p>	
		</div>

		<section class="blog">
			<div class="row">
			
				<?php echo get_sidebar(); ?>				
				
				<!-- ÁREA DE POSTS BLOG -->
				<div class="col-md-10">
					<div class="areapost-blog">
						
						<ul>
						<?php 
							$the_query = new WP_Query( 'posts_per_page=-1' ); 
							
			            	
							while ($the_query -> have_posts()) : $the_query -> the_post();
							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];
							
								
								
						?>
							<!-- POST -->
							<li>
								
								<a href="<?php echo get_permalink(); ?>" class="post-blog" style="background:url(<?php echo $foto ?>)">
									
									<div class="lente-post-blog">
										
										<div class="data">
											<!-- DATA MÊS DO POST -->
											<small><?php the_time('j') ?></small><span><?php the_time('F \d\e Y') ?></span>	
										</div>	

										<!-- BREVE DESCRIÇÃO -->
										<p><?php echo get_the_title() ?> </p>									

									</div>

								</a>
								
								<!-- BREVE DESCRIÇÃO -->
								<p class="breve-descricao-blog"><?php echo get_the_title() ?></p>

							</li>
						
						<?php endwhile; wp_reset_query(); ?>	
						</ul>
						
						<div class="paginador-blog">
								
							<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
							
						</div>
					</div>

				</div>

			</div>

		</section>

	</div>

<?php

get_footer();
