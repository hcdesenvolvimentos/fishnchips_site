<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fish_n\'_Chips
 */

$fotoCabecalho = $configuracao['opt-cardapio-imagem']['url'];
$descricaoCabecalho = $configuracao['opt-cardapio-frase'];
get_header(); ?>
<!-- PÁGINA PROMOÇÕES -->
	<div class="pg pg-promoçoes" style="display:">
		<!-- SUB TÍTULOS PROMOÇÕES -->
		
		<div class="titulo-pagina-cardapio">
			<p class="sub-titulo-descricao-paginas">Nosso<b>promocoes</b></p>
			<span>Duis congue aliquam porttitor. Nam vel dolor scelerisque tellus pharetra aliquam.</span>
		</div>

		
		<!-- CARROSSEL DIAS DA SEMANA   -->
		<section class="carrossel-dias-promocoes">
			<!-- BOTÕES DO CARROSSEL  -->
			<button class="navegacaoPGPromocoesFrent"><i class="fa fa-angle-left"></i></button>
			<button class="navegacaoPGPromocoesTras"><i class="fa fa-angle-right"></i></button>

			<div id="carrossel-dias-promocoes" class="owl-Carousel">			
			<?php

				// DEFININDO TAXONOMIA PAI
				$categoriaPai = get_term_by('slug', 'promocao', 'categoriaCardapio');
				
				//ID TAXONOMIA PAI
				$id = $categoriaPai->term_id;

				//PASSANDO ID DA CATEGORIA PAI / LOOP CATEGORIAS FILHAS 
				$categoriaCardapiosFilhos = get_terms( 'categoriaCardapio' , array(
					'orderby'    => 'count',
					'hide_empty' => 0,
					'child_of'     => 0,
					'parent'       => $id,
					'orderby'      => 'name',
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0
				));
					
				// FOR CATEGORIAS FILHAS 
				foreach ($categoriaCardapiosFilhos as $categoriaCardapiosFilho) {
					//PEGANDO NOME CATEGORIA FILHA 
					$nomeFilho = $categoriaCardapiosFilho->name;
					//PEGANDO SLUG CATEGORIA FILHA 
					$slugFilho = $categoriaCardapiosFilho->slug;

					// EXECUTANDO A QUERY 
					$postCardapio = new WP_Query(array(
					    'post_type' => 'cardapio',
					    'posts_per_page' 	=> -1,
					    'tax_query' 		=> array(
														array(
															'taxonomy' => 'categoriaCardapio',
															'field'    => 'slug',
															'terms'    => $slugFilho,
														)
													)
					    )
					);
			?>

				<div class="item">
					<!-- DIA DA SEMANA -->
					<a href="<?php echo get_category_link($categoriaCardapiosFilho->term_id); ?>" class="dia-semana-promocoes">
						<p><?php echo $nomeFilho;?></p>
					</a>
				</div>
			<?php } ?>	
			</div>

		</section>
		
		<!-- ÁREA PRA PROMOÇÃO  -->
		<section class="area-promocao-pg-promocao">
			
			<ul>
				
				<li class="conteudo">
					<!-- TÍTULO DA PROMOÇÃO  -->
					<div class="titulo-promocao">
						<p>Segunda Promocao -01</p>
						<span>Lorem Ipsum Dolor</span>
					</div>	

					<div class="itens-prato-promocao">
						<?php 

							// LOOP DA FOTO DESTACADA
							if ( have_posts() ) : while( have_posts() ) : the_post();

							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				            $foto = $foto[0];
				            $preco = rwmb_meta('Fishnchips_prato_preco');
				            $icone = rwmb_meta('Fishnchips_prato_icone','type=image');
				          

						 ?>	
						<div class="item-prato-promocao">
						<?php 
							foreach ($icone as $icone) {
				          		$urlIcone = $icone;
				          	
						?>
							<img class="icone-promocao" src="<?php echo $urlIcone['url'] ?>" alt="">
						<?php } ?>
							<p><?php echo get_the_title() ?> <span><?php echo $preco ?></span></p>
						</div>
						<?php endwhile; endif;  ?>

						<p class="info-adicionais">Duis congue aliquam porttitor. Nam vel dolor scelerisque tellus pharetra aliquam.</p>
					</div>

					
				</li>

				<li class="foto-prato" id="foto-prato" style="background:url(img/foto-destaque-cardapio.png)">
					
				</li>



			</ul>			
		

		</section>


	</div>

<?php

get_footer();
