<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fish_n\'_Chips
 */

$fotoCabecalho = $configuracao['opt-cardapio-imagem']['url'];
$descricaoCabecalho = $configuracao['opt-cardapio-frase'];
get_header(); ?>
<!-- PÁGINA CARDÁPIO -->
<div class="pg pg-cardapio">
	
	<div class="bg-topo-cardapio" style="background:url(<?php echo $fotoCabecalho ?>)">
		
		<!-- SUB TÍTULOS PROMOÇÕES -->
		<div class="titulo-pagina-cardapio">
			<?php $categoriaTerm = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
			<p class="sub-titulo-descricao-paginas"><?php echo $categoriaTerm->name ?></p>
			<span><?php echo $descricaoCabecalho ?></span>	
		</div>

	</div>

	<!-- CARROSSEL DE PRATOS   -->
	<section class="carrossel-pratos-cardapio">
		<!-- BOTÕES DO CARROSSEL  -->
		<button class="navegacaoPGcardapioFrent"><i class="fa fa-angle-left"></i></button>
		<button class="navegacaoPGcardapioTras"><i class="fa fa-angle-right"></i></button>
		
		<div id="carrossel-pratos-cardapio" class="owl-Carousel">
			 <?php

				// DEFINE A TAXONOMIA
				$taxonomia = 'categoriaCardapio';

				// LISTA AS CATEGORIAS PAI DO CARDAPIO
				$categoriasCardapio = get_terms( $taxonomia, array(
					'orderby'    => 'count',
					'hide_empty' => 0,
					'parent'	 => 0
				));
				
				
				
				foreach ($categoriasCardapio as $categoriaCardapio) {
					//FILTRANDO CATEGORIA PROMOÇÃO
					if ($categoriaCardapio->name != "Promoção") {
						$nome = $categoriaCardapio->name;
						$categoriaAtivaImg = z_taxonomy_image_url($categoriaCardapio->term_id);
													
					// IMAGEM ICONE DA CATEGORIA
					
					// VERIFICANDO CATEGORIA ATUAL / ADICIONANDO CLASSE ATIVA NA ATUAL
					if ($categoriaTerm->name == $nome) {					
					
			?>
			
			<div class="item ativo">
				<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>">
					<div class="item-prato-cardapio">
						<p><?php echo $nome ?></p>
						
						<img src="<?php echo $categoriaAtivaImg ?> " class="icone" alt="">		
					</div>		
				</a>	
			</div>
			
			<?php  }else{  ?>
			
			<div class="item">
				<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>">
					<div class="item-prato-cardapio">
						<p><?php echo $nome ?></p>
						
						<img src="<?php echo $categoriaAtivaImg ?> " class="icone" alt="">		
					</div>		
				</a>	
			</div>

			<?php  } } }?>
		
		</div>

	</section>

	<!-- PRATOS DO CARDÁPIO -->
	<section class="todos-pratos-cardapio">
		<ul>
		<?php 

			$i = 0;
			// LOOP DA FOTO DESTACADA
			if ( have_posts() ) : while( have_posts() ) : the_post();

			$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
            $foto = $foto[0];
			
			if ($i % 2 == 0) {
					
			
		 ?>	
			<!-- PRATO -->
			<li>
				<!-- FOTO  -->
				<a href="<?php echo get_permalink(); ?>" class="foto-prato-cardapio" style="background:url(<?php echo $foto ?>)">
					
					<div class="lente-foto-prato-cardapio">
						<div class="info-prato-cardapioEsquerdo">
							<!-- NOME -->
							<h3><?php echo get_the_title() ?></h3>
							<hr>
							<!-- TEXTO -->
							<p>
							<?php
								$descricao = get_the_content();
								$textoresumido = $descricao;
								$textocurto = substr($textoresumido, 0, 100).'...';
								echo $textocurto;
							?>	
							</p> 
						</div>	
					</div>
				</a>
			</li>

			<?php }else{ ?>

			<!-- PRATO ESQUERDO-->
			<li>
				<!-- FOTO  -->
				<a href="<?php echo get_permalink(); ?>" class="foto-prato-cardapio" style="background:url(<?php echo $foto ?>)">
					
					<div class="lente-foto-prato-cardapio">
						<div class="info-prato-cardapioDireito">
							<!-- NOME -->
							<h3><?php echo get_the_title() ?></h3>
							<hr>
							<!-- TEXTO -->
								<p>
								<?php
									$descricao = get_the_content();
									$textoresumido = $descricao;
									$textocurto = substr($textoresumido, 0, 100).'...';
									echo $textocurto;
								?>	
							</p> 
						</div>	
					</div>
				</a>
			</li>

			<?php } ?>
		<?php   $i++; endwhile; endif;  ?>
			

		</ul>
		
	</section>

</div>

<?php

get_footer();
