<?php
/**
 * Template Name: Clube
 * Description: Página Clube do site da Fishnchips
 *
 * @package Fishnchips
 */

get_header(); ?>
<!-- PÁGINA CLUBE -->
	<div class="pg pg-clube">
		<!-- ÁREA SOBRE O CLUBE -->
		<section class="area-sobre-clube">
			<!-- CHAMADA CONHEÇA O CLUB  -->
			<div class="info-conheca-clube">
				<p>Conheca <small>nosso clube</small></p>
				<span><?php echo get_the_content() ?></span>
			</div>

			<div class="row">
				 <?php
					$i = 0;
					
					// DEFINE A TAXONOMIA
					$taxonomia = 'categoriaClube';

					// LISTA AS CATEGORIAS PAI DOS SABORES
					$categoriaClube = get_terms( $taxonomia, array(
						'orderby'    => 'count',
						'hide_empty' => 0,
						'parent'	 => 0
					));
					
					
					
					foreach ($categoriaClube as $categoria) {
						$nome = $categoria->name;
						$decricao = explode("|", $categoria->description);
						$img = $decricao[0];
						$resumo = $decricao[1];
						$categoriaAtivaImg = z_taxonomy_image_url($categoria->term_id);
						
						$taxonomiaSlug = $categoria->slug;
						if ($i == 3) { $i = 0;}
						if($i != 2)
						{
				?>
				
					<!-- ÁREA DE INFORMAÇÕES SOBRE O CLUB -->
					<div class="col-md-6 borda-foto-clube">
						<!-- FOTO DE FUNDO  -->
						<div class="foto-fundo-sobre-clube" style="background:url(<?php echo $categoriaAtivaImg ?>)">
							<!-- TEXTO SOBRE -->
							<div class="posicao-texto-clube">
								<div class="info-clube">

									<img src="<?php echo $img ?>" class="img-responsive icone" alt="">

									<?php

										if ($resumo == null) {
											echo '<p>'.$categoria->description.'</p>';
										}else{
											echo '<p>'.$resumo.'</p>';
										}
										
									 ?>
									
								</div>
								
								<!-- CHAMADA O QUE É O CLUB -->
								<div class="info-quem-somos-clube">							
									<p>Clube Fish 'n' Chips <span><?php echo $nome."?" ?></span> </p>
								</div>
							</div>
						</div>
						<!-- ÁREA DE VALORES DO CLUB -->
						<div class="area-valores-clube caixa-overflow">
							<ul class="sidebar-club">
							<?php     
                            	$postsClub = new WP_Query(array(
								    'post_type' => 'clube',
								    'posts_per_page' 	=> -1,
								    'tax_query' 		=> array(
																	array(
																		'taxonomy' => 'categoriaClube',
																		'field'    => 'slug',
																		'terms'    => $taxonomiaSlug,
																	)
																)
								    )
								);
	                            while ( $postsClub->have_posts() ) : $postsClub->the_post();                           
                            	
							?>		
								<li>
									<p><?php echo get_the_content() ?></p>
								</li>
							<?php  endwhile; ?>
							</ul>
						</div>
					</div>

				<?php }else{  ?>	

					<!-- ÁREA TELA INTEIRA SOBRE O CLUBE -->
					<section class="area-info-sobre-clube">
						
						<div class="row">
							<div class="col-md-12">
									<div class="foto-fundo-info-sobre-clube" style="background:url(<?php echo $categoriaAtivaImg ?>)">
										<div class="lente">
											<div class="caixa-posicao-conteudo-clube">
												
												<div class="info-texto-clube">

													<img src="<?php echo $img ?>" class="img-responsive icone">
													<?php
														if ($resumo == null) {
															echo '<p>'.$categoria->description.'</p>';
														}else{
															echo '<p>'.$resumo.'</p>';
														}
														
													 ?>
												</div>

												<div class="chamada-beneficios-clube">								

													<p>Clube Fish 'n' Chips <?php echo $nome ?>!</p>
												</div>
											
											</div>
										</div>
									</div>
									<!-- ÁREA DE VALORES DO CLUB -->
									<div class="area-valores-clubeBaixo caixa-overflowBaixo">
										<ul class="sidebar-clubBaixo">
											<?php     
				                            	$postsClub = new WP_Query(array(
												    'post_type' => 'clube',
												    'posts_per_page' 	=> -1,
												    'tax_query' 		=> array(
																					array(
																						'taxonomy' => 'categoriaClube',
																						'field'    => 'slug',
																						'terms'    => $taxonomiaSlug,
																					)
																				)
												    )
												);
					                            while ( $postsClub->have_posts() ) : $postsClub->the_post();                           
				                            	
											?>		
												<li>
													<p><?php echo get_the_content() ?></p>
												</li>
											<?php  endwhile; ?>
										</ul>
									</div>
							</div>
						</div>
						

					</section>
				<?php } ?>
				
				<?php $i++; }  ?>	
			
			</div>

		</section>

		<section class="area-formulario">
			<!-- SUB TÍTULOS PROMOÇÕES -->
			<p class="sub-titulo-descricao-paginas">Participar dessa Promocao</p>
		
			<!-- FORMULÁRIO PARA PARTICIPAR DO CLUBE -->
			<div class="form">
				
				<!-- <div class="form-group">
				    <label class="hidden" for="nome">nome</label>
				    <input placeholder="Nome"  type="nome" id="place" class="form-control place" id="nome">
				    <span></span>
				</div>
				
				<div class="form-group">
				    <label class="hidden" for="tel">tel</label>
				    <input placeholder="Telefone"  type="tel" id="place" class="form-control place" id="tel">
				    <span></span>
				</div>

				<div class="form-group">
				    <label class="hidden" for="email">Email</label>
				    <input placeholder="Email"  type="email" id="place" class="form-control place" id="email">
				    <span></span>
				</div>

				<input type="submit" value="participar" class="participar"> -->
				<?php echo do_shortcode('[contact-form-7 id="170" title="Formulário página Clube"]'); ?>
			</div>
		</section>

	</div>
<?php get_footer(); ?>
