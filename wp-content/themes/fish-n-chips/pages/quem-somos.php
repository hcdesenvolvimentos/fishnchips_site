<?php
/**
 * Template Name: Quem Somos
 * Description: Página quem somos do site da Fishnchips
 *
 * @package Fishnchips
 */


	$frase  = $configuracao['opt-quem-somos-frase'];
	
get_header(); ?>
<!-- PÁGINA QUEM SOMOS -->
	<div class="pg pg-quem-somos" style="display:">
		
		<!-- SUB TÍTULOS PROMOÇÕES -->
		<div class="">
			<p class="sub-titulo-descricao-paginas">Quem<b>Somos</b><span></span></p>	
		</div>
		<!-- ÁREA TEXTO QUEM SOMOS -->
		<section class="area-texto-quem-smos">

			<div class="texto-quem-somos">
				<span><?php echo $frase  ?></span>
				<?php echo the_content(); ?>
			</div>

		</section>

		<!-- ÁREA DE INTEGRANTES QUEM SOMOS -->
		<section class="itegrnates-quem-somos">
		
			<div class="lente-itegrnates-quem-somos" id="modal">
			</div>
			
			<ul>	
			<?php 
				// LOOP DE DESTAQUE
				$integrantes = new WP_Query( array( 'post_type' => 'equipe', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
	            $i = 0;
	            while ( $integrantes->have_posts() ) : $integrantes->the_post();
				
				$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$foto = $foto[0];	
				$funcao = rwmb_meta('Fishnchips_integrante_funcao'); 


				if ($i < 6 ) {
					
				
			?>			

				<!-- INTEGRANTE -->
				<li>
					<a href="#<?php echo $i ?>" id="botao-direcao-modal" class="perfil-integrantes" style="display:block;">
						<!-- FOTO -->
						<div class="foto-integrante-quem-somos" style="background:url(<?php echo $foto ?>)"></div>
						<!-- NOME / CARGO -->
						<div class="nome-cargo-itegrante-quem-somos">
							
							<p><?php echo get_the_title() ?></p>
							<span><?php echo $funcao ?></span>
						</div>

						<!-- MODAL DE INFORMAÇÕES COMPLETAS DO INTEGRANTES -->
						<div class="modal-quem-somos" id="<?php echo $i ?>" style="background:url(<?php echo $foto ?>)">
							<div class="lente-modal-quem-somos">	
								<span><?php echo get_the_title() ?></span>
								<b><?php echo $funcao ?></b>
								<?php echo the_content() ?>
							</div>
						</div>
					</a>
				</li>	
				<?php }else{ ?>	


					<!-- INTEGRANTE -->
					<li>
						<a href="#<?php echo $i ?>a" id="botao-direcao-modal" class="perfil-integrantes" style="display:block;">
							<!-- FOTO -->
							<div class="foto-integrante-quem-somos" style="background:url(<?php echo $foto ?>)"></div>
							<!-- NOME / CARGO -->
							<div class="nome-cargo-itegrante-quem-somos">
								<p><?php echo get_the_title() ?></p>
								<span><?php echo $funcao ?></span>
							</div>

							<!-- MODAL DE INFORMAÇÕES COMPLETAS DO INTEGRANTES -->
							<div class="modal-quem-somos modal-quem-somosBottom" id="<?php echo $i ?>a" style="background:url(<?php echo $foto ?>)">
								<div class="lente-modal-quem-somos">	
									<span><?php echo get_the_title() ?></span>
									<b><?php echo $funcao ?></b>
									<?php echo the_content() ?>
								</div>
							</div>
						</a>
					</li>
				<?php } ?>	

			<?php $i++ ;endwhile; wp_reset_query(); ?>
				
			</ul>
		
		</section>
	
	</div>
<?php get_footer(); ?>
