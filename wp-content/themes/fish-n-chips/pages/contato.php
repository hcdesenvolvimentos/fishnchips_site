<?php
/**
 * Template Name: Contato
 * Description: Página contato do site da Fishnchips
 *
 * @package Fishnchips
 */

$fotoContato = $configuracao['opt-contato-foto']['url'];	
$subtituloContato = $configuracao['opt-contato-subtitulo'];	
$fraseContato = $configuracao['opt-contato-frase'];	
$endereco = $configuracao['opt-dadosEndereco-endereco'];	
$sabado = $configuracao['opt-dadosEndereco-horario-atendimento'];	
$domingo = $configuracao['opt-dadosEndereco-horario-atendimento-domingo'];	
$fotoLogofundo = $configuracao['opt-inicial-logo-fundo']['url'];	
$logo = $configuracao['opt-inicial-logo']['url'];	
get_header(); ?>
<!-- PÁGINA CONTATO -->
	<div class="pg pg-contato">
		
		<section class="contato-pg-contato" style="background:url(<?php echo $fotoContato  ?>)">
			
			<div class="area-form-contato" style="background:url(<?php bloginfo('template_directory'); ?>/img/fundo-forme.png)">
				
				<div class="chamada-contato">
					<p><span>Entre em</span><span>contato</span>conosco</p>
				</div>

				<div class="info-contato">					
					<p><?php echo $subtituloContato  ?></p>
					<span><?php echo $fraseContato ?> </span>
				</div>

				<?php echo do_shortcode('[contact-form-7 id="129" title="Formulário de contato"]'); ?>

			</div>

		</section>


		<div class="info-endereco-contato-pg-contato">	
			<div class="row">
				<div class="col-md-6">
					<p>nosso endereco</p>
					<span><?php echo $endereco ?></span>
				</div>

				<div class="col-md-6">
					<p>Horários de Atendimentos</p>
					<span>Segunda a sábado das <?php echo $sabado ?></span>
					<span>Domingo das <?php echo $domingo ?></span>
				</div>				
			</div>
		</div>

		<!-- LOGO FISHNCHIPS -->
		<div class="logo-fishnchips-inicial" style="background:url(<?php echo $fotoLogofundo ?>)">
			<img src="<?php echo $logo ?>" alt="" class="img-responsive">
		</div>


	</div>

<?php get_footer(); ?>
	