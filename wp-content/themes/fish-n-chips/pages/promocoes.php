<?php
/**
 * Template Name: Promoções
 * Description: Página Promoções do site da Fishnchips
 *
 * @package Fishnchips
 */

	global $post;

$fotoCabecalho = $configuracao['opt-cardapio-imagem']['url'];
$descricaoCabecalho = $configuracao['opt-cardapio-frase'];
get_header(); ?>
<!-- PÁGINA PROMOÇÕES -->
	<div class="pg pg-promoçoes" style="display:">
		<!-- SUB TÍTULOS PROMOÇÕES -->
		
		<div class="titulo-pagina-cardapio">
			<p class="sub-titulo-descricao-paginas">Nossas<b>promocoes</b></p>
			<span><?php echo get_the_content() ?></span>
		</div>

		
		<!-- CARROSSEL DIAS DA SEMANA   -->
		<section class="carrossel-dias-promocoes">
			<!-- BOTÕES DO CARROSSEL  -->
			<button class="navegacaoPGPromocoesFrent"><i class="fa fa-angle-left"></i></button>
			<button class="navegacaoPGPromocoesTras"><i class="fa fa-angle-right"></i></button>

			<div id="carrossel-dias-promocoes" class="owl-Carousel">			
			<?php

				// DEFININDO TAXONOMIA PAI
				$categoriaPai = get_term_by('slug', 'promocao', 'categoriaCardapio');
				
				//ID TAXONOMIA PAI
				$id = $categoriaPai->term_id;

				//PASSANDO ID DA CATEGORIA PAI / LOOP CATEGORIAS FILHAS 
				$categoriaCardapiosFilhos = get_terms( 'categoriaCardapio' , array(
					'orderby'    => 'count',
					'hide_empty' => 0,
					'child_of'     => 0,
					'parent'       => $id,
					'orderby'      => 'name',
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0
				));
					
				// FOR CATEGORIAS FILHAS 
				foreach ($categoriaCardapiosFilhos as $categoriaCardapiosFilho):
					//PEGANDO NOME CATEGORIA FILHA 
					$nomeFilho = $categoriaCardapiosFilho->name;
					//PEGANDO SLUG CATEGORIA FILHA 
					$slugFilho = $categoriaCardapiosFilho->slug;

					// EXECUTANDO A QUERY 
					$postCardapio = new WP_Query(array(
					    'post_type' => 'cardapio',
					    'posts_per_page' 	=> -1,
					    'tax_query' 		=> array(
														array(
															'taxonomy' => 'categoriaCardapio',
															'field'    => 'slug',
															'terms'    => $slugFilho,
														)
													)
					    )
					);
			?>
				<div class="item">
					<!-- DIA DA SEMANA -->
					<a href="#" data-nomeDias="<?php echo $slugFilho; ?>" class="dia-semana-promocoes <?php echo $slugFilho; ?>">
						<p><?php echo $nomeFilho;?></p>
					</a>
				</div>
			<?php endforeach; ?>	
			</div>

		</section>
		
		

		<!-- ÁREA PRA PROMOÇÃO  -->
		<section class="area-promocao-pg-promocao">
	 	
	 	<?php

	 		$i = 0;

 			// DEFINE A TAXONOMIA PAI
			$taxonomia = 'categoriaCardapio';

			// LISTA AS CATEGORIAS PAI DO CARDÁPIO
			$categoriasCardapio = get_terms( $taxonomia, array(
				'orderby'    => 'count',
				'hide_empty' => 0,
				'parent'	 => 0
			));
			
			foreach ($categoriasCardapio as $categoriaCardapio):			
				
				// ID CATEGORIA PAI
				$taxonomiaID = $categoriaCardapio->term_id;
				

				// LISTA AS CATEGORIAS FILHAS DO CARDÁPIO
				$categoriasCardapioFilhas = get_terms( $taxonomia, array(
					'orderby'    => 'count',
					'hide_empty' => 0,
					'parent'	 => $taxonomiaID,
				));
			
				
				foreach ($categoriasCardapioFilhas as $categoriaCardapioFilha):
					// NOME CATEGORIA FILHA 
					$nomeFilha = $categoriaCardapioFilha->name;	
					//SLUG CATEGORIA FILHA 	
					$slugFilha = $categoriaCardapioFilha->slug;	
					//DESCRIÇÃO 	
					$descricao = explode("|",$categoriaCardapioFilha->description)	;
					//IMAGEM
					$categoriaAtivaImg = z_taxonomy_image_url($categoriaCardapioFilha->term_id);

		?>
			<ul class="data-categoriadata" id="<?php echo $slugFilha ?>">
				

				<li class="conteudo">
					
					<!-- TÍTULO DA PROMOÇÃO  -->
					<div class="titulo-promocao">
						<p>Promoção de <?php echo $nomeFilha ?></p>
						<!-- <span>Lorem Ipsum Dolor</span> -->
					</div>	
					
					<div class="itens-prato-promocao">
						<?php 

	                    	$PostCardapioCategoriafilhas = new WP_Query(array(
							    'post_type' => 'cardapio',
							    'posts_per_page' 	=> -1,
							    'tax_query' 		=> array(
																array(
																	'taxonomy' => 'categoriaCardapio',
																	'field'    => 'slug',
																	'terms'    => $slugFilha,
																)
															)
							    )
							);
							
							// LOOP DE POST DAS CATEGORAS FILHAS 
	                        while ( $PostCardapioCategoriafilhas->have_posts() ) : $PostCardapioCategoriafilhas->the_post();                           
		                		// PREÇO
		                		$preco = rwmb_meta('Fishnchips_prato_preco');
					            // ICONE
					            $icone = rwmb_meta('Fishnchips_prato_icone','type=image');

						?>
							<div class="item-prato-promocao">
								<!--ICONE -->
								<?php 
									foreach ($icone as $icone) {
						          		$urlIcone = $icone;					          	
								?>
								<img class="icone-promocao" src="<?php echo $urlIcone['url'] ?>" alt="">
								<?php } ?>

								<!--NOME E PREÇO -->
								<p><?php echo get_the_title() ?> <span><?php echo $preco ?></span></p>
							</div>
						
						<?php  endwhile; ?>
							
						<p class="info-adicionais"><?php echo $descricao[1] ?></p>
					
					</div>

					
				</li>

				<li class="foto-prato" id="foto-prato" style="background:url(<?php  echo $categoriaAtivaImg ?>)"></li>
			
			</ul>

		<?php $i++; endforeach; endforeach; ?>					
		
		</section>






	</div>



						

<?php get_footer(); ?>