<?php
/**
 * Template Name: Inicial
 * Description: Página inicial do site da Fishnchips
 *
 * @package Fishnchips
 */

	global $post;

	//OPT CONFIGURAÇÕES DA PÁGINA 

	// Informações da página ao lado da logo
	$LogoFishnChips = $configuracao['opt-inicial-logo']['url'];
	$titulo = $configuracao['opt-inicial-area-info-titulo'];
	$texto = $configuracao['opt-inicial-area-info-texto'];
	$linkVermais = $configuracao['opt-inicial-area-info-link'];

	// subtítulo cardápio
	$subtitulo_cardapio = $configuracao['opt-inicial-subtitulo-cardapio'];

	// área clube
	$tituloClube = $configuracao['opt-inicial-clube'];
	$fraseClube = $configuracao['opt-inicial-clube-frase'];
	$linkClube = $configuracao['opt-inicial-clube-link'];
	$imagemFundoClube = $configuracao['opt-inicial-clube-imagem']['url'];

	// imagem de fundo logo
	$imagemFundologo = $configuracao['opt-inicial-logo-fundo']['url'];
get_header(); ?>

	<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		
		<!-- CARROSSEL DESTAQUE  -->
		<section class="carrossel-destaque-inicial">
			<!-- BOTÕES -->
			<button class="navegacaodestaqueFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
			<button class="navegacaodestaqueTras hidden-xs"><i class="fa fa-angle-right"></i></button>
			
			<div id="carrossel-destaque-inicial" class="owl-Carousel">
				
			<?php 

				// LOOP DE DESTAQUE

				$destaquesPost = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
	           
	            while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();
				
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$foto = $foto[0];			
			?>

				<!-- ITEM / BG FUNDO -->
				<div class="item" style="background:url(<?php echo $foto ?>)">
					
					<div class="area-posicao-texto-inicial">
						<!-- TÍTULO -->
						<h2><?php echo get_the_title() ?></h2>
						<!-- BREVE DESCRIÇÃO -->
						<p><?php echo get_the_content() ?></p>

					</div>

				</div>		

			<?php endwhile; wp_reset_query(); ?>

			</div>

		</section>

		<!-- ÁREA DE INFORMAÇÕES DO SITE  -->
		<section class="row">
			
			<!-- ÁREA LOGO MARCA FISHNCHIPS -->
			<div class="col-md-5">
				<div class="logo-inicial">
					<img src="<?php echo $LogoFishnChips ?>" alt="" class="img-responsive">
				</div>
			</div>

			<!-- ÁREA DE INFORMAÇÕES DO SITE -->
			<div class="col-md-7">
				<div class="area-info-inicial">
					<p><?php echo $titulo ?></p>
					<span><?php echo $texto ?></span>
					<a href="<?php echo $linkVermais ?>" alt="Ver mais">Ver mais</a>
				</div>
			</div>

		</section>

		<!-- ÁREA DE PROMOÇÕES -->
		<section class="promocoes-inicial">
			<!-- SUB TÍTULOS PROMOÇÕES -->
			<p class="sub-titulo-paginas">Nossas Promocoes</p>

			<!-- CARROSSEL DE PROMOÇÕES  -->
			<div class="carrossel-promocoes-inicial" class="owl-Carousel">
				
				<!-- BOTÕES -->
				<button class="navegacaoPromocoesFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
				<button class="navegacaoPromocoesTras hidden-xs"><i class="fa fa-angle-right"></i></button>
				
				<div id="carrossel-promocoes-inicial" class="owl-Carousel">
				<?php

						// DEFININDO TAXONOMIA PAI
						$categoriaPai = get_term_by('slug', 'promocao', 'categoriaCardapio');
						
						//ID TAXONOMIA PAI
						$id = $categoriaPai->term_id;

						//PASSANDO ID DA CATEGORIA PAI / LOOP CATEGORIAS FILHAS 
						$categoriaCardapiosFilhos = get_terms( 'categoriaCardapio' , array(
							'orderby'    => 'count',
							'hide_empty' => 0,
							'child_of'     => 0,
							'parent'       => $id,
							'orderby'      => 'name',
							'pad_counts'   => 0,
							'hierarchical' => 1,
							'title_li'     => '',
							'hide_empty'   => 0
						));
							
						// FOR CATEGORIAS FILHAS 
						foreach ($categoriaCardapiosFilhos as $categoriaCardapiosFilho) :
							//PEGANDO NOME CATEGORIA FILHA 
							$nomeFilho = $categoriaCardapiosFilho->name;
						//var_dump($categoriaCardapiosFilho->description);
							//PEGANDO SLUG CATEGORIA FILHA 
							$slugFilho = $categoriaCardapiosFilho->slug;

							// EXECUTANDO A QUERY 
							$postCardapio = new WP_Query(array(
							    'post_type' => 'cardapio',
							    'posts_per_page' 	=> -1,
							    'tax_query' 		=> array(
																array(
																	'taxonomy' => 'categoriaCardapio',
																	'field'    => 'slug',
																	'terms'    => $slugFilho,
																)
															)
							    )
							);

						//LOOP DE POSTS 	
						while ( $postCardapio->have_posts() ) : $postCardapio->the_post();
							$fotoPromocao = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPromocao = $fotoPromocao[0];

						    $preco = rwmb_meta('Fishnchips_prato_preco');
						?>
					
						
					<!-- ITEM/BG FUNDO  -->
					<div class="item" style="background:url(<?php echo $fotoPromocao ?>)">
						
						<!-- ÁREA DE INFORMAÇÕES  -->
						<div class="info-carrossel-promocao">
							
							<!-- DATA DA PROMOÇÃO -->
							<span><?php echo $nomeFilho;?> </span>
							
							<!-- INFORMAÇÕES DA PROMOÇÃO -->
							<div class="info-carrossel-promocoes-inicial">
							<!-- LOOP DE CATEGORIAS -->
							<?php
								// DEFININDO CATEGORIAS ATUAIS MARCADAS NO POST 
								$termosPost = wp_get_post_terms( $post->ID, 'categoriaCardapio' ); 							
								$termosPost;
								
								
									//IF PARA FILTRAR CATEGORIA PROMOÇÃO E AS FILHAS  
									if ($termosPost->slug != "promocao" && $termosPost->slug != $slugFilho):
										
										//PEGANDO ICONE DA DESCRIÇÃO DA CATEGORIA PAI 
										$iconeCategoria = $categoriaCardapiosFilho->description;
										$icone = explode("|", $iconeCategoria);
										
							?>								
								<img src="<?php echo $icone[0] ?>" class="img-responsive" alt="">							
							<?php endif;?>
								
								<!-- NOME -->
								<h3><?php echo get_the_title(); ?> <b>por <?php echo $preco ?></b></h3>
								<hr>	
								<!-- DESCRIÇÕES -->
								<p>
								<?php
									$descricao = get_the_content();
									$textoresumido = $descricao;
									$textocurto = substr($textoresumido, 0, 100).'...';
									echo $textocurto;
								?>	
								</p>

							</div>
						
						</div>
						
					</div>
					
					<?php endwhile; wp_reset_query(); endforeach; ?>	
				
				</div>
				
			</div>

		</section>

		<!-- ÁREA CARDÁPIO -->
		<section class="cardapio-inicial">
			<!-- SUB TÍTULOS PROMOÇÕES -->
			<p class="sub-titulo-descricao-paginas">Nosso<b>Cardápio</b>
				<span><?php echo $subtitulo_cardapio  ?></span>
			</p>

			<!-- CARROSSEL CARDÁPIO  -->
			<div class="carrossel-cardapio-inicial">
				<!-- BOTÕES -->
				<button class="navegacaoCardapioFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
				<button class="navegacaoCardapioTras hidden-xs"><i class="fa fa-angle-right"></i></button>
				
				<div id="carrossel-cardapio-inicial" class="owl-Carousel">
					
					<!-- ITEM / BG FUNDO -->
					<?php 

						// LOOP DE 	CARDÁPIO

						$cardapioPratos = new WP_Query( array( 'post_type' => 'cardapio', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 10 ) );
			           
			            while ( $cardapioPratos->have_posts() ) : $cardapioPratos->the_post();
						
							$fotoPrato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPrato = $fotoPrato[0];			
					?>
						<div class="item">
							<a href="<?php echo get_permalink(); ?>" class="permalink-post"></a>
							<div class="item-carrossel" style="background:url(<?php echo $fotoPrato ?>)">
								<div class="area-cardapio-texto-inicial">
									<!-- TÍTULO -->
									<h2><?php echo get_the_title() ?></h2>							
									<a href="<?php echo get_permalink(); ?>" class="permalink">ver produto</a>
								</div>
							</div>
							
						</div>
					<?php endwhile; wp_reset_query(); ?>
				</div>

			</div>

			<a class="ver-cardapio" href="<?php echo home_url('cardapio/'); ?>">Ver cardápio completo</a>
		
		</section>
		
		<!-- ÁREA CLUBE -->
		<section class="clube-inicial" style="background:url(<?php echo $imagemFundoClube ?>)">	
			
			
			<!-- ÁREA INFORMAÇÕES SOBRE O CLUBE -->
			<div class="info-club-inicial">
				<!-- TÍTULO -->
				<h4><?php echo $tituloClube ?></h4>
				<!-- TEXTO -->
				<p><?php echo $fraseClube ?></p>
				<!-- LINK PARTICIPAR -->
				<a href="<?php echo $linkClube ?>">Participar</a>
			</div>

			<!-- ÁREA VALORES FISHNCHIPS -->
			<div class="carrossel-club-valores-inicial">		
				
				<div class="owl-Carousel club-valores-ul-inicial" id="club-valores-ul-inicial">
				<?php

				

					$categoriasClube = get_terms('categoriaClube' , array(
						'orderby'    => 'count',
						'hide_empty' => 0,
						'child_of'     => 0,
						'parent'       => 0,
						'orderby'      => 'name',
						'pad_counts'   => 0,
						'hierarchical' => 1,
						'title_li'     => '',
						'hide_empty'   => 0
					));
						

					foreach ($categoriasClube as $categoriaClube):
						if ($categoriaClube->slug == "beneficios") {
							$slugCategoriaclub = $categoriaClube->slug;
						}

						// EXECUTANDO A QUERY
						$postcategoriaClube = new WP_Query(array(
						    'post_type' => 'clube',
						    'posts_per_page' 	=> -1,
						    'tax_query' 		=> array(
															array(
																'taxonomy' => 'categoriaClube',
																'field'    => 'slug',
																'terms'    => $slugCategoriaclub,
															)
														)
						    )
						);

						while ( $postcategoriaClube->have_posts() ) : $postcategoriaClube->the_post();
							$iconeClube = rwmb_meta('Fishnchips_club_beneficio','type=image');
							foreach ($iconeClube as $iconeClube) {
								$iconePost = $iconeClube;
							}
				?>
					<!-- ITEM VALORES -->
					<div class="item click-item">

						<div class="club-valores-text-inicial">

							<img src="<?php echo $iconePost['url'] ?>" class="img-responsive icone"  alt="">
							<!-- VALOR / ICONE -->
							<p class="opacidade-efeito"><?php echo get_the_title() ?></p>
							
							<!-- TEXTO PARA EXIBIR NO CLIQUE MODAL  -->
							<div class="texto-modal">	
								<span><?php echo get_the_title() ?></span>
								<p><?php echo get_the_content() ?></p>
										
							</div>
						
						</div>
					</div>
					<?php endwhile; wp_reset_query();?>
				
				<?php endforeach; ?>	
				
				</div>

			</div>	
			
			<!-- LENTE EFEITO MODAL -->
			<div class="lente-clube-inicial" id="lente-clube-inicial">
			</div>

		</section>

		<!-- ÁREA BLOG -->
		<section class="blog-inicial">
			
			<ul>
				<!-- CHAMADA ÚLTIMAS DO BLOG -->
				<li class="titulo-blog">
					<a href="<?php echo home_url('/blog/'); ?>">
						<div class="blog-info">
							<img src="<?php bloginfo('template_directory'); ?>/svg/calendario.svg" alt="" class="img-responsive icone">
							<p>última <span>do blog</span></p>
						</div>
					</a>

				</li>

				<?php 

					// LOOP DE BLOG

					$PostBlog = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 7 ) );
		           
		            while ( $PostBlog->have_posts() ) : $PostBlog->the_post();
					
						$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoBlog = $fotoBlog[0];			
				?>
				<li>	
					
					<a href="<?php echo get_permalink(); ?>" class="blog-post" style="background:url(<?php echo $fotoBlog ?>)">
						<div class="lente-blog">
							<span>25/01/2016</span>
							<p><?php echo get_the_title() ?></p>
							<b>[ ver mais ]</b>
						</div>
					</a>

				
				</li>		

				<?php endwhile; wp_reset_query(); ?>	

			</ul>

		</section>

		<!-- LOGO FISHNCHIPS -->
		<div class="logo-fishnchips-inicial" style="background:url(<?php echo $imagemFundologo ?>)">
			<img src="<?php echo $LogoFishnChips ?>" alt="" class="img-responsive">
		</div>

	</div>



						

<?php get_footer(); ?>