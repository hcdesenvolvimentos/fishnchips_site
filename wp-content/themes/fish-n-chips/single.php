<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Fish_n\'_Chips
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];

$fotoBlog = $configuracao['opt-blog-foto']['url'];
get_header(); ?>

	<!-- PÁGINA POST BLOG -->
	<div class="pg pg-postagem">
		
		<div class="pg-blog">
			<div class="bg-blog-topo" style="background:url(<?php echo $fotoBlog   ?>)"></div>

			<!-- SUB TÍTULOS PROMOÇÕES -->
			<div class="text-center">
				<p class="sub-titulo-descricao-paginas">últimas<b>do blog</b><span></span></p>	
			</div>
		</div>
	

		<section class="area-postagem">
			<div class="row">
				
				<!--  SIDE BAR BLOG -->
				<?php echo get_sidebar(); ?>
				
				<!-- ÁREA POSTAGEM -->
				<div class="col-md-8">
					<div class="area-post">
						<!-- FOTO DA POSTAGEM -->
						<div class="foto-postagem" style="background:url(<?php echo $foto ?>)">
							<!-- DATA DA POSTAGEM -->
							<div class="data-postagem">
								<small><?php the_time('j') ?></small><span><?php the_time('F \d\e Y') ?></span>

							</div>
						</div>
						<!-- TEXTO POSTAGEM -->
						<div class="texto-postagem">
							<h3 class="titulo text-center"><i><?php echo get_the_title() ?></i></h3>
							<?php echo the_content() ?>
						</div>
						<!-- LINK COMPARTILHAR -->
						<div class="redessociais-postagem">
							<a  href="http://facebook.com/share.php?u=<?php the_permalink() ?>&amp;t=<?php echo urlencode(the_title('','', false)) ?>" target="_blank" title="Compartilhar <?php the_title();?> no Facebook" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href="http://twitter.com/intent/tweet?text=<?php the_title();?>&url=<?php the_permalink();?>&via=seunomenotwitter" title="Twittar sobre <?php the_title();?>" target="_blank" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</div>
						<!-- BOTÕES DE NAVEGAÇÕES -->
						<div class="botoes">
							<?php previous_post('%','<i class="fa fa-angle-left" aria-hidden="true"></i>Anterior', 'no') ?>
							<?php next_post('%','	Próximo<i class="fa fa-angle-right" aria-hidden="true"></i> ', 'no') ?>
						</div>

					
					
						<!-- COMENTÁRIOS  -->
						<div class="comentarios">
							<?php
								//If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
							?>	
						</div>

					</div>
				</div>

			</div>
		</section>
	</div>

<?php

get_footer();
