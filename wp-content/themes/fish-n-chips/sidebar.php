<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fish_n\'_Chips
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<!--  SIDE BAR BLOG -->
<div class="col-md-2 sidebar-blog">
<?php
	$i = 0;
	// CATEGORIA ATUAL
	$categoriaAtual = get_the_category();
	$categoriaAtual = $categoriaAtual[0]->cat_name;

	// LISTA DE CATEGORIAS
	$arrayCategorias = array();

	$categorias=get_categories($args);
	foreach($categorias as $categoria) {
	$arrayCategorias[$categoria->cat_ID] = $categoria->name;
	$nomeCategoria = $arrayCategorias[$categoria->cat_ID];


?>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  	<div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingOne">
		     	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $i; ?>" aria-expanded="true" aria-controls="<?php echo $i; ?>"><?php echo $nomeCategoria; ?></a>
		    </div>
		    <div id="<?php echo $i; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
     		<?php
				/* Seleciona os anos no banco de dados */
				$anos = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC");
				$i = 0;
				foreach($anos as $ano) :
			?>
		     	<a href="" class="ano" >Arquivo <?php echo $ano; ?></a>
	     		<ul>
					<?php
						$args = array(
										'type' => 'monthly',
										'echo' => 0,
										'year' => ''.$ano.'',
										);
						echo wp_get_archives( $args );
					?>
				</ul>
     		<?php $i++; endforeach; ?>
		    </div>
		</div>
	</div>
<?php $i++;} ?>

</div>