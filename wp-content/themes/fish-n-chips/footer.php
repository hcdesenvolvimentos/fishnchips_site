<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fish_n\'_Chips
 */
global $configuracao;
// DADOS ENDEREÇOS E CONTATO
$endereco = $configuracao['opt-dadosEndereco-endereco'];
$cidadeCep = $configuracao['opt-dadosEndereco-cep'];
$telefone = $configuracao['opt-telefone'];
$horarioAtendimento = $configuracao['opt-dadosEndereco-horario-atendimento'];
$horarioAtendimentoDomingo = $configuracao['opt-dadosEndereco-horario-atendimento-domingo'];
$logo = $configuracao['opt-dadosEndereco-logo-branca']['url'];
?>

	<!-- RODAPÉ  -->
	<footer class="rodape">
		<!-- MAPA FUNDO -->
		<div class="mapa">
			<div class="redessociais">	
				<a target="_blank" href="<?php echo $configuracao['opt-facebook'] ?>"><i class="fa fa-facebook hvr-pop" aria-hidden="true"></i></a>
				<a target="_blank" href="<?php echo $configuracao['opt-twitter'] ?>"><i class="fa fa-instagram hvr-wobble-horizontal" aria-hidden="true"></i></a>
				<!-- <a target="_blank" href="<?php echo $configuracao['opt-youtube'] ?>"><i class="fa fa-youtube hvr-buzz-out" aria-hidden="true"></i></a>
				<a target="_blank" href="<?php echo $configuracao['opt-pinterest'] ?>"><i class="fa fa-pinterest-p hvr-wobble-vertical" aria-hidden="true"></i></a> -->
			</div>
			<!-- INFORMAÇÕES DE CONTATO ENDEREÇO HORÁRIO DE FUNCIONAMENTO-->
			<div class="info-rodape">
				<img src="<?php echo $logo ?>" alt="Fish 'n' chips" class="img-responsive">

				<!-- INFORMAÇÕES DE CONTATO ENDEREÇO -->
				<div class="info-contato-endereco">
					<span><?php echo $endereco  ?></span>
					<span>Telefone :<?php echo $telefone ?></span>
					<a href="#">www.victorfishnchips.com.br</a>
				</div>
				
				<!-- INFORMAÇÕES HORÁRIO DE ATENDIMENTO -->
				<div class="horario-atendimento">
					<span>Horário de antendimento</span>
					<p>Segunda a Sábado das <?php echo $horarioAtendimento ?> e domingos das <?php echo $horarioAtendimentoDomingo ?></p>
				</div>
				
				<!-- FORMULÁRIO DE NEWSALLATER -->
				<div class="formulario-rodape">
					<div class="titutlo-formulario-rodape"><span class="icone"><i class="fa fa-envelope" aria-hidden="true"></i></span> receba nossas news</div>
					
					<!--START Scripts : this is the script part you can add to the header of your theme-->
					<script type="text/javascript" src="http://localhost/projetos/fishnchips_site/wp-includes/js/jquery/jquery.js?ver=2.7.2"></script>
					<script type="text/javascript" src="http://localhost/projetos/fishnchips_site/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.2"></script>
					<script type="text/javascript" src="http://localhost/projetos/fishnchips_site/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.2"></script>
					<script type="text/javascript" src="http://localhost/projetos/fishnchips_site/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.2"></script>
					<script type="text/javascript">
						/* <![CDATA[ */
						var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://localhost/projetos/fishnchips_site/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
						/* ]]> */
					</script><script type="text/javascript" src="http://localhost/projetos/fishnchips_site/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.2"></script>
					<!--END Scripts-->

					<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html57694afa5c6bb-3" class="wysija-msg ajax"></div>
						<form id="form-wysija-html57694afa5c6bb-3" style="position:relative;" method="post" action="#wysija" class="widget_wysija html_wysija">
							
							<div class="form-group">
								<label for="nome" class="hidden">Nome</label>
								<input  placeholder="nome" id="formulario-rodape" type="text" name="wysija[user][firstname]" class="form-control text-center" title="Nome"  value="" />
								<span></span>
								<small class="abs-req">
									<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
								</small>

							</div>
							
						  	<div class="form-group">
								<label for="email" class="hidden">Email <span class="wysija-required">*</span></label>
								<input placeholder="email" id="formulario-rodape" type="text" name="wysija[user][email]" class="form-control text-center validate[required,custom[email]]" title="Email"  value="" />
								<span></span>
								<small class="abs-req">
									<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
								</small>

							</div>
						  	<div class="form-group">
								<input class="enviar-rodape wysija-submit-field" type="submit" value="enviar" />
							</div>
							<input type="hidden" name="form_id" value="3" />
							<input type="hidden" name="action" value="save" />
							<input type="hidden" name="controller" value="subscribers" />
							<input type="hidden" value="1" name="wysija-page" />


							<input type="hidden" name="wysija[user_list][list_ids]" value="3" />

					</form>
				</div>



			</div>

			</div>


			<!-- MAPA -->
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.9541687698656!2d-49.284752884444366!3d-25.439795739511087!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce47672298c8f%3A0x7bb06755fb9b567!2sThe+Fish+N+Chips!5e0!3m2!1spt-BR!2sbr!4v1465335659890" frameborder="0" style="border:0" allowfullscreen></iframe>
			
			
		</div>
	</footer>

	<div class="copyright">
		<i class="fa fa-copyright" aria-hidden="true"></i>
		<span>Copyright 2016 - Fish n Chips - Todos os direitos reservados</span>
	</div>
	<div class="col-md-12" style="text-align: center; background: #222528;">
      <small>
        <span style="vertical-align: text-bottom;color:#ccc;">Desenvolvido por</span>
        <a href="http://palupa.com.br/" target="_blank" title="Desenvolvido por Palupa Marketing" style="display: inline-block;"><img src="<?php bloginfo('template_directory'); ?>/img/palupaLogo.png" style="max-height: 15px;"></a>
      </small>
	</div>


<?php wp_footer(); ?>

</body>
</html>
