<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fish_n\'_Chips
 */
global $configuracao;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/img/fave.ico" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- TOPO -->
<header class="topo" id="topo">
	<!-- LOGO -->
	<a href="<?php echo home_url('/'); ?>">
		<h1>Fishn Chips</h1>
	</a>

	<!-- MENU -->
	<div class="nav">	
		
		<div class="botao-menu-ativo" id="botao-sidebarAbrir">				
			<i class="fa fa-bars" aria-hidden="true"></i>
		</div>

		<div class="botao-menu-ativo" id="botao-sidebarFechar" style="display:none">		
			<i class="fa fa-times" aria-hidden="true"></i>				
		</div>
		
		<ul>

			<li>
				<a  class="topo-borda" href="<?php echo home_url('/quem-somos/'); ?>">Quem somos </a>
			</li>

			<li>
				<a  class="topo-borda" href="<?php echo home_url('promocoes/'); ?>">Promoções </a>
			</li>

			<li>
				<a  class="topo-borda" href="<?php echo home_url('cardapio/'); ?>">Cardápio </a>
			</li>

			<li>
				<a  class="topo-borda" href="<?php echo home_url('clube-fishn-chips/'); ?>">Clube </a>
			</li>

			<li>
				<a  class="topo-borda" href="<?php echo home_url('/blog/'); ?>">Blog </a>
			</li>

			<li>
				<a  class="topo-borda" href="<?php echo home_url('/delivery /'); ?>">Delivery</a>
			</li>

			<li>
				<a href="<?php echo home_url('/contato/'); ?>">Contato </a>
			</li>

		</ul>
	</div>
</header>