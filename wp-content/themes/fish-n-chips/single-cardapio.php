<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Fish_n\'_Chips
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
$fotoCabecalho = $configuracao['opt-cardapio-imagem']['url'];
$descricaoCabecalho = $configuracao['opt-cardapio-frase'];

$fotoBlog = $configuracao['opt-blog-foto']['url'];


	// DEFININDO CATEGORIAS ATUAIS MARCADAS NO POST 
	$termosPost = wp_get_post_terms( $post->ID, 'categoriaCardapio' ); 							
	$termosPost;
	
	foreach ($termosPost as $termosPost ) {
		//IF PARA FILTRAR CATEGORIA PROMOÇÃO E AS FILHAS  
		if ($termosPost->slug != "promocao" 
			&& $termosPost->slug != "segunda-feira" 
			&& $termosPost->slug != "terca-feira" 
			&& $termosPost->slug != "quarta-feira" 
			&& $termosPost->slug != "quinta-feira" 
			&& $termosPost->slug != "sexta-feira" 
			&& $termosPost->slug != "sabado-feira" 
			&& $termosPost->slug != "domingo-feira") {
			
			//PEGANDO ICONE DA DESCRIÇÃO DA CATEGORIA PAI 
			$nomeCategoria = $termosPost->name;
				
		}
	 }
		
get_header(); ?>

	<div class="pg pg-cardapio">
	
	<div class="bg-topo-cardapio" style="background:url(<?php echo $fotoCabecalho ?>)">							
									
		
		<!-- SUB TÍTULOS PROMOÇÕES -->
		<div class="titulo-pagina-cardapio">
		
			<p class="sub-titulo-descricao-paginas"><?php echo $nomeCategoria ?></p>
			<span><?php echo $descricaoCabecalho ?></span>	
		</div>

	</div>
	
	<!-- CARROSSEL DE PRATOS   -->
	<section class="carrossel-pratos-cardapio">
		<!-- BOTÕES DO CARROSSEL  -->
		<button class="navegacaoPGcardapioFrent"><i class="fa fa-angle-left"></i></button>
		<button class="navegacaoPGcardapioTras"><i class="fa fa-angle-right"></i></button>
		
		<div id="carrossel-pratos-cardapio" class="owl-Carousel">
			 <?php

				// DEFINE A TAXONOMIA
				$taxonomia = 'categoriaCardapio';

				// LISTA AS CATEGORIAS PAI DO CARDAPIO
				$categoriasCardapio = get_terms( $taxonomia, array(
					'orderby'    => 'count',
					'hide_empty' => 0,
					'parent'	 => 0
				));
				
				
				
				foreach ($categoriasCardapio as $categoriaCardapio) {
					//FILTRANDO CATEGORIA PROMOÇÃO
					if ($categoriaCardapio->name != "Promoção") {
						$nome = $categoriaCardapio->name;

					}								
					// IMAGEM ICONE DA CATEGORIA
					$categoriaAtivaImg = z_taxonomy_image_url($categoriaCardapio->term_id);
					// VERIFICANDO CATEGORIA ATUAL / ADICIONANDO CLASSE ATIVA NA ATUAL
					if ($nome == $nomeCategoria) {					
					
			?>	
			
			<div class="item ativo">
				<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>">
					<div class="item-prato-cardapio">
						<p><?php echo $nome ?></p>
						
						<img src="<?php echo $categoriaAtivaImg ?> " class="icone" alt="">		
					</div>		
				</a>	
			</div>
			
			<?php  }else{  ?>
			
			<div class="item">
				<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>">
					<div class="item-prato-cardapio">
						<p><?php echo $nome ?></p>
						
						<img src="<?php echo $categoriaAtivaImg ?> " class="icone" alt="">		
					</div>		
				</a>	
			</div>

			<?php  } } ?>
		
		</div>

	</section>

	<!-- PRATOS DO CARDÁPIO -->
	<section class="todos-pratos-cardapio pg-produto">
		<ul>
			
			<!-- PRATO -->
			<li>
				<!-- FOTO  -->
				<a href="<?php echo get_permalink(); ?>" class="foto-prato-cardapio" style="background:url(<?php echo $foto ?>)">
					
					<div class="lente-foto-prato-cardapio">
						<div class="info-prato-cardapioEsquerdo">
							<!-- NOME -->
							<h3><?php echo get_the_title() ?></h3>
							<hr>
							<!-- TEXTO -->
							<p>
							<?php
								echo get_the_content();
							
							?>	
							</p> 
						</div>	
					</div>
				</a>
			</li>

		</ul>
		
	</section>

</div>

<?php

get_footer();
