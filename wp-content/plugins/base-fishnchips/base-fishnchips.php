<?php

/**
 * Plugin Name: Base Fishnchips
 * Description: Controle base do tema Fishnchips.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function baseFishnchips () {

		// TIPOS DE CONTEÚDO
		conteudosFishnchips();

		// TAXONOMIA
		taxonomiaFishnchips();

		// META BOXES
		metaboxesFishnchips();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosFishnchips (){

		// TIPOS DE CONTEÚDO

		tipoDestaque();

		tipoCardapio();
		
		tipoEquipe();
		
		tipoClube();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'título do destaque';
				break;				
				
				case 'prato':
					$titulo = 'nome do prato';
				break;

				case 'integrante':
					$titulo = 'nome do integrante';
				break;

				case 'clube':
					$titulo = 'nome';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaques',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaques',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaques',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}
	
		// CUSTOM POST TYPE CARDÁPIO
		function tipoCardapio() {

			$rotulosCardapio = array(
									'name'               => 'Pratos',
									'singular_name'      => 'prato',
									'menu_name'          => 'Cardápio',
									'name_admin_bar'     => 'Pratos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo prato',
									'new_item'           => 'Novo prato',
									'edit_item'          => 'Editar prato',
									'view_item'          => 'Ver prato',
									'all_items'          => 'Todos os pratos',
									'search_items'       => 'Buscar prato',
									'parent_item_colon'  => 'Dos pratos',
									'not_found'          => 'Nenhum prato cadastrado.',
									'not_found_in_trash' => 'Nenhum prato na lixeira.'
								);

			$argsCardapio	= array(
									'labels'             => $rotulosCardapio,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-carrot',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'cardapio' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('cardapio', $argsCardapio);

		}

		// CUSTOM POST TYPE EQUIPE
		function tipoEquipe() {

			$rotulosEquipe = array(
									'name'               => 'Integrantes',
									'singular_name'      => 'integrante',
									'menu_name'          => 'Equipe',
									'name_admin_bar'     => 'integrantes',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo integrante',
									'new_item'           => 'Novo integrante',
									'edit_item'          => 'Editar integrante',
									'view_item'          => 'Ver integrante',
									'all_items'          => 'Todos os integrantes',
									'search_items'       => 'Buscar integrante',
									'parent_item_colon'  => 'Dos integrantes',
									'not_found'          => 'Nenhum integrante cadastrado.',
									'not_found_in_trash' => 'Nenhum integrante na lixeira.'
								);

			$argsEquipe	= array(
									'labels'             => $rotulosEquipe,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-groups',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'equipe' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('equipe', $argsEquipe);

		}

		// CUSTOM POST TYPE CLUBE
		function tipoClube() {

			$rotulosClube = array(
									'name'               => 'Post',
									'singular_name'      => 'post',
									'menu_name'          => 'Clube',
									'name_admin_bar'     => 'post',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo post',
									'new_item'           => 'Novo post',
									'edit_item'          => 'Editar post',
									'view_item'          => 'Ver post',
									'all_items'          => 'Todos os posts',
									'search_items'       => 'Buscar post',
									'parent_item_colon'  => 'Dos posts',
									'not_found'          => 'Nenhum post cadastrado.',
									'not_found_in_trash' => 'Nenhum post na lixeira.'
								);

			$argsClube	= array(
									'labels'             => $rotulosClube,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-universal-access-alt',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'clube' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('clube', $argsClube);

		}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaFishnchips () {

		taxonomiaCategoriaCardapio();
		
		taxonomiaCategoriaClube();
	}
	
		// TAXONOMIA DE CARDÁPIO
		function taxonomiaCategoriaCardapio() {

			$rotulosCategoriaCardapio = array(
												'name'              => 'Categorias de cardápio',
												'singular_name'     => 'Categoria de cardápio',
												'search_items'      => 'Buscar categorias de cardápio',
												'all_items'         => 'Todas as categorias de cardápio',
												'parent_item'       => 'Categoria de cardápio pai',
												'parent_item_colon' => 'Categoria de cardápio pai:',
												'edit_item'         => 'Editar categoria de cardápio',
												'update_item'       => 'Atualizar categoria de cardápio',
												'add_new_item'      => 'Nova categoria de cardápio',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de cardápio',
											);

			$argsCategoriaCardapio 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaCardapio,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-cardapio' ),
											);

			register_taxonomy( 'categoriaCardapio', array( 'cardapio' ), $argsCategoriaCardapio );

		}

		// TAXONOMIA DE CLUBE
		function taxonomiaCategoriaClube() {

			$rotulosCategoriaClube = array(
												'name'              => 'Categorias do clube',
												'singular_name'     => 'Categoria do clube',
												'search_items'      => 'Buscar categorias do clube',
												'all_items'         => 'Todas as categorias do clube',
												'parent_item'       => 'Categoria do clube pai',
												'parent_item_colon' => 'Categoria do clube pai:',
												'edit_item'         => 'Editar categoria do clube',
												'update_item'       => 'Atualizar categoria do clube',
												'add_new_item'      => 'Nova categoria do clube',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias do clube',
											);

			$argsCategoriaClube 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaClube,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-clube' ),
											);

			register_taxonomy( 'categoriaClube', array( 'clube' ), $argsCategoriaClube );

		}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesFishnchips(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Fishnchips_';
		
			//METABOX DE CARDÁPIO
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxCardapio',
				'title'			=> 'Detalhes do prato',
				'pages' 		=> array( 'cardapio' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
				
					array(
						'name'  => 'Preço: ',
						'id'    => "{$prefix}prato_preco",
						'desc'  => '',
						'type'  => 'text',					
					),
					array(
						'name'  => 'Icone do prato: ',
						'id'    => "{$prefix}prato_icone",
						'desc'  => '',
						'type'  => 'image',
						'max_file_uploads' => 1

					)
				),
			);
			
			//METABOX DE EQUIPE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxEquipe',
				'title'			=> 'Detalhes do integrante',
				'pages' 		=> array( 'equipe' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(		
					
					array(
						'name'  => 'Função: ',
						'id'    => "{$prefix}integrante_funcao",
						'desc'  => '',
						'type'  => 'text',
						
					),
					
					
				),
			);

			//METABOX DE EQUIPE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxClube',
				'title'			=> 'Icone do benefício',
				'pages' 		=> array( 'clube' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(		
					
					array(
						'name'  => 'icone benefícios: ',
						'id'    => "{$prefix}club_beneficio",
						'desc'  => '',
						'type'  => 'image',
						'max_file_uploads' => 1
						
					),
					
					
				),
			);


			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesFishnchips(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerFishnchips(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseFishnchips');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseFishnchips();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );